<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 14.03.2017
 * Time: 11:44
 */
class LinkPager extends CLinkPager
{
    public $nextPageLabel = '<i class="g-loader__button-icon"></i><span class="g-loader__button-text">Загрузить ещё</span>';
    public $nextPageCssClass = 'g-loader__content';

//    public $htmlOptions = ['class' => 'g-loader__content'];

    protected function createPageButtons()
    {
        if (($pageCount = $this->getPageCount()) <= 1) {
            return [];
        }

        list($beginPage, $endPage) = $this->getPageRange();
        $currentPage = $this->getCurrentPage(false); // currentPage is calculated in getPageRange()
        $buttons = [];

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->createPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        return $buttons;
    }

    protected function createPageButton($label, $page, $class, $hidden, $selected)
    {
        if ($hidden || $selected) {
            $class .= ' ' . ($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);
        }
        return '<div class="' . $class . '">' . CHtml::link($label, $this->createPageUrl($page), ['class' => 'g-loader__button']) . '</div>';
    }

    public function run()
    {
        $this->registerClientScript();
        $buttons = $this->createPageButtons();
        if (empty($buttons)) {
            return;
        }
        echo $this->header;
        echo CHtml::tag('div', $this->htmlOptions, implode("\n", $buttons));
        echo $this->footer;
    }
}
