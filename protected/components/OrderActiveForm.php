<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 18.01.2017
 * Time: 1:57
 */

Yii::import('bootstrap.widgets.TbActiveForm');

class OrderActiveForm extends TbActiveForm
{
    /**
     * Renders a vertical custom field group for a model attribute.
     *
     * @param array|string $fieldData Pre-rendered field as string or array of arguments for call_user_func_array() function.
     * @param CModel $model The data model.
     * @param string $attribute The attribute.
     * @param array $options Row options.
     *
     */
    protected function verticalGroup(&$fieldData, &$model, &$attribute, &$options)
    {

        $groupOptions = isset($options['groupOptions']) ? $options['groupOptions'] : [];
        self::addCssClass($groupOptions, 'form-group');

        if ($model->hasErrors($attribute)) {
            self::addCssClass($groupOptions, 'has-error');
        }

        echo CHtml::openTag('div', $groupOptions);

        self::addCssClass($options['labelOptions'], 'control-label');
        if (isset($options['label'])) {
            if (!empty($options['label'])) {
                echo CHtml::label($options['label'], CHtml::activeId($model, $attribute), $options['labelOptions']);
            }
        } else {
            echo $this->labelEx($model, $attribute, $options['labelOptions']);
        }

        if (isset($options['hint'])) {
            self::addCssClass($options['hintOptions'], $this->hintCssClass);
            echo CHtml::tag($this->hintTag, $options['hintOptions'], $options['hint']);
        }

        if (!empty($options['prepend']) || !empty($options['append'])) {
            $this->renderAddOnBegin($options['prepend'], $options['append'], $options['prependOptions']);
        }

        if (is_array($fieldData)) {
            echo call_user_func_array($fieldData[0], $fieldData[1]);
        } else {
            echo $fieldData;
        }

        if (!empty($options['prepend']) || !empty($options['append'])) {
            $this->renderAddOnEnd($options['append'], $options['appendOptions']);
        }

        if ($this->showErrors && $options['errorOptions'] !== false) {
            echo $this->error($model, $attribute, $options['errorOptions'], $options['enableAjaxValidation'], $options['enableClientValidation']);
        }

        echo '</div>';
    }
}
