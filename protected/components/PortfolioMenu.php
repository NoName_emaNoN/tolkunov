<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 24.12.2016
 * Time: 21:58
 */

Yii::import('zii.widgets.CMenu');

class PortfolioMenu extends CMenu
{
    public $tagName = 'div';

    /**
     * Renders the menu items.
     * @param array $items menu items. Each menu item will be an array with at least two elements: 'label' and 'active'.
     * It may have three other optional elements: 'items', 'linkOptions' and 'itemOptions'.
     */
    protected function renderMenu($items)
    {
        if (count($items)) {
            echo CHtml::openTag($this->tagName, $this->htmlOptions) . "\n";
            $this->renderMenuRecursive($items);
            echo CHtml::closeTag($this->tagName);
        }
    }

    /**
     * Recursively renders the menu items.
     * @param array $items the menu items to be rendered recursively
     */
    protected function renderMenuRecursive($items)
    {
        $count = 0;
        $n = count($items);
        foreach ($items as $item) {
            $count++;
            $options = isset($item['itemOptions']) ? $item['itemOptions'] : [];
            $class = [];
            if ($item['active'] && $this->activeCssClass != '') {
                $class[] = $this->activeCssClass;
            }
            if ($count === 1 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($count === $n && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if ($this->itemCssClass !== null) {
                $class[] = $this->itemCssClass;
            }
            if ($class !== []) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            if (isset($item['linkOptions']['class'], $options['class'])) {
                $item['linkOptions']['class'] = trim(implode(' ', array_unique(array_merge(explode(' ', $item['linkOptions']['class']), explode(' ', $options['class'])))));
            }

            if(isset($item['linkOptions'])) {
                $item['linkOptions'] = CMap::mergeArray($options, $item['linkOptions'] ?: []);
            }

            $menu = $this->renderMenuItem($item);
            if (isset($this->itemTemplate) || isset($item['template'])) {
                $template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template, ['{menu}' => $menu]);
            } else {
                echo $menu;
            }

            if (isset($item['items']) && count($item['items'])) {
                echo "\n" . CHtml::openTag($this->tagName, isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions) . "\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag($this->tagName) . "\n";
            }

            echo CHtml::closeTag('li') . "\n";
        }
    }
}
