<?php

/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 16.12.2016
 * Time: 10:47
 */

Yii::import('zii.widgets.CMenu');

class MyMenu extends CMenu
{
    protected function renderMenu($items)
    {
        if (count($items)) {
            echo CHtml::openTag('div', $this->htmlOptions) . "\n";
            $this->renderMenuRecursive($items);
            echo CHtml::closeTag('div');
        }
    }

    /**
     * Recursively renders the menu items.
     * @param array $items the menu items to be rendered recursively
     */
    protected function renderMenuRecursive($items)
    {
        $count = 0;
        $n = count($items);
        foreach ($items as $item) {
            $count++;
            $options = isset($item['itemOptions']) ? $item['itemOptions'] : [];
            $class = [];
            if ($item['active'] && $this->activeCssClass != '') {
                $class[] = $this->activeCssClass;
            }
            if ($count === 1 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($count === $n && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if ($this->itemCssClass !== null) {
                $class[] = $this->itemCssClass;
            }
            if ($class !== []) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            echo CHtml::openTag('div', $options);

            $menu = $this->renderMenuItem($item);
            if (isset($this->itemTemplate) || isset($item['template'])) {
                $template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
                echo strtr($template, ['{menu}' => $menu]);
            } else {
                echo $menu;
            }

            if (isset($item['items']) && count($item['items'])) {
                echo "\n" . CHtml::openTag('div', isset($item['submenuOptions']) ? $item['submenuOptions'] : $this->submenuHtmlOptions) . "\n";
                $this->renderMenuRecursive($item['items']);
                echo CHtml::closeTag('div') . "\n";
            }

            echo CHtml::closeTag('div') . "\n";
        }
    }
}