<?php

use yupe\components\Event;

//Yii::import('application.modules.page.models.Page');

/**
 * Class ClientSitemapGeneratorListener
 */
class ClientSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Client::model()->published());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                $item->absoluteUrl,
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }
}
