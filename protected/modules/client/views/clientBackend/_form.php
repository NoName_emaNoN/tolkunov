<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Client
 * @var $form \yupe\widgets\ActiveForm
 * @var $this ClientBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'client-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('ClientModule.client', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('ClientModule.client', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('title'),
                        'data-content' => $model->getAttributeDescription('title'),
                    ],
                ],
            ]); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->slugFieldGroup(
                $model,
                'slug',
                [
                    'sourceAttribute' => 'title',
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('slug'),
                            'data-content' => $model->getAttributeDescription('slug'),
                            'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data' => $model->getStatusList(),
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'empty' => Yii::t('ClientModule.client', '--выберите--'),
                            'data-original-title' => $model->getAttributeLabel('status'),
                            'data-content' => $model->getAttributeDescription('status'),
                            'data-container' => 'body',
                        ],
                    ],
                ]
            ); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'sort', ['hint' => 'Чем больше — тем выше будет отображаться клиент.']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 <?= $model->hasErrors('text') ? 'has-error' : ''; ?>">
            <?= $form->labelEx($model, 'text'); ?>
            <?php $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'text',
                ]
            ); ?>
            <?= $form->error($model, 'text'); ?>
        </div>
    </div>
    <div class='row'>
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->image ? $model->getImageUrl(200, 200, true) : '#',
                    $model->title,
                    [
                        'class' => 'preview-image img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->image): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'image',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'onchange' => 'readURL(this);',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class='row hidden'>
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image2 ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->image2 ? $model->imageUpload2->getImageUrl(200, 200, true) : '#',
                    $model->title,
                    [
                        'class' => 'img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->image2 ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->image2): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file-2"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup($model, 'image2'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?= $form->textFieldGroup($model, 'link', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('link'),
                        'data-content' => $model->getAttributeDescription('link'),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <br/>


    <div class="row">
        <div class="col-sm-12">
            <?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
            <div class="panel-group" id="extended-options">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <a data-toggle="collapse" data-parent="#extended-options" href="#collapseTwo">
                                <?php echo Yii::t('ClientModule.client', 'Data for SEO'); ?>
                            </a>
                        </div>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $form->textFieldGroup($model, 'seo_title'); ?>
                            <?= $form->textFieldGroup($model, 'seo_keywords'); ?>
                            <?= $form->textAreaGroup($model, 'seo_description', ['widgetOptions' => ['htmlOptions' => ['rows' => 8]]]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <br/><br/>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('ClientModule.client', 'Сохранить клиента и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('ClientModule.client', 'Сохранить клиента и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>