<?php

class m181104_060140_add_sort_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{client_client}}', 'sort', 'integer NOT NULL DEFAULT 1');

        //ix
        $this->createIndex("ix_{{client_client}}_sort", '{{client_client}}', 'sort', false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{client_client}}', 'sort');
    }
}
