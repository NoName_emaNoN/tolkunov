<?php

/**
 * This is the model class for table "{{offer_offer}}".
 *
 * The followings are the available columns in table '{{offer_offer}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $image_splash
 * @property string $coupons
 * @property string $description
 * @property string $text
 * @property string $date
 * @property integer $status
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * The followings are the available model relations:
 * @property OfferItem[] $items
 *
 * @method Offer published()
 */
class Offer extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{offer_offer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, description, text, coupons, seo_title, seo_description, seo_keywords', 'filter', 'filter' => 'trim'],
            ['title, coupons, seo_title, seo_description, seo_keywords', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['title, slug', 'required'],
            ['status, coupons', 'numerical', 'integerOnly' => true],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['title, seo_title, seo_keywords, seo_description', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['slug', 'unique'],
            ['date', 'date', 'allowEmpty' => true, 'format' => 'yyyy-MM-dd'],
            ['date', 'default', 'value' => null, 'setOnEmpty' => true],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('OfferModule.offer', 'Bad characters in {attribute} field'),
            ],
            ['description, text', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, title, slug, image, image_splash, description, coupons, text, date, status, seo_title, seo_keywords, seo_description', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    public function tariff($id)
    {
        $criteria = new CDbCriteria();

        $criteria->join = 'INNER JOIN {{offer_item}} i ON i.offer_id = `t`.`id`';
        $criteria->compare('i.tariff_id', $id);
//        $criteria->group = 't.id';

        $this->getDbCriteria()->mergeWith($criteria);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('offer');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/offer/offer/view',
                'params' => [
                    'slug' => function (Offer $data) {
                        return $data->slug;
                    },
                ],
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
//                'defaultImage' => $module->defaultImage,
                'resizeOnUpload' => false,
            ],
            'imageSplashUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image_splash',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
//                'defaultImage' => $module->defaultImage,
                'resizeOnUpload' => false,
                'deleteFileKey' => 'delete-file-2',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'items' => [self::HAS_MANY, 'OfferItem', 'offer_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'title' => 'Заголовок',
            'slug' => 'URL',
            'image' => 'Изображение',
            'image_splash' => 'Изображение (в списке)',
            'coupons' => 'Кол-во промокодов',
            'description' => 'Короткое описание',
            'text' => 'Текст',
            'date' => 'Действует до',
            'status' => 'Статус',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.image_splash', $this->image_splash, true);
        $criteria->compare($this->tableAlias . '.coupons', $this->coupons);
        $criteria->compare($this->tableAlias . '.description', $this->description, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date ASC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Offer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('OfferModule.offer', 'Draft'),
            self::STATUS_MODERATION => Yii::t('OfferModule.offer', 'On moderation'),
            self::STATUS_PUBLISHED => Yii::t('OfferModule.offer', 'Published'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('OfferModule.offer', '*unknown*');
    }

    public function getMaxBonus(Tariff $tariff = null)
    {
        return $this->items ? max(array_map(function (OfferItem $item) use ($tariff) {
            return $tariff ? ($item->tariff_id == $tariff->id ? $item->bonus : 0) : $item->bonus;
        }, $this->items)) : 0;
    }
}
