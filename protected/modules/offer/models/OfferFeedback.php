<?php

/**
 * This is the model class for table "{{offer_feedback}}".
 *
 * The followings are the available columns in table '{{offer_feedback}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $name
 * @property string $email
 * @property string $site
 * @property string $text
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property OfferFeedbackFile[] $files
 */
class OfferFeedback extends yupe\models\YModel
{
    const STATUS_NEW = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_DONE = 2;
    const STATUS_DENIED = 3;

    public $agree;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{offer_feedback}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, email, site, text', 'filter', 'filter' => 'trim'],
            ['name, email, site, text', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['name, email', 'required'],
            ['status', 'numerical', 'integerOnly' => true],
            ['name, email, site', 'length', 'max' => 255],
            ['email', 'email'],
            ['site', 'url'],
            ['text', 'safe'],
            [
                'agree',
                'compare',
                'allowEmpty' => false,
                'compareValue' => '1',
                'message' => 'Вы должны согласиться с условиями',
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, name, email, site, text, status', 'safe', 'on' => 'search'],
        ];
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('offer');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'files' => [self::HAS_MANY, 'OfferFeedbackFile', 'feedback_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'name' => 'Как вас зовут',
            'email' => 'Email для связи',
            'site' => 'Ссылка на сайт компании',
            'text' => 'Сообщение',
            'status' => 'Статус',
            'agree' => 'Я соглашаюсь на передачу персональных данных согласно <a href="https://tolkunov.com/view/privacy.pdf" target="_blank">политике конфиденциальности</a> и <a href="https://tolkunov.com/view/terms.pdf" target="_blank">пользовательскому соглашению</a>',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.email', $this->email, true);
        $criteria->compare($this->tableAlias . '.site', $this->site, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.create_time DESC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OfferFeedback the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PROCESSING => 'В обработке',
            self::STATUS_DONE => 'Обработано',
            self::STATUS_DENIED => 'Отклонено',
        ];
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }
}
