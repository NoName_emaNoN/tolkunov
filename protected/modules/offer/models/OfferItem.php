<?php

/**
 * This is the model class for table "{{offer_item}}".
 *
 * The followings are the available columns in table '{{offer_item}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $offer_id
 * @property integer $tariff_id
 * @property string $description
 * @property integer $bonus
 *
 * The followings are the available model relations:
 * @property Offer $offer
 * @property Tariff $tariff
 */
class OfferItem extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{offer_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['offer_id, tariff_id', 'required'],
            ['offer_id, tariff_id, bonus', 'numerical', 'integerOnly' => true],
            ['offer_id', 'exist', 'className' => 'Offer', 'attributeName' => 'id'],
            ['tariff_id', 'exist', 'className' => 'Tariff', 'attributeName' => 'id'],
            ['description', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, offer_id, tariff_id, description, bonus', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('offer');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'offer' => [self::BELONGS_TO, 'Offer', 'offer_id'],
            'tariff' => [self::BELONGS_TO, 'Tariff', 'tariff_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'offer_id' => 'Акция',
            'tariff_id' => 'Тариф',
            'description' => 'Описание',
            'bonus' => 'Бонус',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.offer_id', $this->offer_id);
        $criteria->compare($this->tableAlias . '.tariff_id', $this->tariff_id);
        $criteria->compare($this->tableAlias . '.description', $this->description, true);
        $criteria->compare($this->tableAlias . '.bonus', $this->bonus);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OfferItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getMaxBonusForTarif($tariff_id)
    {
        return Yii::app()->db->createCommand('SELECT MAX(bonus) FROM {{offer_item}} WHERE tariff_id = :tariff_id')->queryScalar([':tariff_id' => $tariff_id]);
    }

    public function getMaxBonus()
    {
        return Yii::app()->db->createCommand('SELECT MAX(bonus) FROM {{offer_item}}')->queryScalar();
    }
}
