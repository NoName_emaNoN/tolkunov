<?php
/**
 * Файл настроек для модуля offer
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.offer.install
 * @since 0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.offer.OfferModule',
    ],
    'import' => [
        'application.modules.offer.listeners.*',
        'application.modules.offer.models.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\OfferSitemapGeneratorListener', 'onGenerate'],
                ],
            ],
        ],
    ],
    'rules' => [
        '/actions/create' => '/offer/offer/create',
        '/price' => '/offer/offer/price',
        '/actions' => '/offer/offer/index',
        '/actions/<slug:[\w-]+>' => [
            '/offer/offer/category',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{tariff_tariff}}',
                    'field' => 'slug',
                ],
            ],
        ],
        '/action/<slug:[\w-]+>' => [
            '/offer/offer/view',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{offer_offer}}',
                    'field' => 'slug',
                ],
            ],
        ],
    ],
];