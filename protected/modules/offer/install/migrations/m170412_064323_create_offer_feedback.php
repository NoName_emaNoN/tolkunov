<?php

class m170412_064323_create_offer_feedback extends yupe\components\DbMigration
{
    public function safeUp()
    {
        /* Offer Feedback */
        $this->createTable(
            '{{offer_feedback}}',
            [
                'id' => 'pk',
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'name' => 'string',
                'email' => 'string',
                'site' => 'string',
                'text' => 'text',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{offer_feedback}}_create_time", '{{offer_feedback}}', "create_time", false);
        $this->createIndex("ix_{{offer_feedback}}_update_time", '{{offer_feedback}}', "update_time", false);
        $this->createIndex("ix_{{offer_feedback}}_status", '{{offer_feedback}}', "status", false);

        /* Offer Feedback Files */
        $this->createTable(
            '{{offer_feedback_file}}',
            [
                'id' => 'pk',
                'feedback_id' => 'integer NOT NULL',
                'file' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{offer_feedback_file}}_feedback_id", '{{offer_feedback_file}}', "feedback_id", false);

        //fk
        $this->addForeignKey('fk_{{offer_feedback_file}}_feedback_id', '{{offer_feedback_file}}', 'feedback_id', '{{offer_feedback}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{offer_feedback_file}}');
        $this->dropTableWithForeignKeys('{{offer_feedback}}');
    }
}
