<?php

/**
 * Offer install migration
 * Класс миграций для модуля Offer:
 *
 * @category YupeMigration
 * @package  yupe.modules.offer.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_offer_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{offer_offer}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'image' => 'string',
                'description' => 'text',
                'text' => 'text',
                'date' => 'DATE NOT NULL',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'seo_title' => 'string',
                'seo_keywords' => 'string',
                'seo_description' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{offer_offer}}_create_time", '{{offer_offer}}', "create_time", false);
        $this->createIndex("ix_{{offer_offer}}_update_time", '{{offer_offer}}', "update_time", false);
        $this->createIndex("ix_{{offer_offer}}_status", '{{offer_offer}}', "status", false);
        $this->createIndex("ix_{{offer_offer}}_date", '{{offer_offer}}', "date", false);
        $this->createIndex("ix_{{offer_offer}}_slug", '{{offer_offer}}', "slug", true);

        $this->createTable('{{offer_item}}', [
            'id' => 'pk',
            //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
            'create_time' => 'datetime NOT NULL',
            'update_time' => 'datetime NOT NULL',
            'offer_id' => 'integer NOT NULL',
            'tariff' => 'integer NOT NULL',
            'description' => 'text',
            'bonus' => 'integer NOT NULL DEFAULT 0',
        ], $this->getOptions());

        //ix
        $this->createIndex("ix_{{offer_item}}_offer_id", '{{offer_item}}', "offer_id", false);
        $this->createIndex("ix_{{offer_item}}_tariff", '{{offer_item}}', "tariff", false);

        //fk
        $this->addForeignKey('fk_{{offer_item}}_offer_id', '{{offer_item}}', 'offer_id', '{{offer_offer}}', 'id', 'CASCADE');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{offer_item}}');
        $this->dropTableWithForeignKeys('{{offer_offer}}');
    }
}
