<?php

class m170317_193301_add_tariff_id extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->dropIndex("ix_{{offer_item}}_tariff", '{{offer_item}}');

        $this->renameColumn('{{offer_item}}', 'tariff', 'tariff_id');

        $this->createIndex("ix_{{offer_item}}_tariff_id", '{{offer_item}}', "tariff_id", false);

        //fk
        $this->addForeignKey('fk_{{offer_item}}_tariff_id', '{{offer_item}}', 'tariff_id', '{{tariff_tariff}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_{{offer_item}}_tariff_id', '{{offer_item}}');
        $this->dropIndex("ix_{{offer_item}}_tariff_id", '{{offer_item}}');
        $this->renameColumn('{{offer_item}}', 'tariff_id', 'tariff');
        $this->createIndex("ix_{{offer_item}}_tariff", '{{offer_item}}', "tariff", false);
    }
}
