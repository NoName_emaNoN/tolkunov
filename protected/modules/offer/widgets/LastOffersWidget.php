<?php

/**
 * Class LastOffersWidget
 */
class LastOffersWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'last_offers_widget';

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = (int)$this->limit;

        $models = Offer::model()->published()->findAll($criteria);

        $this->render($this->view, ['models' => $models]);
    }
}
