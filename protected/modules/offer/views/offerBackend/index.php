<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Offer
 * @var $this OfferBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OfferModule.offer', 'Акции') => ['/offer/offerBackend/index'],
    Yii::t('OfferModule.offer', 'Управление'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Акции - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OfferModule.offer', 'Управление акциями'), 'url' => ['/offer/offerBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OfferModule.offer', 'Добавить акцию'), 'url' => ['/offer/offerBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('OfferModule.offer', 'Акции'); ?>
        <small><?= Yii::t('OfferModule.offer', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('OfferModule.offer', 'Поиск акций'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('offer-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('OfferModule.offer', 'В данном разделе представлены средства управления акциями'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'offer-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '80px'],
            ],
            [
                'type' => 'raw',
                'name' => 'image',
                'filter' => false,
                'value' => function (Offer $data) {
                    return CHtml::image($data->getImageUrl(80, 80), CHtml::encode($data->title), ["width" => 80, "height" => 80, "class" => "img-thumbnail"]);
                },
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            'date',
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'coupons',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'coupons', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_PUBLISHED => ['class' => 'label-success'],
                    $model::STATUS_MODERATION => ['class' => 'label-warning'],
                    $model::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            'create_time',
            'update_time',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'frontViewButtonUrl' => function ($data) {
                    return $data->url;
                },
                'buttons' => [
                    'front_view' => [
                        'visible' => function ($row, $data) {
                            return $data->status == $data::STATUS_PUBLISHED;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
