<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Offer
 *   @var $this OfferBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OfferModule.offer', 'Акции') => ['/offer/offerBackend/index'],
    Yii::t('OfferModule.offer', 'Добавление'),
];

$this->pageTitle = Yii::t('OfferModule.offer', 'Акции - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OfferModule.offer', 'Управление акциями'), 'url' => ['/offer/offerBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('OfferModule.offer', 'Добавить акцию'), 'url' => ['/offer/offerBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('OfferModule.offer', 'Акции'); ?>
        <small><?=  Yii::t('OfferModule.offer', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>