<?php

use yupe\components\WebModule;

/**
 * OfferModule основной класс модуля offer
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.offer
 * @since 0.1
 */
class OfferModule extends WebModule
{
    const VERSION = '1.0';

    /**
     * @var string
     */
    public $uploadPath = 'offer';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'tariff',
        ];
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array|false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'OfferModule.offer',
                    'Directory "{dir}" is not writeable! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('OfferModule.offer', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'offer',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('OfferModule.offer', 'Content');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('OfferModule.offer', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'OfferModule.offer',
                'File uploads directory (relative to "{path}")',
                ['{path}' => Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule("yupe")->uploadPath]
            ),
            'allowedExtensions' => Yii::t('OfferModule.offer', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('OfferModule.offer', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('OfferModule.offer', 'Maximum size (in bytes)'),
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('OfferModule.offer', 'Акции')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('OfferModule.offer', 'Список акций'),
                'url' => ['/offer/offerBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('OfferModule.offer', 'Добавить акцию'),
                'url' => ['/offer/offerBackend/create'],
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('OfferModule.offer', self::VERSION);
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('OfferModule.offer', 'Акции');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('OfferModule.offer', 'Описание модуля "Акции"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('OfferModule.offer', 'Mikhail Chemezov');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('OfferModule.offer', 'michlenanosoft@gmail.com');
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('OfferModule.offer', 'http://zexed.net');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/offer/offerBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-gift";
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'offer.models.*',
                'offer.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Offer.OfferManager',
                'description' => Yii::t('OfferModule.offer', 'Manage offer'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Create',
                        'description' => Yii::t('OfferModule.offer', 'Create'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Delete',
                        'description' => Yii::t('OfferModule.offer', 'Delete'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Index',
                        'description' => Yii::t('OfferModule.offer', 'Index'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.Update',
                        'description' => Yii::t('OfferModule.offer', 'Update'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Offer.OfferBackend.View',
                        'description' => Yii::t('OfferModule.offer', 'View'),
                    ],
                ],
            ],
        ];
    }
}
