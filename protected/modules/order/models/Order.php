<?php

/**
 * This is the model class for table "{{order_order}}".
 *
 * The followings are the available columns in table '{{order_order}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property integer $user_status
 * @property string $user
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $text_1
 * @property string $text_2
 * @property string $text_3
 * @property string $text_4
 * @property string $text_5
 * @property string $text_6
 * @property string $tariff
 * @property integer $portfolio_id
 *
 * The followings are the available model relations:
 * @property OrderFile[] $files
 * @property Portfolio $portfolio
 */
class Order extends yupe\models\YModel
{
    const USER_STATUS_OPENED = 1;
    const USER_STATUS_CLOSED = 2;

    const STATUS_NEW = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_DONE = 2;
    const STATUS_DENIED = 3;

    public $agree;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{order_order}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, email, phone, tariff, text_1, text_2, text_3, text_4, text_5, text_6', 'filter', 'filter' => 'trim'],
            ['name, email, phone, tariff, text_1, text_2, text_3, text_4, text_5, text_6', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['user', 'required'],
            ['name, email', 'required', 'on' => 'step_1'],
            ['status, user_status, portfolio_id', 'numerical', 'integerOnly' => true],
            ['user, name, email, phone, tariff', 'length', 'max' => 255],
            ['text_1, text_2, text_3, text_4, text_5, text_6', 'safe'],
            ['portfolio_id', 'exist', 'className' => 'Portfolio', 'attributeName' => 'id'],
            ['email', 'email'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['user_status', 'in', 'range' => array_keys($this->getUserStatusList())],
            [
                'agree',
                'compare',
                'allowEmpty' => false,
                'compareValue' => '1',
                'message' => 'Вы должны согласиться с условиями',
                'on' => 'step_2',
            ],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, status, user, name, email, phone, text_1, text_2, text_3, text_4, text_5, text_6, tariff, portfolio_id', 'safe', 'on' => 'search'],
        ];
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('order');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'files' => [self::HAS_MANY, 'OrderFile', 'order_id'],
            'portfolio' => [self::BELONGS_TO, 'Portfolio', 'portfolio_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'user_status' => 'Статус заполнения',
            'status' => 'Статус',
            'user' => 'Идентификатор пользователя',
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Номер телефона',
            'text_1' => 'Что рекламируем и кто целевая аудитория',
            'text_2' => 'Типы и размеры баннеров',
            'text_3' => 'Содержимое баннеров',
            'text_4' => 'Стиль баннеров',
            'text_5' => 'Площадки размещения',
            'text_6' => 'Предпочтительный способ оплаты',
            'tariff' => 'Выбранный тариф',
            'portfolio_id' => 'Выбранное портфолио',
            'agree' => 'Я соглашаюсь на передачу персональных данных согласно <br><a href="https://tolkunov.com/view/privacy.pdf" target="_blank">политике конфиденциальности</a> и <a href="https://tolkunov.com/view/terms.pdf" target="_blank">пользовательскому соглашению</a>',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.user_status', $this->user_status);
        $criteria->compare($this->tableAlias . '.user', $this->user, true);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.email', $this->email, true);
        $criteria->compare($this->tableAlias . '.phone', $this->phone, true);
        $criteria->compare($this->tableAlias . '.text_1', $this->text_1, true);
        $criteria->compare($this->tableAlias . '.text_2', $this->text_2, true);
        $criteria->compare($this->tableAlias . '.text_3', $this->text_3, true);
        $criteria->compare($this->tableAlias . '.text_4', $this->text_4, true);
        $criteria->compare($this->tableAlias . '.text_5', $this->text_5, true);
        $criteria->compare($this->tableAlias . '.text_6', $this->text_6, true);
        $criteria->compare($this->tableAlias . '.tariff', $this->tariff, true);
        $criteria->compare($this->tableAlias . '.portfolio_id', $this->portfolio_id);

        $criteria->addCondition($this->tableAlias . '.name != "" AND ' . $this->tableAlias . '.email != ""');

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.create_time DESC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_PROCESSING => 'В обработке',
            self::STATUS_DONE => 'Обработано',
            self::STATUS_DENIED => 'Отклонено',
        ];
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }

    public function getUserStatusList()
    {
        return [
            self::USER_STATUS_OPENED => 'Открыт',
            self::USER_STATUS_CLOSED => 'Закрыт',
        ];
    }

    public function getUserStatus()
    {
        $data = $this->getUserStatusList();

        return isset($data[$this->user_status]) ? $data[$this->user_status] : '*unknown*';
    }

    /**
     * @param string $uid
     * @param bool $create
     * @return Order|null
     */
    public static function getOrderByUser($uid, $create = false)
    {
        $order = Order::model()->find(
            'user = :uid AND user_status = :status',
            [':uid' => $uid, ':status' => Order::USER_STATUS_OPENED]
        );

        if (empty($order) && $create) {
            $order = new Order();
            $order->user = $uid;
            $order->user_status = Order::USER_STATUS_OPENED;
            $order->save();
        }

        return $order;
    }
}
