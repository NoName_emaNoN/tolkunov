<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Order
 * @var $this OrderBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('OrderModule.order', 'Заказы') => ['/order/orderBackend/index'],
    Yii::t('OrderModule.order', 'Управление'),
];

$this->pageTitle = Yii::t('OrderModule.order', 'Заказы - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('OrderModule.order', 'Управление заказами'), 'url' => ['/order/orderBackend/index']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('OrderModule.order', 'Заказы'); ?>
        <small><?= Yii::t('OrderModule.order', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('OrderModule.order', 'Поиск заказов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('order-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('OrderModule.order', 'В данном разделе представлены средства управления заказами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'order-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            'create_time',
            'update_time',
//            'user',
            'name',
            'email',
            'phone',
//            'text_1',
//            'text_2',
//            'text_3',
//            'text_4',
//            'text_5',
//            'text_6',
//            'tariff',
//            'portfolio_id',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'user_status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getUserStatusList(),
                'options' => [
                    $model::USER_STATUS_OPENED => ['class' => 'label-default'],
                    $model::USER_STATUS_CLOSED => ['class' => 'label-success'],
                ],
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_NEW => ['class' => 'label-default'],
                    $model::STATUS_PROCESSING => ['class' => 'label-primary'],
                    $model::STATUS_DONE => ['class' => 'label-success'],
                    $model::STATUS_DENIED => ['class' => 'label-danger'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'buttons' => [
                    'update' => [
                        'visible' => function ($row, $data) {
                            return false;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
