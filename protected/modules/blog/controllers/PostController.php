<?php

/**
 * PostController контроллер для постов на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.blog.controllers
 * @since 0.1
 *
 */
class PostController extends \yupe\components\controllers\FrontController
{

    /**
     *
     */
    public function actionIndex()
    {
        $model = Post::model();

        $dataProvider = $model->allPosts();
        $dataProvider->pagination = false;
        
        $this->render('index', ['model' => Post::model(), 'dataProvider' => $dataProvider]);
    }

    /**
     * Показываем пост по урлу
     *
     * @param  string $slug - урл поста
     * @throws CHttpException
     * @return void
     */
    public function actionView($slug)
    {
        $post = Post::model()->get($slug, ['blog', 'createUser', 'comments.author']);

        if (null === $post) {
            throw new CHttpException(404, Yii::t('BlogModule.blog', 'Post was not found!'));
        }

        preg_match_all('/{gallery_(\d+)}/', $post->content, $matches);

        if (count($matches[0]) > 0) {
            foreach ($matches[1] as $key => $gallery_id) {
                $post->content = str_replace($matches[0][$key], $this->renderEntity('Gallery', $gallery_id),
                    $post->content);
            }
        }

        $this->render('view', ['post' => $post]);

        Yii::app()->db->createCommand('UPDATE ' . $post->tableName() . ' SET views = views + 1 WHERE id = :id')->bindValue(':id',
            $post->id, PDO::PARAM_INT)->execute();
    }

    /**
     * Показываем посты по тегу
     *
     * @param  string $tag - Tag поста
     * @throws CHttpException
     * @return void
     */
    public function actionTag($tag)
    {
        $tag = CHtml::encode($tag);

        $posts = Post::model()->getByTag($tag);

        if (empty($posts)) {
            throw new CHttpException(404, Yii::t('BlogModule.blog', 'Posts not found!'));
        }

        $this->render('tag', ['posts' => $posts, 'tag' => $tag]);
    }

    /**
     * @param  string $tag - Tag поста
     * @throws CHttpException
     * @return void
     */
    public function actionSearch($q)
    {
        $query = array_diff(array_map('trim', explode(' ', $q)), ['']);

        $criteria = new CDbCriteria();

        foreach ($query as $queryItem) {
            $searchCriteria = new CDbCriteria();

            $searchCriteria->addSearchCondition('title', $queryItem, true, 'OR');
            $searchCriteria->addSearchCondition('content', $queryItem, true, 'OR');
            $searchCriteria->addSearchCondition('quote', $queryItem, true, 'OR');

            $criteria->mergeWith($searchCriteria);
        }

        $dataProvider = new CActiveDataProvider(Post::model()->public()->published(), [
            'criteria' => $criteria,
            'pagination' => false,
        ]);

        $this->render('search', ['dataProvider' => $dataProvider, 'q' => $q]);
    }

    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionBlog($slug)
    {
        $blog = Blog::model()->getByUrl($slug)->find();

        if (null === $blog) {
            throw new CHttpException(404);
        }

        $this->render('blog-post', ['target' => $blog, 'posts' => $blog->getPosts()]);
    }

    /**
     * @param $slug
     * @throws CHttpException
     */
    public function actionCategory($slug)
    {
        $category = Category::model()->getByAlias($slug);

        if (null === $category) {
            throw new CHttpException(404, Yii::t('BlogModule.blog', 'Page was not found!'));
        }

        $posts = Post::model()->getForCategory($category->id);

        $dataProvider = $posts->search();
        $dataProvider->pagination = false;

        $this->render(
            'category-post',
            [
                'target' => $category,
                'posts' => $posts,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     *
     */
    public function actionCategories()
    {
        $this->render('categories', ['categories' => Post::model()->getCategories()]);
    }

    public function renderEntity($modelClass, $id)
    {
        Yii::import('application.modules.' . strtolower($modelClass) . '.models.' . $modelClass, true);

        $model = CActiveRecord::model($modelClass)->findByPk($id);

        if (!$model) {
            return false;
        }

        return $this->renderPartial('_' . strtolower($modelClass), ['model' => $model], true);
    }
}
