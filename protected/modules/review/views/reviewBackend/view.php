<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Review
 *   @var $this ReviewBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Отзывы') => ['/review/reviewBackend/index'],
    $model->id,
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Отзывы - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление отзывами'), 'url' => ['/review/reviewBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить отзыв'), 'url' => ['/review/reviewBackend/create']],
    ['label' => Yii::t('ReviewModule.review', 'Отзыв') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ReviewModule.review', 'Редактирование отзыва'), 'url' => [
        '/review/reviewBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ReviewModule.review', 'Просмотреть отзыв'), 'url' => [
        '/review/reviewBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ReviewModule.review', 'Удалить отзыв'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/review/reviewBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ReviewModule.review', 'Вы уверены, что хотите удалить отзыв?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ReviewModule.review', 'Просмотр') . ' ' . Yii::t('ReviewModule.review', 'отзыва'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        'type',
        'client_id',
        'date',
        'social_network_id',
        'forum_id',
        'status',
        'text',
        'video_link',
        'image',
        'link',
    ],
]); ?>
