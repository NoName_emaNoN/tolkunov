<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model ReviewForum
 *   @var $this ReviewForumBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Форумы') => ['/review/reviewForumBackend/index'],
    $model->title => ['/review/reviewForumBackend/view', 'id' => $model->id],
    Yii::t('ReviewModule.review', 'Редактирование'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Форумы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление форумами'), 'url' => ['/review/reviewForumBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить форум'), 'url' => ['/review/reviewForumBackend/create']],
    ['label' => Yii::t('ReviewModule.review', 'Форум') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('ReviewModule.review', 'Редактирование форума'), 'url' => [
        '/review/reviewForumBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('ReviewModule.review', 'Просмотреть форум'), 'url' => [
        '/review/reviewForumBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('ReviewModule.review', 'Удалить форум'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/review/reviewForumBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('ReviewModule.review', 'Вы уверены, что хотите удалить форум?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ReviewModule.review', 'Редактирование') . ' ' . Yii::t('ReviewModule.review', 'форума'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>