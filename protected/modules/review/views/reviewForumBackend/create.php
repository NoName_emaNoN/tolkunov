<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model ReviewForum
 *   @var $this ReviewForumBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('ReviewModule.review', 'Форумы') => ['/review/reviewForumBackend/index'],
    Yii::t('ReviewModule.review', 'Добавление'),
];

$this->pageTitle = Yii::t('ReviewModule.review', 'Форумы - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('ReviewModule.review', 'Управление форумами'), 'url' => ['/review/reviewForumBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('ReviewModule.review', 'Добавить форум'), 'url' => ['/review/reviewForumBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('ReviewModule.review', 'Форумы'); ?>
        <small><?=  Yii::t('ReviewModule.review', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>