<?php

/**
 * This is the model class for table "{{review_review}}".
 *
 * The followings are the available columns in table '{{review_review}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $type
 * @property integer $client_id
 * @property string $date
 * @property integer $social_network_id
 * @property integer $forum_id
 * @property integer $status
 * @property string $text
 * @property string $video_link
 * @property string $image
 * @property string $pattern
 * @property string $link
 *
 * The followings are the available model relations:
 * @property ReviewForum $forum
 * @property Client $client
 * @property ReviewSocialNetwork $socialNetwork
 *
 * @method Review published()
 */
class Review extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    const TYPE_VIDEO = 1;
    const TYPE_MAIL = 2;
    const TYPE_SOCIAL = 3;
    const TYPE_FL = 4;
    const TYPE_FORUM = 5;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{review_review}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['type, client_id, date', 'required'],
            ['type, client_id, social_network_id, forum_id, status', 'numerical', 'integerOnly' => true],
            ['client_id', 'exist', 'className' => 'Client', 'attributeName' => 'id'],
            ['social_network_id', 'exist', 'className' => 'ReviewSocialNetwork', 'attributeName' => 'id'],
            ['forum_id', 'exist', 'className' => 'ReviewForum', 'attributeName' => 'id'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['type', 'in', 'range' => array_keys($this->getTypeList())],
            ['video_link, link', 'length', 'max' => 255],
            ['video_link, link', 'url'],
            ['text', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, type, client_id, date, social_network_id, forum_id, status, text, video_link, image, pattern, link', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    public function type($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.type = :type',
            'params' => [':type' => $id],
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('review');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
            ],
            'patternUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'pattern',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => 'delete-file-2',
//                'defaultImage' => $module->defaultImage,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'forum' => [self::BELONGS_TO, 'ReviewForum', 'forum_id'],
            'client' => [self::BELONGS_TO, 'Client', 'client_id'],
            'socialNetwork' => [self::BELONGS_TO, 'ReviewSocialNetwork', 'social_network_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'type' => 'Тип отзыва',
            'client_id' => 'Клиент',
            'date' => 'Дата',
            'social_network_id' => 'Социальная сеть',
            'forum_id' => 'Форум',
            'status' => 'Статус',
            'text' => 'Текст отзыва',
            'video_link' => 'Ссылка на видео',
            'image' => 'Изображение',
            'pattern' => 'Паттерн',
            'link' => 'Ссылка на оригинал отзыва',
        ];
    }

    public function attributeDescriptions()
    {
        return [
            'video_link' => 'Для видеоотзывов. Пример: <code>http://www.youtube.com/watch?v=opj24KnzrWo</code>',
            'link' => 'Ссылка на оригинал отзыва. Пример: <code>https://vk.com/topic-68302756_30037271?post=3</code>',
        ];
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.client_id', $this->client_id);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.social_network_id', $this->social_network_id);
        $criteria->compare($this->tableAlias . '.forum_id', $this->forum_id);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.video_link', $this->video_link, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.pattern', $this->pattern, true);
        $criteria->compare($this->tableAlias . '.link', $this->link, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date DESC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Review the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('ReviewModule.review', 'Draft'),
            self::STATUS_PUBLISHED => Yii::t('ReviewModule.review', 'Published'),
            self::STATUS_MODERATION => Yii::t('ReviewModule.review', 'On moderation'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('ReviewModule.review', '*unknown*');
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_VIDEO => Yii::t('ReviewModule.review', 'Видеоотзывы'),
            self::TYPE_MAIL => Yii::t('ReviewModule.review', 'Рекомендации'),
            self::TYPE_SOCIAL => Yii::t('ReviewModule.review', 'Соцсети'),
            self::TYPE_FL => Yii::t('ReviewModule.review', 'FL'),
            self::TYPE_FORUM => Yii::t('ReviewModule.review', 'Форумы'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $data = $this->getTypeList();

        return isset($data[$this->type]) ? $data[$this->type] : Yii::t('ReviewModule.review', '*unknown*');
    }

    public function getTypeStringList()
    {
        return [
            self::TYPE_VIDEO => 'video',
            self::TYPE_MAIL => 'mail',
            self::TYPE_SOCIAL => 'social',
            self::TYPE_FL => 'fl',
            self::TYPE_FORUM => 'forums',
        ];
    }

    public function getTypeString()
    {
        $data = $this->getTypeStringList();

        return isset($data[$this->type]) ? $data[$this->type] : null;
    }

    public function getVideoThumbnail()
    {
        if (!$this->video_link) {
            return null;
        }

        parse_str(parse_url($this->video_link, PHP_URL_QUERY), $query);

        if (!array_key_exists('v', $query)) {
            return null;
        }

        return 'https://img.youtube.com/vi/' . $query['v'] . '/hqdefault.jpg';
    }
}
