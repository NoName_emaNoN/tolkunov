<?php

use yupe\components\WebModule;

/**
 * ReviewModule основной класс модуля review
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.review
 * @since 0.1
 */
class ReviewModule extends WebModule
{
    const VERSION = '1.0';

    /**
     * @var string
     */
    public $uploadPath = 'review';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['client'];
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array|false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'ReviewModule.review',
                    'Directory "{dir}" is not writeable! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('ReviewModule.review', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'review',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('ReviewModule.review', 'Content');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('NewsModule.news', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'ReviewModule.review',
                'File uploads directory (relative to "{path}")',
                ['{path}' => Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule("yupe")->uploadPath]
            ),
            'allowedExtensions' => Yii::t('NewsModule.news', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('NewsModule.news', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('NewsModule.news', 'Maximum size (in bytes)'),
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('ReviewModule.review', 'Отзывы')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ReviewModule.review', 'Список отзывов'),
                'url' => ['/review/reviewBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ReviewModule.review', 'Добавить отзыв'),
                'url' => ['/review/reviewBackend/create'],
            ],
            '',
            ['label' => Yii::t('ReviewModule.review', 'Социальные сети')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ReviewModule.review', 'Список социальных сетей'),
                'url' => ['/review/reviewSocialNetworkBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ReviewModule.review', 'Добавить социальную сеть'),
                'url' => ['/review/reviewSocialNetworkBackend/create'],
            ],
            '',
            ['label' => Yii::t('ReviewModule.review', 'Форумы')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('ReviewModule.review', 'Список форумов'),
                'url' => ['/review/reviewForumBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('ReviewModule.review', 'Добавить форум'),
                'url' => ['/review/reviewForumBackend/create'],
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('ReviewModule.review', self::VERSION);
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('ReviewModule.review', 'Отзывы');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('ReviewModule.review', 'Описание модуля "Отзывы"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('ReviewModule.review', 'Михаил Чемезов');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('ReviewModule.review', 'michlenanosoft@gmail.com');
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('ReviewModule.review', 'http://zexed.net');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/review/reviewBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-trophy";
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'review.models.*',
                'review.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Review.ReviewManager',
                'description' => Yii::t('ReviewModule.review', 'Manage review'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Review.ReviewBackend.Create',
                        'description' => Yii::t('ReviewModule.review', 'Create'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Review.ReviewBackend.Delete',
                        'description' => Yii::t('ReviewModule.review', 'Delete'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Review.ReviewBackend.Index',
                        'description' => Yii::t('ReviewModule.review', 'Index'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Review.ReviewBackend.Update',
                        'description' => Yii::t('ReviewModule.review', 'Update'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Review.ReviewBackend.View',
                        'description' => Yii::t('ReviewModule.review', 'View'),
                    ],
                ],
            ],
        ];
    }
}
