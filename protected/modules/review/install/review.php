<?php
/**
 * Файл настроек для модуля review
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.review.install
 * @since 0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.review.ReviewModule',
    ],
    'import' => [
        'application.modules.review.models.*',
    ],
    'component' => [],
    'rules' => [
//        '/reviews/<type:(video|mail|social|fl|forums)>' => '/review/review/index',
        '/reviews' => '/review/review/index',
    ],
];
