<?php

/**
 * ReviewController контроллер для review на публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.review.controllers
 * @since 0.1
 *
 */
class ReviewController extends \yupe\components\controllers\FrontController
{
    public function behaviors()
    {
        return [
            'seo' => ['class' => 'vendor.chemezov.yii-seo.behaviors.SeoBehavior'],
        ];
    }

    public function actionIndex()
    {
        $model = new Review('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Review'])) {
            $model->attributes = $_GET['Review'];
        }
        $model->status = Review::STATUS_PUBLISHED;

        //send model object for search
        $dataProvider = $model->search();
        $dataProvider->pagination = [
//            'class' => 'Pagination',
            'pageSize' => 20,
        ];

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('index', [
                    'dataProvider' => $dataProvider,
                    'model' => $model,
                ]
            );
        }

        $this->render('index', [
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]
        );
    }
}
