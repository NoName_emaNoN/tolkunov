<?php

/**
 * Class LastNewsWidget
 */
class LastReviewWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'last_review_widget';

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = (int)$this->limit;
        $criteria->order = 'date DESC';

        $models = Review::model()->published()->findAll($criteria);

        $this->render($this->view, ['models' => $models]);
    }
}
