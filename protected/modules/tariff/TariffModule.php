<?php

use yupe\components\WebModule;

/**
 * TariffModule основной класс модуля tariff
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.tariff
 * @since 0.1
 */
class TariffModule extends WebModule
{
    const VERSION = '1.0';

    /**
     * @var string
     */
    public $uploadPath = 'tariff';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['portfolio'];
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array|false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'TariffModule.tariff',
                    'Directory "{dir}" is not writeable! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('TariffModule.tariff', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'tariff',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('TariffModule.tariff', 'Structure');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('TariffModule.tariff', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'TariffModule.tariff',
                'File uploads directory (relative to "{path}")',
                ['{path}' => Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule("yupe")->uploadPath]
            ),
            'allowedExtensions' => Yii::t('TariffModule.tariff', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('TariffModule.tariff', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('TariffModule.tariff', 'Maximum size (in bytes)'),
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('TariffModule.tariff', 'Тарифы')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TariffModule.tariff', 'Список тарифов'),
                'url' => ['/tariff/tariffBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TariffModule.tariff', 'Добавить тариф'),
                'url' => ['/tariff/tariffBackend/create'],
            ],
            '',
            ['label' => Yii::t('TariffModule.tariff', 'Особенности тарифа')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TariffModule.tariff', 'Список особенностей'),
                'url' => ['/tariff/tariffPropertyBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TariffModule.tariff', 'Добавить особенность'),
                'url' => ['/tariff/tariffPropertyBackend/create'],
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('TariffModule.tariff', self::VERSION);
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('TariffModule.tariff', 'Тарифы');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('TariffModule.tariff', 'Описание модуля "Тарифы"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('TariffModule.tariff', 'Михаил Чемезов');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('TariffModule.tariff', 'michlenanosoft@gmail.com');
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('TariffModule.tariff', 'http://zexed.net');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/tariff/tariffBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-rub";
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'tariff.models.*',
                'tariff.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Tariff.TariffManager',
                'description' => Yii::t('TariffModule.tariff', 'Manage tariff'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Tariff.TariffBackend.Create',
                        'description' => Yii::t('TariffModule.tariff', 'Create'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Tariff.TariffBackend.Delete',
                        'description' => Yii::t('TariffModule.tariff', 'Delete'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Tariff.TariffBackend.Index',
                        'description' => Yii::t('TariffModule.tariff', 'Index'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Tariff.TariffBackend.Update',
                        'description' => Yii::t('TariffModule.tariff', 'Update'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Tariff.TariffBackend.View',
                        'description' => Yii::t('TariffModule.tariff', 'View'),
                    ],
                ],
            ],
        ];
    }
}
