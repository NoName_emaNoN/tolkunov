<?php
/**
 * Файл настроек для модуля tariff
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.tariff.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.tariff.TariffModule',
    ],
    'import'    => [
        'application.modules.tariff.models.*',
    ],
    'component' => [],
    'rules'     => [
//        '/tariff' => '/tariff/tariff/index',
//        '/tariff/<slug:[\w-]+>' => [
//            '/tariff/tariff/view',
//            'type' => 'db',
//            'fields' => [
//                'slug' => [
//                    'table' => '{{tariff_tariff}}',
//                    'field' => 'slug',
//                ],
//            ],
//        ],
    ],
];