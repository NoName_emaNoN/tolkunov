<?php

/**
 * Класс TariffPropertyBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class TariffPropertyBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Tariff.TariffPropertyBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Tariff.TariffPropertyBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Tariff.TariffPropertyBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Tariff.TariffPropertyBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Tariff.TariffPropertyBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'TariffProperty',
                'validAttributes' => ['title', 'description', 'status', 'tariff_id'],
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'TariffProperty',
                'attribute' => 'sort',
            ],
        ];
    }

    /**
     * Отображает особенность тарифа по указанному идентификатору
     *
     * @param integer $id Идинтификатор особенность тарифа для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель особенности тарифа.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new TariffProperty;

        if (Yii::app()->getRequest()->getPost('TariffProperty') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('TariffProperty'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TariffModule.tariff', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'create',
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование особенности тарифа.
     *
     * @param integer $id Идинтификатор особенность тарифа для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('TariffProperty') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('TariffProperty'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TariffModule.tariff', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель особенности тарифа из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор особенности тарифа, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('TariffModule.tariff', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('TariffModule.tariff', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление особенностями тарифа.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new TariffProperty('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('TariffProperty') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('TariffProperty'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return TariffProperty    * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = TariffProperty::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('TariffModule.tariff', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
