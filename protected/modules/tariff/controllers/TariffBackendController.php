<?php

/**
 * Класс TariffBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class TariffBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Tariff.TariffBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Tariff.TariffBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Tariff.TariffBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Tariff.TariffBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Tariff.TariffBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'Tariff',
                'validAttributes' => ['title', 'name', 'slug', 'status', 'type', 'is_recommended', 'price'],
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'Tariff',
                'attribute' => 'sort',
            ],
        ];
    }

    /**
     * Отображает тариф по указанному идентификатору
     *
     * @param integer $id Идинтификатор тариф для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель тарифа.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Tariff;

        if (Yii::app()->getRequest()->getPost('Tariff') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Tariff'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TariffModule.tariff', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'create',
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование тарифа.
     *
     * @param integer $id Идинтификатор тариф для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Tariff') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Tariff'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TariffModule.tariff', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель тарифа из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор тарифа, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('TariffModule.tariff', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('TariffModule.tariff', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление тарифами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Tariff('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Tariff') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('Tariff'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return Tariff    * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Tariff::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('TariffModule.tariff', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
