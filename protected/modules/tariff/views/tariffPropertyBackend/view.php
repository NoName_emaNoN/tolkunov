<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model TariffProperty
 *   @var $this TariffPropertyBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Особенности тарифа') => ['/tariff/tariffPropertyBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Особенности тарифа - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление особенностями тарифа'), 'url' => ['/tariff/tariffPropertyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить особенность тарифа'), 'url' => ['/tariff/tariffPropertyBackend/create']],
    ['label' => Yii::t('TariffModule.tariff', 'Особенность тарифа') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('TariffModule.tariff', 'Редактирование особенности тарифа'), 'url' => [
        '/tariff/tariffPropertyBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('TariffModule.tariff', 'Просмотреть особенность тарифа'), 'url' => [
        '/tariff/tariffPropertyBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('TariffModule.tariff', 'Удалить особенность тарифа'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/tariff/tariffPropertyBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('TariffModule.tariff', 'Вы уверены, что хотите удалить особенность тарифа?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('TariffModule.tariff', 'Просмотр') . ' ' . Yii::t('TariffModule.tariff', 'особенности тарифа'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        'tariff_id',
        'title',
        'description',
        'sort',
    ],
]); ?>
