<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Tariff
 * @var $this TariffBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('TariffModule.tariff', 'Тарифы') => ['/tariff/tariffBackend/index'],
    Yii::t('TariffModule.tariff', 'Управление'),
];

$this->pageTitle = Yii::t('TariffModule.tariff', 'Тарифы - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('TariffModule.tariff', 'Управление тарифами'), 'url' => ['/tariff/tariffBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('TariffModule.tariff', 'Добавить тариф'), 'url' => ['/tariff/tariffBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('TariffModule.tariff', 'Тарифы'); ?>
        <small><?= Yii::t('TariffModule.tariff', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('TariffModule.tariff', 'Поиск тарифов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('tariff-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('TariffModule.tariff', 'В данном разделе представлены средства управления тарифами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'tariff-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/tariff/tariffBackend/sortable',
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            'create_time',
            'update_time',
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'price',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'price', ['class' => 'form-control']),
            ],
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'type',
                'url' => $this->createUrl('inline'),
                'source' => $model->getTypeList(),
                'editable' => [
                    'emptytext' => '---',
                ],
                'options' => [
                    $model::TYPE_HTML5 => ['class' => 'label-primary'],
                    $model::TYPE_GIF => ['class' => 'label-info'],
                    $model::TYPE_JPG => ['class' => 'label-success'],
                ],
            ],
//            'color',
//            'price',
//            [
//                'class' => 'yupe\widgets\EditableStatusColumn',
//                'name' => 'status',
//                'url' => $this->createUrl('inline'),
//                'source' => $model->getStatusList(),
//                'options' => [
//                    $model::STATUS_PUBLISHED => ['class' => 'label-success'],
//                    $model::STATUS_MODERATION => ['class' => 'label-warning'],
//                    $model::STATUS_DRAFT => ['class' => 'label-default'],
//                ],
//            ],
//            [
//                'class' => 'yupe\widgets\EditableStatusColumn',
//                'name' => 'is_recommended',
//                'url' => $this->createUrl('inline'),
//                'source' => $model->getYesNoList(),
//                'options' => [
//                    ['class' => 'label-default'],
//                    ['class' => 'label-success'],
//                ],
//            ],
            [
                'header' => 'Особенности',
                'type' => 'html',
                'value' => function ($data) {
//                    return CHtml::link(Yii::app()->getDateFormatter()->formatDateTime($data->date, 'medium'), ["/order/orderBackend/update", "id" => $data->id]);
                    return '<div class="btn-group btn-group-sm" role="group">' .
                    CHtml::link('Особенности: ' . $data->propertiesCount, ['/tariff/tariffPropertyBackend/index', 'TariffProperty[tariff_id]' => $data->id], ['class' => 'btn btn-sm btn-default']) .
//                    CHtml::link('<i class="fa fa-fw fa-plus"></i>', ['/tariff/tariffPropertyBackend/create'], ['class' => 'btn btn-sm btn-default']) .
                    '</div>';
                },
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
