<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaContentblock
 * @var $this  TildaContentblockBackendController
 **/
$this->breadcrumbs = [
	$this->getModule()->getCategory(),
	Yii::t( 'TildaModule.tilda', $this->getModule()->getName() ),
	Yii::t( 'TildaModule.tilda', 'Блоки' ) => [ '/tilda/tildaContentblockBackend/index' ],
	Yii::t( 'TildaModule.tilda', 'Управление' ),
];

$this->pageTitle = Yii::t( 'TildaModule.tilda', 'Блоки - управление' );

$this->menu = [
	[ 'icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t( 'TildaModule.tilda', 'Управление Блоками' ), 'url' => [ '/tilda/tildaContentblockBackend/index' ] ],
	[ 'icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t( 'TildaModule.tilda', 'Добавить Блок' ), 'url' => [ '/tilda/tildaContentblockBackend/create' ] ],
];
?>
<div class="page-header">
	<h1>
		<?= Yii::t( 'TildaModule.tilda', 'Блоки' ); ?>
		<small><?= Yii::t( 'TildaModule.tilda', 'управление' ); ?></small>
	</h1>
</div>

<p>
	<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
		<i class="fa fa-search">&nbsp;</i>
		<?= Yii::t( 'TildaModule.tilda', 'Поиск Блоков' ); ?>
		<span class="caret">&nbsp;</span>
	</a>
</p>

<div id="search-toggle" class="collapse out search-form">
	<?php Yii::app()->clientScript->registerScript( 'search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('tilda-contentblock-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    " );
	$this->renderPartial( '_search', [ 'model' => $model ] );
	?>
</div>

<br/>

<p> <?= Yii::t( 'TildaModule.tilda', 'В данном разделе представлены средства управления Блоками' ); ?>
</p>

<?php
$this->widget( 'yupe\widgets\CustomGridView', [
	'id'           => 'tilda-contentblock-grid',
	'type'         => 'striped condensed',
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'columns'      => [
		'id',
		'create_time',
		'update_time',
		'status',
		'name',
		'alias',
		//            'tilda_FK',
		[
			'class' => 'yupe\widgets\CustomButtonColumn',
		],
	],
] ); ?>
