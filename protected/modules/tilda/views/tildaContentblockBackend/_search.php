<?php
/**
 * Отображение для _search:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$form = $this->beginWidget( 'bootstrap.widgets.TbActiveForm', [
	'action'      => Yii::app()->createUrl( $this->route ),
	'method'      => 'get',
	'type'        => 'vertical',
	'htmlOptions' => [ 'class' => 'well' ],
] );
?>

<fieldset>
	<div class="row">
		<div class="col-sm-3">
			<?= $form->textFieldGroup( $model, 'id', [
				'widgetOptions' => [
					'htmlOptions' => [
						'class'               => 'popover-help',
						'data-original-title' => $model->getAttributeLabel( 'id' ),
						'data-content'        => $model->getAttributeDescription( 'id' )
					]
				]
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dateTimePickerGroup( $model, 'create_time', [
				'widgetOptions' => [
					'options'     => [],
					'htmlOptions' => []
				],
				'prepend'       => '<i class="fa fa-calendar"></i>'
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->dateTimePickerGroup( $model, 'update_time', [
				'widgetOptions' => [
					'options'     => [],
					'htmlOptions' => []
				],
				'prepend'       => '<i class="fa fa-calendar"></i>'
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup( $model, 'status', [
				'widgetOptions' => [
					'htmlOptions' => [
						'class'               => 'popover-help',
						'data-original-title' => $model->getAttributeLabel( 'status' ),
						'data-content'        => $model->getAttributeDescription( 'status' )
					]
				]
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup( $model, 'name', [
				'widgetOptions' => [
					'htmlOptions' => [
						'class'               => 'popover-help',
						'data-original-title' => $model->getAttributeLabel( 'name' ),
						'data-content'        => $model->getAttributeDescription( 'name' )
					]
				]
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup( $model, 'alias', [
				'widgetOptions' => [
					'htmlOptions' => [
						'class'               => 'popover-help',
						'data-original-title' => $model->getAttributeLabel( 'alias' ),
						'data-content'        => $model->getAttributeDescription( 'alias' )
					]
				]
			] ); ?>
		</div>
		<div class="col-sm-3">
			<?= $form->textFieldGroup( $model, 'tilda_FK', [
				'widgetOptions' => [
					'htmlOptions' => [
						'class'               => 'popover-help',
						'data-original-title' => $model->getAttributeLabel( 'tilda_FK' ),
						'data-content'        => $model->getAttributeDescription( 'tilda_FK' )
					]
				]
			] ); ?>
		</div>
	</div>
</fieldset>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'context'     => 'primary',
	'encodeLabel' => false,
	'buttonType'  => 'submit',
	'label'       => '<i class="fa fa-search">&nbsp;</i> ' . Yii::t( 'TildaModule.tilda', 'Искать Блок' ),
] ); ?>

<?php $this->endWidget(); ?>
