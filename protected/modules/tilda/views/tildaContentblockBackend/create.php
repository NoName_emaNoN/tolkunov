<?php
/**
 * Отображение для create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaContentblock
 * @var $this  TildaContentblockBackendController
 **/
$this->breadcrumbs = [
	$this->getModule()->getCategory(),
	Yii::t( 'TildaModule.tilda', $this->getModule()->getName() ),
	Yii::t( 'TildaModule.tilda', 'Блоки' ) => [ '/tilda/tildaContentblockBackend/index' ],
	Yii::t( 'TildaModule.tilda', 'Добавление' ),
];

$this->pageTitle = Yii::t( 'TildaModule.tilda', 'Блоки - добавление' );

$this->menu = [
	[ 'icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t( 'TildaModule.tilda', 'Управление Блоками' ), 'url' => [ '/tilda/tildaContentblockBackend/index' ] ],
	[ 'icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t( 'TildaModule.tilda', 'Добавить Блок' ), 'url' => [ '/tilda/tildaContentblockBackend/create' ] ],
];
?>
<div class="page-header">
	<h1>
		<?= Yii::t( 'TildaModule.tilda', 'Блоки' ); ?>
		<small><?= Yii::t( 'TildaModule.tilda', 'добавление' ); ?></small>
	</h1>
</div>

<?= $this->renderPartial( '_form', [ 'model' => $model ] ); ?>
