<?php
/**
 * Отображение для update:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaContentblock
 * @var $this  TildaContentblockBackendController
 **/
$this->breadcrumbs = [
	$this->getModule()->getCategory(),
	Yii::t( 'TildaModule.tilda', $this->getModule()->getName() ),
	Yii::t( 'TildaModule.tilda', 'Блоки' ) => [ '/tilda/tildaContentblockBackend/index' ],
	$model->name                           => [ '/tilda/tildaContentblockBackend/view', 'id' => $model->id ],
	Yii::t( 'TildaModule.tilda', 'Редактирование' ),
];

$this->pageTitle = Yii::t( 'TildaModule.tilda', 'Блоки - редактирование' );

$this->menu = [
	[ 'icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t( 'TildaModule.tilda', 'Управление Блоками' ), 'url' => [ '/tilda/tildaContentblockBackend/index' ] ],
	[ 'icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t( 'TildaModule.tilda', 'Добавить Блок' ), 'url' => [ '/tilda/tildaContentblockBackend/create' ] ],
	[ 'label' => Yii::t( 'TildaModule.tilda', 'Блок' ) . ' «' . mb_substr( $model->id, 0, 32 ) . '»' ],
	[
		'icon'  => 'fa fa-fw fa-pencil',
		'label' => Yii::t( 'TildaModule.tilda', 'Редактирование Блока' ),
		'url'   => [
			'/tilda/tildaContentblockBackend/update',
			'id' => $model->id
		]
	],
	[
		'icon'  => 'fa fa-fw fa-eye',
		'label' => Yii::t( 'TildaModule.tilda', 'Просмотреть Блок' ),
		'url'   => [
			'/tilda/tildaContentblockBackend/view',
			'id' => $model->id
		]
	],
	[
		'icon'        => 'fa fa-fw fa-trash-o',
		'label'       => Yii::t( 'TildaModule.tilda', 'Удалить Блок' ),
		'url'         => '#',
		'linkOptions' => [
			'submit'  => [ '/tilda/tildaContentblockBackend/delete', 'id' => $model->id ],
			'confirm' => Yii::t( 'TildaModule.tilda', 'Вы уверены, что хотите удалить Блок?' ),
			'csrf'    => true,
		]
	],
];
?>
<div class="page-header">
	<h1>
		<?= Yii::t( 'TildaModule.tilda', 'Редактирование' ) . ' ' . Yii::t( 'TildaModule.tilda', 'Блока' ); ?> <br/>
		<small>&laquo;<?= $model->name; ?>&raquo;</small>
	</h1>
</div>

<?= $this->renderPartial( '_form', [ 'model' => $model ] ); ?>
