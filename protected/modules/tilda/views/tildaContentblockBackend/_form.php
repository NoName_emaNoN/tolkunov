<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaContentblock
 * @var $form  yupe\widgets\ActiveForm
 * @var $this  TildaContentblockBackendController
 **/
$form = $this->beginWidget( 'yupe\widgets\ActiveForm', [
	'id'                     => 'tilda-contentblock-form',
	'enableAjaxValidation'   => false,
	'enableClientValidation' => true,
	'htmlOptions'            => [ 'class' => 'well' ],
] );
?>

<div class="alert alert-info">
	<?= Yii::t( 'TildaModule.tilda', 'Поля, отмеченные' ); ?>
	<span class="required">*</span>
	<?= Yii::t( 'TildaModule.tilda', 'обязательны.' ); ?>
</div>

<?= $form->errorSummary( $model ); ?>


<div class="row">
	<div class="col-sm-12">
		<?= $form->textFieldGroup( $model, 'name' ); ?>
	</div>
	<div class="col-sm-12">
		<? //= $form->textFieldGroup( $model, 'tilda_FK' ); ?>

		<?
		$pages = $this->getModule()->getComponent( 'TildaApi' )->getPages( $this->getModule()->projectId );;

		if ( ! empty( $pages ) ) {

			echo $form->select2Group( $model, 'tilda_FK', [
				//			'label'         => Yii::t( 'TildaModule.tilda', '' ),
				'widgetOptions' => [
					'data'    => CHtml::listData( $pages, 'id', 'title' ),
					'options' => [
						'placeholder' => Yii::t( 'TildaModule.tilda', 'Выберите страницу' ),
						'width'       => '100%',
					],
				],
			] );
		}
		?>
	</div>
	<div class="col-sm-12">
		<?= $form->textFieldGroup( $model, 'alias' ); ?>
	</div>
	<div class="col-sm-12">
		<?= $form->checkboxGroup( $model, 'status' ); ?>
	</div>
</div>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context'    => 'primary',
	'label'      => Yii::t( 'TildaModule.tilda', 'Сохранить Блок и продолжить' ),
] ); ?>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index' ],
	'label'       => Yii::t( 'TildaModule.tilda', 'Сохранить Блок и закрыть' ),
] ); ?>

<?php $this->endWidget(); ?>
