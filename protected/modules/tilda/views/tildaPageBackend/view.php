<?php
/**
 * Отображение для view:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaPage
 * @var $this  TildaPageBackendController
 **/
$this->breadcrumbs = [
	$this->getModule()->getCategory(),
	Yii::t( 'TildaModule.tilda', $this->getModule()->getName() ),
	Yii::t( 'TildaModule.tilda', 'Страницы' ) => [ '/tilda/tildaPageBackend/index' ],
	$model->title,
];

$this->pageTitle = Yii::t( 'TildaModule.tilda', 'Страницы - просмотр' );

$this->menu = [
	[ 'icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t( 'TildaModule.tilda', 'Управление Страницами' ), 'url' => [ '/tilda/tildaPageBackend/index' ] ],
	[ 'icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t( 'TildaModule.tilda', 'Добавить Страницу' ), 'url' => [ '/tilda/tildaPageBackend/create' ] ],
	[ 'label' => Yii::t( 'TildaModule.tilda', 'Страница' ) . ' «' . mb_substr( $model->id, 0, 32 ) . '»' ],
	[
		'icon'  => 'fa fa-fw fa-pencil',
		'label' => Yii::t( 'TildaModule.tilda', 'Редактирование Страницы' ),
		'url'   => [
			'/tilda/tildaPageBackend/update',
			'id' => $model->id
		]
	],
	[
		'icon'  => 'fa fa-fw fa-eye',
		'label' => Yii::t( 'TildaModule.tilda', 'Просмотреть Страницу' ),
		'url'   => [
			'/tilda/tildaPageBackend/view',
			'id' => $model->id
		]
	],
	[
		'icon'        => 'fa fa-fw fa-trash-o',
		'label'       => Yii::t( 'TildaModule.tilda', 'Удалить Страницу' ),
		'url'         => '#',
		'linkOptions' => [
			'submit'  => [ '/tilda/tildaPageBackend/delete', 'id' => $model->id ],
			'confirm' => Yii::t( 'TildaModule.tilda', 'Вы уверены, что хотите удалить Страницу?' ),
			'csrf'    => true,
		]
	],
];
?>
<div class="page-header">
	<h1>
		<?= Yii::t( 'TildaModule.tilda', 'Просмотр' ) . ' ' . Yii::t( 'TildaModule.tilda', 'Страницы' ); ?> <br/>
		<small>&laquo;<?= $model->title; ?>&raquo;</small>
	</h1>
</div>

<?php $this->widget( 'bootstrap.widgets.TbDetailView', [
	'data'       => $model,
	'attributes' => [
		'id',
		'create_time',
		'update_time',
		'status',
		'title',
		'url',
		'tilda_FK',
	],
] ); ?>
