<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaPage
 * @var $form  yupe\widgets\ActiveForm
 * @var $this  TildaPageBackendController
 **/
$form = $this->beginWidget( 'yupe\widgets\ActiveForm', [
	'id'                     => 'tilda-page-form',
	'enableAjaxValidation'   => false,
	'enableClientValidation' => true,
	'htmlOptions'            => [ 'class' => 'well' ],
] );
?>

<div class="alert alert-info">
	<?= Yii::t( 'TildaModule.tilda', 'Поля, отмеченные' ); ?>
	<span class="required">*</span>
	<?= Yii::t( 'TildaModule.tilda', 'обязательны.' ); ?>
</div>

<?= $form->errorSummary( $model ); ?>

<div class="row">
	<div class="col-sm-12">
		<?= $form->textFieldGroup( $model, 'title' ); ?>
	</div>
	<div class="col-sm-12">
		<?= $form->textFieldGroup( $model, 'title_short' ); ?>
	</div>
	<div class="col-sm-12">
		<?= $form->textFieldGroup( $model, 'url' ); ?>
	</div>
	<div class="col-sm-12">
		<?
		$pages = $this->getModule()->getComponent( 'TildaApi' )->getPages( $this->getModule()->projectId );

		if ( ! empty( $pages ) ) {
			echo $form->select2Group( $model, 'tilda_FK', [
				//			'label'         => Yii::t( 'TildaModule.tilda', '' ),
				'widgetOptions' => [
					'data'    => CHtml::listData( $pages, 'id', 'title' ),
					'options' => [
						'placeholder' => Yii::t( 'TildaModule.tilda', 'Выберите страницу' ),
						'width'       => '100%',
					],
				],
			] );
		}
		?>
	</div>
	<div class="col-sm-12">
		<?= $form->checkboxGroup( $model, 'status' ); ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?php $collapse = $this->beginWidget( 'bootstrap.widgets.TbCollapse' ); ?>
		<div class="panel-group" id="extended-options">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">
						<a data-toggle="collapse" data-parent="#extended-options" href="#collapseTwo">
							<?= Yii::t( 'TildaModule.tilda', 'SEO данные' ); ?>
						</a>
					</div>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="row">
							<div class="col-xs-7">
								<?= $form->textFieldGroup( $model, 'keywords'); ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-7">
								<?= $form->textAreaGroup( $model, 'description'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context'    => 'primary',
	'label'      => Yii::t( 'TildaModule.tilda', 'Сохранить Страницу и продолжить' ),
] ); ?>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index' ],
	'label'       => Yii::t( 'TildaModule.tilda', 'Сохранить Страницу и закрыть' ),
] ); ?>

<?php $this->endWidget(); ?>
