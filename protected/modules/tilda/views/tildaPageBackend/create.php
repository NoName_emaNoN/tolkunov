<?php
/**
 * Отображение для create:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model TildaPage
 * @var $this  TildaPageBackendController
 **/
$this->breadcrumbs = [
	$this->getModule()->getCategory(),
	Yii::t( 'TildaModule.tilda', $this->getModule()->getName() ),
	Yii::t( 'TildaModule.tilda', 'Страницы' ) => [ '/tilda/tildaPageBackend/index' ],
	Yii::t( 'TildaModule.tilda', 'Добавление' ),
];

$this->pageTitle = Yii::t( 'TildaModule.tilda', 'Страницы - добавление' );

$this->menu = [
	[ 'icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t( 'TildaModule.tilda', 'Управление Страницами' ), 'url' => [ '/tilda/tildaPageBackend/index' ] ],
	[ 'icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t( 'TildaModule.tilda', 'Добавить Страницу' ), 'url' => [ '/tilda/tildaPageBackend/create' ] ],
];
?>
<div class="page-header">
	<h1>
		<?= Yii::t( 'TildaModule.tilda', 'Страницы' ); ?>
		<small><?= Yii::t( 'TildaModule.tilda', 'добавление' ); ?></small>
	</h1>
</div>

<?= $this->renderPartial( '_form', [ 'model' => $model ] ); ?>
