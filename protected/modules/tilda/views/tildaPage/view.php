<?php

/* @var $content string */
/* @var $pageModel TildaPage */
/* @var $this TildaPageController */

$this->title = [$pageModel->title, Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [];
$this->description = $pageModel->description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $pageModel->keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>
<?=$content ?>
