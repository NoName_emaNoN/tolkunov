<?php

class Tilda extends CApplicationComponent
{
    const TILDA_ASSETS_CACHED_KEY = 'TILDA_ASSETS_CACHED';

    public function getTildaAssetsCachedKey()
    {
        return self::TILDA_ASSETS_CACHED_KEY;
    }

    public function getTildaPageDataCacheKey($pageId)
    {
        return 'tildaPageData' . $pageId;
    }

    public function cacheTildaAssets()
    {
        $tildaAssetsCached = Yii::app()->getCache()->get(self::TILDA_ASSETS_CACHED_KEY);

        if (false === $tildaAssetsCached) {

            /* @var $api TildaApi */
            $api = Yii::app()->getModule('tilda')->getComponent('TildaApi');
            $projectData = Yii::app()->cache->get('tildaProjectData');

            if (false === $projectData) {
                $projectData = $api->getProjectExport(Yii::app()->getModule('tilda')->projectId);

                Yii::app()->cache->set('tildaProjectData', $projectData);
            }

            foreach ($projectData->css as $item) {
                $filename = Yii::app()->uploadManager->getFilePath($item->to, 'tilda/css');

                if (!file_exists($filename)) {
                    file_put_contents($filename, file_get_contents($item->from));
                }
            }

            foreach ($projectData->js as $item) {
                $filename = Yii::app()->uploadManager->getFilePath($item->to, 'tilda/js');

                if (!file_exists($filename)) {
                    file_put_contents($filename, file_get_contents($item->from));
                }
            }

            Yii::app()->getCache()->set(self::TILDA_ASSETS_CACHED_KEY, true);
        }

        return true;
    }

    public function cacheTildaPageAssets($pageId)
    {
        $tildaPageData = Yii::app()->getCache()->get($this->getTildaPageDataCacheKey($pageId));

        if (false === $tildaPageData) {

            /* @var $api TildaApi */
            $api = Yii::app()->getModule('tilda')->getComponent('TildaApi');
            $tildaPageData = $api->getPageFullExport($pageId);

            foreach ($tildaPageData->images as $item) {
                $filename = Yii::app()->uploadManager->getFilePath($item->to, 'tilda/images');
                file_put_contents($filename, file_get_contents($item->from));
            }

            Yii::app()->getCache()->set($this->getTildaPageDataCacheKey($pageId), $tildaPageData);
        }

        return true;
    }
}
