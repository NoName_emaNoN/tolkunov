<?php

use GuzzleHttp\Client;

class TildaApi extends CApplicationComponent
{

    const STATUS_SUCCESS = 'FOUND';
    const STATUS_ERROR = 'ERROR';

    /**
     * @var string
     */
    private $baseUri = 'http://api.tildacdn.info/';

    /**
     * @var string
     */
    public $secretKey;

    /**
     * @var string
     */
    public $publicKey;

    /**
     * @var GuzzleHttp\Client
     */
    private $api;

    public function init()
    {
        $this->secretKey = Yii::app()->getModule('tilda')->apiSecretKey;
        $this->publicKey = Yii::app()->getModule('tilda')->apiPublicKey;
        $this->api = new Client([
            'base_uri' => $this->baseUri,
        ]);

        return parent::init();
    }

    private function _params($params = [])
    {
        return CMap::mergeArray($params, [
            'publickey' => $this->publicKey,
            'secretkey' => $this->secretKey,
        ]);
    }

    /**
     * @var $method string
     * @var $params array
     * @return array|null
     * @throws CHttpException
     */
    public function makeRequest($method, $params = [])
    {
        $request = $this->api->get($method, [
            'query' => $this->_params($params),
        ]);

        $body = $request->getBody();
        if (!empty($body)) {
            $response = json_decode($body);
            if ($response->status === self::STATUS_SUCCESS) {
                return $response->result;
            } else {
                if (YII_DEBUG === true && $response->status === self::STATUS_ERROR) {
                    throw new CHttpException(500, $response->message);
                }
            }
        }

        return null;
    }

    /**
     * @var $projectId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getPages($projectId)
    {
        return $this->makeRequest('/v1/getpageslist/', ['projectid' => $projectId]);
    }

    /**
     * @var $pageId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getPage($pageId)
    {
        return $this->makeRequest('/v1/getpage/', ['pageid' => $pageId]);
    }

    /**
     * @var $pageId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getPageFullExport($pageId)
    {
        return $this->makeRequest('/v1/getpagefullexport/', ['pageid' => $pageId]);
    }

    /**
     * @var $pageId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getPageExport($pageId)
    {
        return $this->makeRequest('/v1/getpageexport/', ['pageid' => $pageId]);
    }

    /**
     * @return array|null
     * @throws CHttpException
     */
    public function getBlocks()
    {
        return $this->makeRequest('/v1/getprojectslist/');
    }

    /**
     * @var $blockId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getBlock($blockId)
    {
        return $this->makeRequest('/v1/getpage/', ['pageid' => $blockId]);
    }


    /**
     * @return array|null
     * @throws CHttpException
     */
    public function getProjects()
    {
        return $this->makeRequest('/v1/getprojectslist/');
    }

    /**
     * @var $projectId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getProject($projectId)
    {
        return $this->makeRequest('/v1/getproject/', ['projectid' => $projectId]);
    }

    /**
     * @var $projectId integer
     * @return array|null
     * @throws CHttpException
     */
    public function getProjectExport($projectId)
    {
        return $this->makeRequest('/v1/getprojectexport/', ['projectid' => $projectId]);
    }
}
