<?php

/**
 * This is the model class for table "{{tilda_page}}".
 *
 * The followings are the available columns in table '{{tilda_page}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property string $title
 * @property string $title_short
 * @property string $keywords
 * @property string $description
 * @property string $url
 * @property integer $tilda_FK
 * @property integer $tilda_projectId
 */
class TildaPage extends yupe\models\YModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_IN_ACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tilda_page}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, tilda_FK', 'required'],
            ['status, tilda_FK, tilda_projectId', 'numerical', 'integerOnly' => true],
            ['title, url, keywords, description', 'length', 'max' => 255],
            ['title_short', 'length', 'max' => 150],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, status, title, url, tilda_FK, title_short, keywords, description, tilda_projectId', 'safe', 'on' => 'search'],
            ['status', 'boolean'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('TildaModule.tilda', 'ID'),
            'create_time' => Yii::t('TildaModule.tilda', 'Дата создания'),
            'update_time' => Yii::t('TildaModule.tilda', 'Дата изменения'),
            'status' => Yii::t('TildaModule.tilda', 'Статус'),
            'title' => Yii::t('TildaModule.tilda', 'Заголовок'),
            'url' => Yii::t('TildaModule.tilda', 'URL'),
            'tilda_FK' => Yii::t('TildaModule.tilda', 'ID страницы (tilda)'),
            'title_short' => Yii::t('TildaModule.tilda', 'Короткий заголовок'),
            'keywords' => Yii::t('TildaModule.tilda', 'Ключевые слова'),
            'description' => Yii::t('TildaModule.tilda', 'Описание'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('tilda_FK', $this->tilda_FK);
        $criteria->compare('tilda_projectId', $this->tilda_projectId);
        $criteria->compare('title_short', $this->title_short);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return TildaPage the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function active()
    {

        $this->getDbCriteria()->addCondition('status=' . self::STATUS_ACTIVE);

        return $this;
    }

    /**
     * @param $url string
     *
     * @return $this
     */
    public function forPage($url)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.url= :url',
            'params' => [':url' => $url],
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return bool
     */
    protected function beforeSave()
    {
        Yii::app()->getCache()->delete($this->getCacheId());

        if ($this->isNewRecord || empty($this->tilda_projectId)) {
            $this->tilda_projectId = Yii::app()->getModule('tilda')->projectId;
        }

        if ($this->status == self::STATUS_IN_ACTIVE) {
            $this->url = '';
        }

        return parent::beforeSave();
    }

    public function getCacheId()
    {
        return "TildaPage{$this->tilda_FK}" . Yii::app()->language;
    }

    public function findByTildaFK($pageid, $projectid)
    {
        return self::model()->findByAttributes(['tilda_FK' => $pageid, 'tilda_projectId' => $projectid]);
    }
}
