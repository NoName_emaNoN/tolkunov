<?php

/**
 * This is the model class for table "{{tilda_contentblock}}".
 *
 * The followings are the available columns in table '{{tilda_contentblock}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property string $name
 * @property string $alias
 * @property integer $tilda_FK
 * @property integer $tilda_projectId
 */
class TildaContentblock extends yupe\models\YModel
{
    const STATUS_ACTIVE = 1;
    const STATUS_IN_ACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{tilda_contentblock}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, alias, tilda_FK', 'required'],
            ['status, tilda_FK, tilda_projectId', 'numerical', 'integerOnly' => true],
            ['name, alias', 'length', 'max' => 255],
            ['alias', 'unique'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, status, name, alias, tilda_FK, tilda_projectId', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('TildaModule.tilda', 'ID'),
            'create_time' => Yii::t('TildaModule.tilda', 'Дата создания'),
            'update_time' => Yii::t('TildaModule.tilda', 'Дата изменения'),
            'status' => Yii::t('TildaModule.tilda', 'Статус'),
            'name' => Yii::t('TildaModule.tilda', 'Название'),
            'alias' => Yii::t('TildaModule.tilda', 'Алиас'),
            'tilda_FK' => Yii::t('TildaModule.tilda', 'ID блока(tilda)'),
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('update_time', $this->update_time, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('tilda_FK', $this->tilda_FK);
        $criteria->compare('tilda_projectId', $this->tilda_projectId);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return TildaContentblock the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->alias) {
            $this->alias = yupe\helpers\YText::translit($this->name);
        }

        return parent::beforeValidate();
    }

    protected function beforeSave()
    {
        Yii::app()->getCache()->delete($this->getCacheId());

        if ($this->isNewRecord || empty($this->tilda_projectId)) {
            $this->tilda_projectId = Yii::app()->getModule('tilda')->projectId;
        }

        return parent::beforeSave();
    }

    public function getCacheId()
    {
        return "TildaContentblock{$this->tilda_FK}" . Yii::app()->language;
    }

    public function active()
    {

        $this->getDbCriteria()->addCondition('status=' . self::STATUS_ACTIVE);

        return $this;
    }

    public function byAlias($alias)
    {

        $this->getDbCriteria()->mergeWith([
            'condition' => 'alias=:alias',
            'params' => [':alias' => $alias],
        ]);

        return $this;
    }

    public function findByTildaFK($pageid, $projectid)
    {
        return self::model()->findByAttributes(['tilda_FK' => $pageid, 'tilda_projectId' => $projectid]);
    }
}
