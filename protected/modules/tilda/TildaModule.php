<?php
use yupe\components\WebModule;

/**
 * TildaModule основной класс модуля tilda
 *
 * @author    yupe team <team@yupe.ru>
 * @link      http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package   yupe.modules.tilda
 * @since     0.1
 */
class TildaModule extends WebModule {
	const VERSION = '1.0';

	/**
	 * @var string
	 */
	public $apiSecretKey;

	/**
	 * @var string
	 */
	public $apiPublicKey;

	/**
	 * @var string
	 */
	public $projectId;

	/**
	 * @var int порядок следования модуля в меню панели управления (сортировка)
	 */
	public $adminMenuOrder = 0;
	/**
	 * @var int некоторые компоненты Юпи! автоматически кэширует, если время жизни кэша не указано - берется это значение
	 */
	public $coreCacheTime = 3600;

	/**
	 * @var int время кеширования для запросов к АПИ
	 */
	public $pageCacheTime = 3600;

	/**
	 * @var int время кеширования для запросов к АПИ
	 */
	public $blockCacheTime = 3600;

	/**
	 * @var int время кеширования для запросов к АПИ
	 */
	public $apiCacheTime = 3600;

	/**
	 * Массив с именами модулей, от которых зависит работа данного модуля
	 *
	 * @return array
	 */
	public function getDependencies() {
		return [ 'user' ];
	}

	/**
	 * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
	 * В этом методе необходимо выполнить все проверки.
	 *
	 * @return array|false
	 */
	public function checkSelf() {
		$messages = [];

		return isset( $messages[ WebModule::CHECK_ERROR ] ) ? $messages : true;
	}

	/**
	 * @return bool
	 */
	public function getInstall() {
		if ( parent::getInstall() ) {
			@mkdir( Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755 );
		}

		return false;
	}

	/**
	 * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
	 *
	 * @return string
	 */
	public function getCategory() {
		return Yii::t( 'TildaModule.tilda', 'Content' );
	}

	/**
	 * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
	 *
	 * @return array
	 */
	public function getParamsLabels() {
		return [
			'editor'         => Yii::t( 'TildaModule.tilda', 'Visual Editor' ),
			'apiSecretKey'   => Yii::t( 'TildaModule.tilda', 'Secret Key' ),
			'apiPublicKey'   => Yii::t( 'TildaModule.tilda', 'Public Key' ),
			'projectId'      => Yii::t( 'TildaModule.tilda', 'Проект на tilda.cc' ),
			'pageCacheTime'  => Yii::t( 'TildaModule.tilda', 'Время кеширования для страниц (сек.)' ),
			'blockCacheTime' => Yii::t( 'TildaModule.tilda', 'Время кеширования для блоков(сек.)' ),
			'apiCacheTime'   => Yii::t( 'TildaModule.tilda', 'Общее время кеширования (сек.)' ),
		];
	}

	/**
	 * массив параметров модуля, которые можно редактировать через панель управления (GUI)
	 *
	 * @return array
	 */
	public function getEditableParams() {
		$params = [
			'editor' => Yii::app()->getModule( 'yupe' )->getEditors(),
			'apiSecretKey',
			'apiPublicKey',
			'pageCacheTime',
			'blockCacheTime',
			'apiCacheTime',
		];

		if ( ! empty( $this->apiPublicKey ) && ! empty( $this->apiSecretKey ) ) {
			$projects = Yii::app()->getCache()->get( 'tilda-settings::projects' );
			if ( false === $projects ) {
				$projects = $this->getComponent( 'TildaApi' )->getProjects();

				Yii::app()->getCache()->set( 'tilda-settings::projects', $projects, $this->apiCacheTime );
			}

			if ( ! empty( $projects ) ) {
				$params['projectId'] = CHtml::listData( $projects, 'id', 'title' );
			}
		}

		return $params;
	}

	/**
	 * массив групп параметров модуля, для группировки параметров на странице настроек
	 *
	 * @return array
	 */
	public function getEditableParamsGroups() {
		$groups = [
			'tilda' => [
				'label' => Yii::t( 'TildaModule.tilda', 'Tilda.CC' ),
				'items' => [
					'apiSecretKey',
					'apiPublicKey',
					'projectId',
					'pageCacheTime',
					'blockCacheTime',
					'apiCacheTime'
				]
			]
		];

		return $groups + parent::getEditableParamsGroups();
	}

	/**
	 * если модуль должен добавить несколько ссылок в панель управления - укажите массив
	 *
	 * @return array
	 */
	public function getNavigation() {
		return [
			[ 'label' => Yii::t( 'TildaModule.tilda', 'Страницы' ) ],
			[
				'icon'  => 'fa fa-fw fa-list-alt',
				'label' => Yii::t( 'TildaModule.tilda', 'Список страниц' ),
				'url'   => [ '/tilda/tildaPageBackend/index' ]
			],
			[
				'icon'  => 'fa fa-fw fa-plus-square',
				'label' => Yii::t( 'TildaModule.tilda', 'Добавить страницу' ),
				'url'   => [ '/tilda/tildaPageBackend/create' ]
			],
			[ 'label' => Yii::t( 'TildaModule.tilda', 'Блоки' ) ],
			[
				'icon'  => 'fa fa-fw fa-list-alt',
				'label' => Yii::t( 'TildaModule.tilda', 'Список блоков' ),
				'url'   => [ '/tilda/tildaContentblockBackend/index' ]
			],
			[
				'icon'  => 'fa fa-fw fa-plus-square',
				'label' => Yii::t( 'TildaModule.tilda', 'Добавить блок' ),
				'url'   => [ '/tilda/tildaContentblockBackend/create' ]
			],
		];
	}

	/**
	 * текущая версия модуля
	 *
	 * @return string
	 */
	public function getVersion() {
		return Yii::t( 'TildaModule.tilda', self::VERSION );
	}

	/**
	 * Возвращает название модуля
	 *
	 * @return string.
	 */
	public function getName() {
		return Yii::t( 'TildaModule.tilda', 'Tilda' );
	}

	/**
	 * Возвращает описание модуля
	 *
	 * @return string.
	 */
	public function getDescription() {
		return Yii::t( 'TildaModule.tilda', 'Модуль для взаимодействия с сервисом Tilda"' );
	}

	/**
	 * Имя автора модуля
	 *
	 * @return string
	 */
	public function getAuthor() {
		return Yii::t( 'TildaModule.tilda', 'maroon775 (Altibaev Amal)' );
	}

	/**
	 * Контактный email автора модуля
	 *
	 * @return string
	 */
	public function getAuthorEmail() {
		return Yii::t( 'TildaModule.tilda', 'maroon775@gmail.com' );
	}

	/**
	 * веб-сайт разработчика модуля или страничка самого модуля
	 *
	 * @return string
	 */
	public function getUrl() {
		return Yii::t( 'TildaModule.tilda', 'https://vk.com/maroon775' );
	}

	/**
	 * Ссылка, которая будет отображена в панели управления
	 * Как правило, ведет на страничку для администрирования модуля
	 *
	 * @return string
	 */
	public function getAdminPageLink() {
		return '/tilda/tildaPageBackend/index';
	}

	/**
	 * Название иконки для меню админки, например 'user'
	 *
	 * @return string
	 */
	public function getIcon() {
		return "fa fa-fw fa-columns";
	}

	/**
	 * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
	 *
	 * @return bool
	 **/
	public function getIsInstallDefault() {
		return parent::getIsInstallDefault();
	}

	/**
	 * Инициализация модуля, считывание настроек из базы данных и их кэширование
	 *
	 * @return void
	 */
	public function init() {
		parent::init();

		$this->setImport( [
			'tilda.models.*',
			'tilda.components.*',
		] );
	}

	/**
	 * Массив правил модуля
	 * @return array
	 */
	public function getAuthItems() {
		return [
			[
				'name'        => 'Tilda.TildaManager',
				'description' => Yii::t( 'TildaModule.tilda', 'Manage tilda' ),
				'type'        => AuthItem::TYPE_TASK,
				'items'       => [
					[
						'type'        => AuthItem::TYPE_OPERATION,
						'name'        => 'Tilda.TildaBackend.Create',
						'description' => Yii::t( 'TildaModule.tilda', 'Create' )
					],
					[
						'type'        => AuthItem::TYPE_OPERATION,
						'name'        => 'Tilda.TildaBackend.Delete',
						'description' => Yii::t( 'TildaModule.tilda', 'Delete' )
					],
					[
						'type'        => AuthItem::TYPE_OPERATION,
						'name'        => 'Tilda.TildaBackend.Index',
						'description' => Yii::t( 'TildaModule.tilda', 'Index' )
					],
					[
						'type'        => AuthItem::TYPE_OPERATION,
						'name'        => 'Tilda.TildaBackend.Update',
						'description' => Yii::t( 'TildaModule.tilda', 'Update' )
					],
					[
						'type'        => AuthItem::TYPE_OPERATION,
						'name'        => 'Tilda.TildaBackend.View',
						'description' => Yii::t( 'TildaModule.tilda', 'View' )
					],
				]
			]
		];
	}
}
