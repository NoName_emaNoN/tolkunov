<?php

/**
 * Tilda install migration
 * Класс миграций для модуля Tilda:
 *
 * @category YupeMigration
 * @package  yupe.modules.tilda.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m180705_183401_tilda_base extends yupe\components\DbMigration {
	/**
	 * Функция настройки и создания таблицы:
	 *
	 * @return null
	 **/
	public function safeUp() {
		$this->createTable( '{{tilda_page}}', [
			'id'          => 'pk',
			//для удобства добавлены некоторые базовые поля, которые могут пригодиться.
			'create_time' => 'datetime NOT NULL',
			'update_time' => 'datetime NOT NULL',
			'status'      => 'tinyint(1) NOT NULL DEFAULT 0',
			'title'       => 'string NOT NULL',
			'url'         => 'varchar(255)',
			'tilda_FK'    => 'integer NOT NULL',
		], $this->getOptions() );

		//ix
		$this->createIndex( "ix_{{tilda_page}}_create_time", '{{tilda_page}}', "create_time", false );
		$this->createIndex( "ix_{{tilda_page}}_update_time", '{{tilda_page}}', "update_time", false );
		$this->createIndex( "ix_{{tilda_page}}_status", '{{tilda_page}}', "status", false );
		$this->createIndex( "ix_{{tilda_page}}_url", '{{tilda_page}}', "url", false );

		$this->createTable( '{{tilda_contentblock}}', [
			'id'          => 'pk',
			//для удобства добавлены некоторые базовые поля, которые могут пригодиться.
			'create_time' => 'datetime NOT NULL',
			'update_time' => 'datetime NOT NULL',
			'status'      => 'tinyint(1) NOT NULL DEFAULT 0',
			'name'        => 'string NOT NULL',
			'alias'       => 'varchar(255) NOT NULL',
			'tilda_FK'    => 'integer NOT NULL',
		], $this->getOptions() );

		$this->createIndex( "ix_{{tilda_contentblock}}_create_time", '{{tilda_contentblock}}', "create_time", false );
		$this->createIndex( "ix_{{tilda_contentblock}}_update_time", '{{tilda_contentblock}}', "update_time", false );
		$this->createIndex( "ix_{{tilda_contentblock}}_status", '{{tilda_contentblock}}', "status", false );
		$this->createIndex( "ix_{{tilda_contentblock}}_alias", '{{tilda_contentblock}}', "alias", true );
	}

	/**
	 * Функция удаления таблицы:
	 *
	 * @return null
	 **/
	public function safeDown() {
		$this->dropTableWithForeignKeys( '{{tilda_page}}' );
		$this->dropTableWithForeignKeys( '{{tilda_contentblock}}' );
	}
}
