<?php

class m180709_082330_add_seo_fields_to_tilda_page extends yupe\components\DbMigration {
	public function safeUp() {
		$this->addColumn( '{{tilda_page}}', 'title_short', 'varchar(150) NOT NULL' );
		$this->addColumn( '{{tilda_page}}', 'keywords', 'varchar(250)' );
		$this->addColumn( '{{tilda_page}}', 'description', 'varchar(250)' );
	}

	public function safeDown() {
		$this->dropColumn( '{{tilda_page}}', 'title_short' );
		$this->dropColumn( '{{tilda_page}}', 'keywords' );
		$this->dropColumn( '{{tilda_page}}', 'description' );
	}
}
