<?php

class m180709_202128_add_project_field extends yupe\components\DbMigration {
	public function safeUp() {
		$this->addColumn( "{{tilda_page}}", 'tilda_projectId', 'integer NOT NULL' );
		$this->addColumn( "{{tilda_contentblock}}", 'tilda_projectId', 'integer NOT NULL' );
	}

	public function safeDown() {

		$this->dropColumn( "{{tilda_page}}", 'tilda_projectId' );
		$this->dropColumn( "{{tilda_contentblock}}", 'tilda_projectId' );
	}
}
