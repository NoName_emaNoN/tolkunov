<?php
/**
 * Файл настроек для модуля tilda
 *
 * @author    yupe team <team@yupe.ru>
 * @link      http://yupe.ru
 * @copyright 2009-2018 amyLabs && Yupe! team
 * @package   yupe.modules.tilda.install
 * @since     0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.tilda.TildaModule',
        'components' => [
            'TildaApi' => [
                'class' => 'application.modules.tilda.components.TildaApi',
            ],
            'Tilda' => [
                'class' => 'application.modules.tilda.components.Tilda',
            ],
        ],
    ],
    'import' => [
        'application.modules.tilda.models.*',
        'application.modules.tilda.components.*',
    ],
    'component' => [
        'TildaApi' => [
            'class' => 'application.modules.tilda.components.TildaApi',
        ],
        'Tilda' => [
            'class' => 'application.modules.tilda.components.Tilda',
        ],
    ],
    'rules' => [
        '/block/<url:[\w-]+>' => [
            '/tilda/tildaPage/viewAsBlock',
            'type' => 'db',
            'fields' => [
                'url' => [
                    'table' => '{{tilda_page}}',
                    'field' => 'url',
                ],
            ],
        ],
        '/<url:[\w-]+>' => [
            '/tilda/tildaPage/view',
            'type' => 'db',
            'fields' => [
                'url' => [
                    'table' => '{{tilda_page}}',
                    'field' => 'url',
                ],
            ],
        ],
        '/updateTilda' => '/tilda/tildaPage/update',
    ],
];
