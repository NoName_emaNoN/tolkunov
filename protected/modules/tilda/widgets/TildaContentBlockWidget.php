<?php
/**
 * Виджет для отрисовки блока Tilda:
 *
 * @category YupeWidgets
 * @package  yupe.modules.tilda.widgets
 * @author   Yupe Team <team@yupe.ru>
 * @license  BSD http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D1%86%D0%B5%D0%BD%D0%B7%D0%B8%D1%8F_BSD
 * @link     http://yupe.ru
 *
 **/
Yii::import('application.modules.tilda.models.TildaContentblock');
Yii::import('application.modules.tilda.TildaModule');

/**
 * Class TildaContentBlockWidget
 */
class TildaContentBlockWidget extends yupe\widgets\YWidget
{
    /**
     * @var
     */
    public $alias;
    /**
     * @var string
     */
    public $view = 'default';

    private $_data = null;

    private $_contentExist = false;


    public $htmlOptions = [];

    /**
     * @throws CException
     */
    public function init()
    {
        if (empty($this->alias)) {
            throw new CException(Yii::t('TildaModule.contentblock', 'Insert content block title for TildaContentBlockWidget!'));
        }

        ob_start();

        /**@var  $tildaModule TildaModule */
        $tildaModule = Yii::app()->getModule('tilda');

        /* @var $tilda Tilda */
        $tilda = $tildaModule->getComponent('Tilda');

        $block = TildaContentblock::model()->cache($tildaModule->blockCacheTime)->active()->byAlias($this->alias)->find();

        if (null !== $block && !empty($block->tilda_FK)) {

            $this->_data = Yii::app()->getCache()->get($block->getCacheId());
            if (false === $this->_data) {
                /* @var $api TildaApi */
                $api = $tildaModule->getComponent('TildaApi');

                $tilda_page = $api->getPageExport($block->tilda_FK);
                $tilda_project = $api->getProjectExport($block->tilda_projectId);

                $this->_data = [
                    'page' => $tilda_page,
                    'project' => $tilda_project,
                ];

                $tilda->cacheTildaPageAssets($block->tilda_FK);

                Yii::app()->getCache()->set($block->getCacheId(), $this->_data, $tildaModule->blockCacheTime);
            }

            $tilda->cacheTildaAssets();

            if (!empty($this->_data['page']->id)) {
                $this->_contentExist = true;
            }
        }
    }

    /**
     * @throws CException
     */
    public function run()
    {
        if ($this->_contentExist) {
            ob_get_clean();

            if (!empty($this->_data['project']->id)) {
                if (!empty($this->_data['project']->js)) {
                    $jsBaseUrl = $this->_data['project']->export_jspath;

                    foreach ($this->_data['project']->js as $jsAsset) {
                        if (preg_match('/\/js\/jquery\-(.*)?\.min\.js/', $jsAsset->from)) {
                            continue;
                        }
                        Yii::app()->getClientScript()->registerScriptFile($jsBaseUrl . '/' . $jsAsset->to, CClientScript::POS_HEAD);
                    }
                }
                if (!empty($this->_data['project']->css)) {
                    $cssBaseUrl = $this->_data['project']->export_csspath;

                    foreach ($this->_data['project']->css as $cssAsset) {
                        Yii::app()->getClientScript()->registerCssFile($cssBaseUrl . '/' . $cssAsset->to);
                    }
                }
            }

            $this->render($this->view, ['output' => $this->_data['page']->html, 'htmlOptions' => $this->htmlOptions]);
        } else {
            $wrapp_content = ob_get_clean();

            $this->render($this->view, ['output' => $wrapp_content, 'htmlOptions' => $this->htmlOptions]);
        }
    }

    /** @return bool */
    public function isEmpty()
    {
        return $this->_contentExist === true ? false : true;
    }
}
