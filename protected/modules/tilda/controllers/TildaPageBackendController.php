<?php

/**
 * Класс TildaPageController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class TildaPageBackendController extends \yupe\components\controllers\BackController {
	/**
	 * @return array
	 */
	public function accessRules() {
		return [
			[ 'allow', 'roles' => [ 'admin' ] ],
			[ 'allow', 'actions' => [ 'index' ], 'roles' => [ 'Tilda.TildaPageBackend.Index' ] ],
			[ 'allow', 'actions' => [ 'view' ], 'roles' => [ 'Tilda.TildaPageBackend.View' ] ],
			[ 'allow', 'actions' => [ 'create' ], 'roles' => [ 'Tilda.TildaPageBackend.Create' ] ],
			[ 'allow', 'actions' => [ 'update', 'inline' ], 'roles' => [ 'Tilda.TildaPageBackend.Update' ] ],
			[ 'allow', 'actions' => [ 'delete', 'multiaction' ], 'roles' => [ 'Tilda.TildaPageBackend.Delete' ] ],
			[ 'deny' ],
		];
	}

	/**
	 * @return array
	 */
	public function actions() {
		return [
			'inline' => [
				'class'           => 'yupe\components\actions\YInLineEditAction',
				'model'           => 'TildaPage',
				'validAttributes' => [ 'title', 'name', 'slug', 'status' ],
			],
		];
	}

	/**
	 * Отображает Страницу по указанному идентификатору
	 *
	 * @param integer $id Идинтификатор Страницу для отображения
	 *
	 * @return void
	 */
	public function actionView( $id ) {
		$this->render( 'view', [ 'model' => $this->loadModel( $id ) ] );
	}

	/**
	 * Создает новую модель Страницы.
	 * Если создание прошло успешно - перенаправляет на просмотр.
	 *
	 * @return void
	 */
	public function actionCreate() {
		$model = new TildaPage;

		if ( Yii::app()->getRequest()->getPost( 'TildaPage' ) !== NULL ) {
			$model->setAttributes( Yii::app()->getRequest()->getPost( 'TildaPage' ) );

			if ( $model->save() ) {
				Yii::app()->user->setFlash( yupe\widgets\YFlashMessages::SUCCESS_MESSAGE, Yii::t( 'TildaModule.tilda', 'Запись добавлена!' ) );

				$this->redirect( (array) Yii::app()->getRequest()->getPost( 'submit-type', [
						'update',
						'id' => $model->id
					] ) );
			}
		}
		$this->render( 'create', [ 'model' => $model ] );
	}

	/**
	 * Редактирование Страницы.
	 *
	 * @param integer $id Идинтификатор Страницу для редактирования
	 *
	 * @return void
	 */
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id );

		if ( Yii::app()->getRequest()->getPost( 'TildaPage' ) !== NULL ) {
			$model->setAttributes( Yii::app()->getRequest()->getPost( 'TildaPage' ) );

			if ( $model->save() ) {
				Yii::app()->user->setFlash( yupe\widgets\YFlashMessages::SUCCESS_MESSAGE, Yii::t( 'TildaModule.tilda', 'Запись обновлена!' ) );

				$this->redirect( (array) Yii::app()->getRequest()->getPost( 'submit-type', [
						'update',
						'id' => $model->id
					] ) );
			}
		}
		$this->render( 'update', [ 'model' => $model ] );
	}

	/**
	 * Удаляет модель Страницы из базы.
	 * Если удаление прошло успешно - возвращется в index
	 *
	 * @param integer $id идентификатор Страницы, который нужно удалить
	 *
	 * @return void
	 * @throws CHttpException
	 */
	public function actionDelete( $id ) {
		if ( Yii::app()->getRequest()->getIsPostRequest() ) {
			// поддерживаем удаление только из POST-запроса
			$this->loadModel( $id )->delete();

			Yii::app()->user->setFlash( yupe\widgets\YFlashMessages::SUCCESS_MESSAGE, Yii::t( 'TildaModule.tilda', 'Запись удалена!' ) );

			// если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
			if ( ! Yii::app()->getRequest()->getIsAjaxRequest() ) {
				$this->redirect( Yii::app()->getRequest()->getPost( 'returnUrl', [ 'index' ] ) );
			}
		} else {
			throw new CHttpException( 400, Yii::t( 'TildaModule.tilda', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы' ) );
		}
	}

	/**
	 * Управление Страницами.
	 *
	 * @return void
	 */
	public function actionIndex() {
		$model = new TildaPage( 'search' );
		$model->unsetAttributes(); // clear any default values
		if ( Yii::app()->getRequest()->getParam( 'TildaPage' ) !== NULL ) {
			$model->setAttributes( Yii::app()->getRequest()->getParam( 'TildaPage' ) );
		}
		$this->render( 'index', [ 'model' => $model ] );
	}

	/**
	 * Возвращает модель по указанному идентификатору
	 * Если модель не будет найдена - возникнет HTTP-исключение.
	 *
	 * @param $id integer идентификатор нужной модели
	 *
	 * @return TildaPage    * @throws CHttpException
	 */
	public function loadModel( $id ) {
		$model = TildaPage::model()->findByPk( $id );
		if ( $model === NULL ) {
			throw new CHttpException( 404, Yii::t( 'TildaModule.tilda', 'Запрошенная страница не найдена.' ) );
		}

		return $model;
	}
}
