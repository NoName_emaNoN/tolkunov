<?php
/**
* Класс ContentblockController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class TildaContentblockBackendController extends \yupe\components\controllers\BackController
{
    /**
    * @return array
    */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Tilda.Contentblock.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Tilda.Contentblock.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Tilda.Contentblock.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Tilda.Contentblock.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Tilda.Contentblock.Delete']],
            ['deny'],
        ];
    }

    /**
    * @return array
    */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'TildaContentblock',
                'validAttributes' => ['title', 'name', 'slug', 'status'],
            ],
        ];
    }

    /**
    * Отображает Блок по указанному идентификатору
    *
    * @param integer $id Идинтификатор Блок для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
    * Создает новую модель Блока.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new TildaContentblock;

        if (Yii::app()->getRequest()->getPost('TildaContentblock') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('TildaContentblock'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TildaModule.tilda', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
    * Редактирование Блока.
    *
    * @param integer $id Идинтификатор Блок для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('TildaContentblock') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('TildaContentblock'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('TildaModule.tilda', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
    * Удаляет модель Блока из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Блока, который нужно удалить
    *
    * @return void
    * @throws CHttpException
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('TildaModule.tilda', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('TildaModule.tilda', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
    * Управление Блоками.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new TildaContentblock('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('TildaContentblock') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('TildaContentblock'));
        $this->render('index', ['model' => $model]);
    }

    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param $id integer идентификатор нужной модели
    *
    * @return TildaContentblock    * @throws CHttpException
    */
    public function loadModel($id)
    {
        $model = TildaContentblock::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('TildaModule.tilda', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
