<?php

class TildaPageController extends \yupe\components\controllers\FrontController
{
    public $layout = "//layouts/tilda";

    public function actionViewAsBlock($url)
    {
        $this->actionView($url, true);
    }

    public function actionView($url, $asBlock = false)
    {
        $page = TildaPage::model()->active()->forPage($url)->find();

        if ($page === null) {
            throw new CHttpException(404, Yii::t('TildaPage.tilda', 'Page "{url}" was not found!', ['{url}' => $url]));
        }

        /* @var $tilda Tilda */
        $tilda = Yii::app()->getModule('tilda')->getComponent('Tilda');

        $tilda_data = Yii::app()->getCache()->get($page->getCacheId());
        if (false === $tilda_data) {
            /* @var $api TildaApi */
            $api = $this->getModule()->getComponent('TildaApi');

            $tilda_page = $api->getPageExport($page->tilda_FK);
            $tilda_project = $api->getProjectExport($page->tilda_projectId);

            $tilda_data = [
                'page' => $tilda_page,
                'project' => $tilda_project,
            ];

            $tilda->cacheTildaPageAssets($page->tilda_FK);

            Yii::app()->getCache()->set($page->getCacheId(), $tilda_data, Yii::app()->getModule('tilda')->pageCacheTime);
        }

        $tilda->cacheTildaAssets();

        $content = '';
        if (!empty($tilda_data['page']->id)) {
            $content = $tilda_data['page']->html;

            if (!empty($tilda_data['project']->id)) {
                if (!empty($tilda_data['project']->js)) {
                    $jsBaseUrl = $tilda_data['project']->export_jspath;

                    foreach ($tilda_data['project']->js as $jsAsset) {
                        Yii::app()->getClientScript()->registerScriptFile($jsBaseUrl . '/' . $jsAsset->to, CClientScript::POS_HEAD);
                    }
                }
                if (!empty($tilda_data['project']->css)) {
                    $cssBaseUrl = $tilda_data['project']->export_csspath;

                    foreach ($tilda_data['project']->css as $cssAsset) {
                        Yii::app()->getClientScript()->registerCssFile($cssBaseUrl . '/' . $cssAsset->to);
                    }
                }
            }
        }

        if ($asBlock) {
            $this->layout = '//layouts/tilda_block';
        }

        $this->render('view', [
            'content' => $content,
            'pageModel' => $page,
        ]);
    }

    public function actionUpdate($pageid = '', $projectid = '', $published = '', $publickey = '')
    {
        $model = TildaPage::model()->findByTildaFK($pageid, $projectid);

        if (!$model) {
            $model = TildaContentblock::model()->findByTildaFK($pageid, $projectid);
        }

        if ($model) {
            /* @var $tilda Tilda */
            $tilda = Yii::app()->getModule('tilda')->getComponent('Tilda');

            Yii::app()->getCache()->delete($model->getCacheId());
            Yii::app()->getCache()->delete($tilda->getTildaPageDataCacheKey($pageid));
            Yii::app()->getCache()->delete($tilda->getTildaAssetsCachedKey());
        }

        echo 'ok';
    }
}
