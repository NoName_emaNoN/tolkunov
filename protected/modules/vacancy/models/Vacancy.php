<?php

/**
 * This is the model class for table "{{vacancy_vacancy}}".
 *
 * The followings are the available columns in table '{{vacancy_vacancy}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $short_title
 * @property string $title
 * @property string $slug
 * @property string $image
 * @property string $text_1
 * @property string $text_2
 * @property integer $status
 * @property integer $order
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * @method Vacancy published()
 */
class Vacancy extends yupe\models\YModel
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_MODERATION = 2;

    public $searchResumeCount;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{vacancy_vacancy}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['short_title, title, slug, seo_title, seo_description, seo_keywords', 'filter', 'filter' => 'trim'],
            ['short_title, title, slug, seo_title, seo_description, seo_keywords', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['title, status, slug', 'required'],
            ['status, order', 'numerical', 'integerOnly' => true],
            ['short_title, title, seo_title, seo_description, seo_keywords', 'length', 'max' => 255],
            ['text_1, text_2', 'safe'],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('VacancyModule.vacancy', 'Bad characters in {attribute} field'),
            ],
            ['slug', 'length', 'max' => 150],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, short_title, title, slug, text_1, text_2, status, order, image, searchResumeCount, seo_title, seo_description, seo_keywords', 'safe', 'on' => 'search'],
        ];
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('vacancy');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/vacancy/vacancy/view',
                'params' => [
                    'slug' => function (Vacancy $data) {
                        return $data->slug;
                    },
                ],
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'order',
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
//                'defaultImage' => $module->defaultImage,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
        ];
    }

    public function defaultScope()
    {
        return [
            'order' => $this->getTableAlias(false, false) . '.`order` ASC',
//            'order' => $this->getTableAlias(false, false) . '.`order` ASC, ' . $this->getTableAlias(false, false) . '.create_time ASC',
        ];
    }

    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'short_title' => 'Кого ищем',
            'title' => 'Название',
            'slug' => 'URL',
            'image' => 'Изображение',
            'text_1' => 'Текст',
//            'text_2' => 'Что для нас важно:',
            'status' => 'Статус',
            'order' => 'Сортировка',
            'searchResumeCount' => 'Кол-во резюме',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.short_title', $this->short_title, true);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.text_1', $this->text_1, true);
        $criteria->compare($this->tableAlias . '.text_2', $this->text_2, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);
        $criteria->compare($this->tableAlias . '.order', $this->order);

        $resume_table = VacancyResume::model()->tableName();
        $resume_count_sql = "(select count(*) from $resume_table pt where pt.vacancy_id = t.id)";

        $criteria->select = [
            '*',
            $resume_count_sql . ' as searchResumeCount',
        ];

        $criteria->compare($resume_count_sql, $this->searchResumeCount);

        return new CActiveDataProvider(
            get_class($this), [
                'criteria' => $criteria,
                'sort' => [
                    'defaultOrder' => $this->tableAlias . '.order',
                    'attributes' => [
                        // order by
                        'searchResumeCount' => [
                            'asc' => 'searchResumeCount ASC',
                            'desc' => 'searchResumeCount DESC',
                        ],
                        '*',
                    ],
                ],
            ]
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Vacancy the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_PUBLISHED => 'Опубликовано',
            self::STATUS_DRAFT => "Черновик",
            self::STATUS_MODERATION => "На модерации",
        ];
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : '*unknown*';
    }
}
