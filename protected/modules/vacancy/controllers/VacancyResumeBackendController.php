<?php

/**
 * Класс VacancyResumeBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class VacancyResumeBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Vacancy.VacancyResumeBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Vacancy.VacancyResumeBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Vacancy.VacancyResumeBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Vacancy.VacancyResumeBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Vacancy.VacancyResumeBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'VacancyResume',
                'validAttributes' => ['title', 'name', 'slug', 'status', 'vacancy_id'],
            ],
        ];
    }

    /**
     * Отображает резюме по указанному идентификатору
     *
     * @param integer $id Идинтификатор резюме для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель резюме.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new VacancyResume;

        if (Yii::app()->getRequest()->getPost('VacancyResume') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('VacancyResume'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VacancyModule.vacancy', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование резюме.
     *
     * @param integer $id Идинтификатор резюме для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('VacancyResume') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('VacancyResume'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('VacancyModule.vacancy', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель резюме из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор резюме, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('VacancyModule.vacancy', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('VacancyModule.vacancy', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление резюме.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new VacancyResume('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('VacancyResume') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('VacancyResume'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return VacancyResume    * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = VacancyResume::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('VacancyModule.vacancy', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
