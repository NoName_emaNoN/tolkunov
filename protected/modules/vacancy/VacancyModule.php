<?php

use yupe\components\WebModule;

class VacancyModule extends yupe\components\WebModule
{
    const VERSION = '1.0';

    /**
     * @var string
     */
    public $uploadPath = 'vacancy';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array или false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'VacancyModule.vacancy',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('VacancyModule.vacancy', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'vacancy',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('VacancyModule.vacancy', 'Контент');
    }

    /**
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('VacancyModule.vacancy', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'VacancyModule.vacancy',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "yupe"
                        )->uploadPath,
                ]
            ),
            'allowedExtensions' => Yii::t('VacancyModule.vacancy', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('VacancyModule.vacancy', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('VacancyModule.vacancy', 'Maximum size (in bytes)'),
            'mainCategory' => Yii::t('VacancyModule.vacancy', 'Main messages category'),
        ];
    }

    /**
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'adminMenuOrder',
//            'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * @return array
     */
//    public function getEditableParamsGroups()
//    {
//        return [
//            'main' => [
//                'label' => Yii::t('VacancyModule.vacancy', 'General module settings'),
//                'items' => [
//                    'editor',
//                ],
//            ],
//            'images' => [
//                'label' => Yii::t('VacancyModule.vacancy', 'Images settings'),
//                'items' => [
//                    'uploadPath',
//                    'allowedExtensions',
//                    'minSize',
//                    'maxSize',
//                ],
//            ],
//        ];
//    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('VacancyModule.vacancy', 'Вакансии')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VacancyModule.vacancy', 'Список вакансий'),
                'url' => ['/vacancy/vacancyBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('VacancyModule.vacancy', 'Добавить вакансию'),
                'url' => ['/vacancy/vacancyBackend/create'],
            ],
            '',
            ['label' => Yii::t('VacancyModule.vacancy', 'Резюме')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('VacancyModule.vacancy', 'Список резюме'),
                'url' => ['/vacancy/vacancyResumeBackend/index'],
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('VacancyModule.vacancy', self::VERSION);
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('VacancyModule.vacancy', 'http://zexed.net');
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('VacancyModule.vacancy', 'Вакансии');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('VacancyModule.vacancy', 'Описание модуля "Вакансии"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('VacancyModule.vacancy', 'Mikhail Chemezov');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('VacancyModule.vacancy', 'michlenanosoft@gmail.com');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/vacancy/vacancyBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-linkedin-square";
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'vacancy.models.*',
                'vacancy.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Vacancy.VacancyManager',
                'description' => Yii::t('VacancyModule.vacancy', 'Управление вакансиями'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vacancy.VacancyBackend.Create',
                        'description' => Yii::t('VacancyModule.vacancy', 'Создание вакансии'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vacancy.VacancyBackend.Delete',
                        'description' => Yii::t('VacancyModule.vacancy', 'Удаление вакансии'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vacancy.VacancyBackend.Index',
                        'description' => Yii::t('VacancyModule.vacancy', 'Просмотр списка вакансий'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vacancy.VacancyBackend.Update',
                        'description' => Yii::t('VacancyModule.vacancy', 'Редактирование вакансии'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Vacancy.VacancyBackend.View',
                        'description' => Yii::t('VacancyModule.vacancy', 'Просмотр вакансии'),
                    ],
                ],
            ],
        ];
    }
}
