<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model VacancyResume
 * @var $this VacancyResumeBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('VacancyModule.vacancy', 'Резюме') => ['/vacancy/vacancyResumeBackend/index'],
    Yii::t('VacancyModule.vacancy', 'Управление'),
];

$this->pageTitle = Yii::t('VacancyModule.vacancy', 'Резюме - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('VacancyModule.vacancy', 'Управление резюме'), 'url' => ['/vacancy/vacancyResumeBackend/index']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('VacancyModule.vacancy', 'Резюме'); ?>
        <small><?= Yii::t('VacancyModule.vacancy', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('VacancyModule.vacancy', 'Поиск резюме'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('vacancy-resume-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('VacancyModule.vacancy', 'В данном разделе представлены средства управления резюме'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'vacancy-resume-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            'create_time',
            'update_time',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'vacancy_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(Vacancy::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            'name',
            'email',
            'phone',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    $model::STATUS_NEW => ['class' => 'label-default'],
                    $model::STATUS_PROCESSING => ['class' => 'label-primary'],
                    $model::STATUS_DONE => ['class' => 'label-success'],
                    $model::STATUS_DENIED => ['class' => 'label-danger'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'buttons' => [
                    'update' => [
                        'visible' => function ($row, $data) {
                            return false;
                        },
                    ],
                ],
            ],
        ],
    ]
); ?>
