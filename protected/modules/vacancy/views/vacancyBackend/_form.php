<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Vacancy
 * @var $form \yupe\widgets\ActiveForm
 * @var $this VacancyBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'vacancy-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('VacancyModule.vacancy', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('VacancyModule.vacancy', 'обязательны.'); ?>
    </div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'title'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->slugFieldGroup(
                $model,
                'slug',
                [
                    'sourceAttribute' => 'title',
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'short_title', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                        'data-original-title' => $model->getAttributeLabel('short_title'),
                        'data-content' => $model->getAttributeDescription('short_title'),
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class='row'>
        <div class="col-sm-7">
            <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                <?=
                CHtml::image(
                    !$model->getIsNewRecord() && $model->image ? $model->getImageUrl(200, 200, true) : '#',
                    $model->title,
                    [
                        'class' => 'preview-image img-thumbnail',
                        'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                    ]
                ); ?>
            </div>

            <?php if (!$model->getIsNewRecord() && $model->image): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file"> <?= Yii::t(
                            'YupeModule.yupe',
                            'Delete the file'
                        ) ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'image',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'onchange' => 'readURL(this);',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 <?php echo $model->hasErrors('text_1') ? 'has-error' : ''; ?>">
            <?php echo $form->labelEx($model, 'text_1'); ?>
            <?php $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'text_1',
                ]
            ); ?>
            <?php echo $form->error($model, 'text_1'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                [
                    'widgetOptions' => [
                        'data' => $model->getStatusList(),
                    ],
                ]
            ); ?>
        </div>
    </div>

    <br/>

    <div class="row">
        <div class="col-sm-12">
            <?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
            <div class="panel-group" id="extended-options">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <a data-toggle="collapse" data-parent="#extended-options" href="#collapseTwo">
                                <?php echo Yii::t('VacancyModule.vacancy', 'Данные для SEO'); ?>
                            </a>
                        </div>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <?= $form->textFieldGroup($model, 'seo_title'); ?>
                            <?= $form->textFieldGroup($model, 'seo_keywords'); ?>
                            <?= $form->textAreaGroup($model, 'seo_description', ['widgetOptions' => ['htmlOptions' => ['rows' => 8]]]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>

    <br/>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('VacancyModule.vacancy', 'Сохранить вакансию и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('VacancyModule.vacancy', 'Сохранить вакансию и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>