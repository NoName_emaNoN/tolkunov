<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('VacancyModule.vacancy', 'Вакансии') => ['/vacancy/vacancyBackend/index'],
    Yii::t('VacancyModule.vacancy', 'Управление'),
];

$this->pageTitle = Yii::t('VacancyModule.vacancy', 'Вакансии - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('VacancyModule.vacancy', 'Управление вакансиями'), 'url' => ['/vacancy/vacancyBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('VacancyModule.vacancy', 'Добавить вакансию'), 'url' => ['/vacancy/vacancyBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('VacancyModule.vacancy', 'Вакансии'); ?>
        <small><?php echo Yii::t('VacancyModule.vacancy', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('VacancyModule.vacancy', 'Поиск вакансий'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('vacancy-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?php echo Yii::t('VacancyModule.vacancy', 'В данном разделе представлены средства управления вакансиями'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'vacancy-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'order',
        'sortableAction' => '/vacancy/vacancyBackend/sortable',
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            [
                'type' => 'raw',
                'name' => 'image',
                'filter' => false,
                'value' => function (Vacancy $data) {
                    return CHtml::image($data->getImageUrl(120, 80), $data->title, ["width" => 120, "height" => 80, "class" => "img-thumbnail"]);
                },
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            'create_time',
            'update_time',
//            'text_1',
//            'text_2',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Vacancy::STATUS_PUBLISHED => ['class' => 'label-success'],
                    Vacancy::STATUS_MODERATION => ['class' => 'label-warning'],
                    Vacancy::STATUS_DRAFT => ['class' => 'label-default'],
                ],
            ],
            [
                'name' => 'searchResumeCount',
                'type' => 'raw',
                'value' => 'CHtml::link($data->searchResumeCount, ["/vacancy/vacancyResumeBackend/index", "VacancyResume[vacancy_id]" => $data->id])',
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
