<?php
return [
    'module' => [
        'class' => 'application.modules.vacancy.VacancyModule',
    ],
    'import' => [
        'application.modules.vacancy.listeners.*',
        'application.modules.vacancy.models.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\VacancySitemapGeneratorListener', 'onGenerate'],
                ],
            ],
        ],
    ],
    'rules' => [
        '/vacancy' => '/vacancy/vacancy/index',
        '/vacancy/create' => '/vacancy/vacancy/create',
        '/vacancy/<slug:[\w-]+>' => [
            '/vacancy/vacancy/view',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{vacancy_vacancy}}',
                    'field' => 'slug',
                ],
            ],
        ],
    ],
];
