<?php

/**
 * Team install migration
 * Класс миграций для модуля Team:
 *
 * @category YupeMigration
 * @package  yupe.modules.team.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000001_create_vacancy_resume extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        /* Vacancy Resume */
        $this->createTable(
            '{{vacancy_resume}}',
            [
                'id' => 'pk',
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'vacancy_id' => 'integer NOT NULL',
                'name' => 'string',
                'phone' => 'string',
                'email' => 'string',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{vacancy_resume}}_create_time", '{{vacancy_resume}}', "create_time", false);
        $this->createIndex("ix_{{vacancy_resume}}_update_time", '{{vacancy_resume}}', "update_time", false);
        $this->createIndex("ix_{{vacancy_resume}}_status", '{{vacancy_resume}}', "status", false);
        $this->createIndex("ix_{{vacancy_resume}}_vacancy_id", '{{vacancy_resume}}', "vacancy_id", false);

        //fk
        $this->addForeignKey('fk_{{vacancy_resume}}_vacancy_id', '{{vacancy_resume}}', 'vacancy_id', '{{vacancy_vacancy}}', 'id', 'CASCADE');

        /* Vacancy Resume Files */
        $this->createTable(
            '{{vacancy_resume_file}}',
            [
                'id' => 'pk',
                'resume_id' => 'integer NOT NULL',
                'file' => 'string',
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{vacancy_resume_file}}_resume_id", '{{vacancy_resume_file}}', "resume_id", false);

        //fk
        $this->addForeignKey('fk_{{vacancy_resume_file}}_resume_id', '{{vacancy_resume_file}}', 'resume_id', '{{vacancy_resume}}', 'id', 'CASCADE');
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{vacancy_resume_file}}');
        $this->dropTableWithForeignKeys('{{vacancy_resume}}');
    }
}
