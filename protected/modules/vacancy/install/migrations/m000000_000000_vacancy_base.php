<?php

/**
 * Team install migration
 * Класс миграций для модуля Team:
 *
 * @category YupeMigration
 * @package  yupe.modules.team.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_vacancy_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{vacancy_vacancy}}',
            [
                'id' => 'pk',
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'image' => 'string',
                'text_1' => 'text',
                'text_2' => 'text',
                'status' => 'tinyint(1) NOT NULL DEFAULT 0',
                'order' => "integer NOT NULL DEFAULT 1",
            ],
            $this->getOptions()
        );

        //ix
        $this->createIndex("ix_{{vacancy_vacancy}}_create_time", '{{vacancy_vacancy}}', "create_time", false);
        $this->createIndex("ix_{{vacancy_vacancy}}_update_time", '{{vacancy_vacancy}}', "update_time", false);
        $this->createIndex("ix_{{vacancy_vacancy}}_order", '{{vacancy_vacancy}}', "order", false);
        $this->createIndex("ix_{{vacancy_vacancy}}_status", '{{vacancy_vacancy}}', "status", false);
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{vacancy_vacancy}}');
    }
}
