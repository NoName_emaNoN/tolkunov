<?php

/**
 * Class LastPortfolioWidget
 */
class LastPortfolioWidget extends yupe\widgets\YWidget
{
    /**
     * @var string
     */
    public $view = 'last_portfolio_widget';

    /**
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = (int)$this->limit;
        $criteria->order = 't.date DESC';

        $models = Portfolio::model()->published()->findAll($criteria);

        $this->render($this->view, ['models' => $models]);
    }
}
