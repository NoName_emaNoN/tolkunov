<?php
/**
* Класс PortfolioTagBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class PortfolioTagBackendController extends \yupe\components\controllers\BackController
{
    /**
    * @return array
    */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Portfolio.PortfolioTagBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Portfolio.PortfolioTagBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Portfolio.PortfolioTagBackend.Create']],
            ['allow', 'actions' => ['update', 'inline'], 'roles' => ['Portfolio.PortfolioTagBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Portfolio.PortfolioTagBackend.Delete']],
            ['deny'],
        ];

    }

    /**
    * @return array
    */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'PortfolioTag',
                'validAttributes' => ['title', 'name', 'slug', 'status', 'section_id'],
            ],
        ];
    }

    /**
    * Отображает Тег по указанному идентификатору
    *
    * @param integer $id Идинтификатор Тег для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
    * Создает новую модель Тега.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new PortfolioTag;

        if (Yii::app()->getRequest()->getPost('PortfolioTag') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('PortfolioTag'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
    * Редактирование Тега.
    *
    * @param integer $id Идинтификатор Тег для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('PortfolioTag') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('PortfolioTag'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
    * Удаляет модель Тега из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Тега, который нужно удалить
    *
    * @return void
    * @throws CHttpException
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('PortfolioModule.portfolio', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('PortfolioModule.portfolio', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }

    /**
    * Управление Тегами.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new PortfolioTag('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('PortfolioTag') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('PortfolioTag'));
        $this->render('index', ['model' => $model]);
    }

    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param $id integer идентификатор нужной модели
    *
    * @return PortfolioTag    * @throws CHttpException
    */
    public function loadModel($id)
    {
        $model = PortfolioTag::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('PortfolioModule.portfolio', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
