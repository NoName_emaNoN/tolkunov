<?php

/**
 * Класс PortfolioThemeBackendController:
 *
 * @category Yupe\yupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class PortfolioThemeBackendController extends \yupe\components\controllers\BackController
{
    /**
     * @return array
     */
    public function accessRules()
    {
        return [
            ['allow', 'roles' => ['admin']],
            ['allow', 'actions' => ['index'], 'roles' => ['Portfolio.PortfolioThemeBackend.Index']],
            ['allow', 'actions' => ['view'], 'roles' => ['Portfolio.PortfolioThemeBackend.View']],
            ['allow', 'actions' => ['create'], 'roles' => ['Portfolio.PortfolioThemeBackend.Create']],
            ['allow', 'actions' => ['update', 'inline', 'sortable'], 'roles' => ['Portfolio.PortfolioThemeBackend.Update']],
            ['allow', 'actions' => ['delete', 'multiaction'], 'roles' => ['Portfolio.PortfolioThemeBackend.Delete']],
            ['deny'],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'inline' => [
                'class' => 'yupe\components\actions\YInLineEditAction',
                'model' => 'PortfolioTheme',
                'validAttributes' => ['title', 'name', 'slug', 'status'],
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'PortfolioTheme',
                'attribute' => 'sort',
            ],
        ];
    }

    /**
     * Отображает сферу деятельности по указанному идентификатору
     *
     * @param integer $id Идинтификатор сферу деятельности для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }

    /**
     * Создает новую модель сферы деятельности.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new PortfolioTheme;

        if (Yii::app()->getRequest()->getPost('PortfolioTheme') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('PortfolioTheme'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        ['create']
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }

    /**
     * Редактирование сферы деятельности.
     *
     * @param integer $id Идинтификатор сферу деятельности для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('PortfolioTheme') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('PortfolioTheme'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('PortfolioModule.portfolio', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id,
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }

    /**
     * Удаляет модель сферы деятельности из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор сферы деятельности, который нужно удалить
     *
     * @return void
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('PortfolioModule.portfolio', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else {
            throw new CHttpException(400, Yii::t('PortfolioModule.portfolio', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление сферами деятельности.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new PortfolioTheme('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('PortfolioTheme') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getParam('PortfolioTheme'));
        }
        $this->render('index', ['model' => $model]);
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param $id integer идентификатор нужной модели
     *
     * @return PortfolioTheme
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PortfolioTheme::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('PortfolioModule.portfolio', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }
}
