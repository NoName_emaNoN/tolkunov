<?php
/**
 * Файл настроек для модуля portfolio
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.portfolio.install
 * @since 0.1
 *
 */
return [
    'module' => [
        'class' => 'application.modules.portfolio.PortfolioModule',
    ],
    'import' => [
        'application.modules.portfolio.listeners.*',
        'application.modules.portfolio.models.*',
        'application.modules.portfolio.helpers.*',
    ],
    'component' => [
        'eventManager' => [
            'class' => 'yupe\components\EventManager',
            'events' => [
                'sitemap.before.generate' => [
                    ['\PortfolioSitemapGeneratorListener', 'onGenerate'],
                ],
            ],
        ],
    ],
    'rules' => [
        '/portfolio/sizes' => '/portfolio/portfolio/sizes',
        '/portfolio/banners-<width:[\d-]+>x<height:[\d-]+>' => '/portfolio/portfolio/size',
        '/portfolio/<section_slug:[\w-]+>' => [
            '/portfolio/portfolio/section',
            'type' => 'db',
            'fields' => [
                'section_slug' => [
                    'table' => '{{portfolio_section}}',
                    'field' => 'slug',
                ],
            ],
        ],
//        '/portfolio/favorite' => '/portfolio/portfolio/favorite',
//        '/portfolio/themes' => '/portfolio/portfolio/themes',
//        '/portfolio/theme/<slug:[\w-]+>' => [
//            '/portfolio/portfolio/theme',
//            'type' => 'db',
//            'fields' => [
//                'slug' => [
//                    'table' => '{{portfolio_theme}}',
//                    'field' => 'slug',
//                ],
//            ],
//        ],
//        '/portfolio/styles' => '/portfolio/portfolio/styles',
//        '/portfolio/style/<slug:[\w-]+>' => [
//            '/portfolio/portfolio/style',
//            'type' => 'db',
//            'fields' => [
//                'slug' => [
//                    'table' => '{{portfolio_style}}',
//                    'field' => 'slug',
//                ],
//            ],
//        ],
        '/portfolio/<section:[\w-]+>/<tag:[\w-]+>' => [
            '/portfolio/portfolio/index',
            'type' => 'db',
            'fields' => [
                'section' => [
                    'table' => '{{portfolio_section}}',
                    'field' => 'slug',
                ],
                'tag' => [
                    'table' => '{{portfolio_tag}}',
                    'field' => 'slug',
                ],
            ],
        ],
        '/portfolio/<slug:[\w-]+>' => [
            '/portfolio/portfolio/view',
            'type' => 'db',
            'fields' => [
                'slug' => [
                    'table' => '{{portfolio_portfolio}}',
                    'field' => 'slug',
                ],
            ],
        ],
        '/portfolio' => '/portfolio/portfolio/index',
    ],
];
