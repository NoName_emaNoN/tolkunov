<?php

class m181219_224322_migrate_portfolio_themes extends yupe\components\DbMigration
{
    public function safeUp()
    {
        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} p INNER JOIN {{portfolio_theme}} pt ON pt.id = p.theme_id SET p.theme = pt.title')->execute();
    }

    public function safeDown()
    {

    }
}
