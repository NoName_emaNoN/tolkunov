<?php

class m170428_144255_add_portfolio_item_order extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_item}}', 'order', 'integer NOT NULL DEFAULT 1');

        $this->getDbConnection()->createCommand('UPDATE {{portfolio_item}} SET `order` = `id`')->execute();

        $this->createIndex("ix_{{portfolio_item}}_order", '{{portfolio_item}}', "order", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_item}}', 'order');
    }
}
