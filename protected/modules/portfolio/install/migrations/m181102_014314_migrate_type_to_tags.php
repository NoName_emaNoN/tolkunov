<?php

class m181102_014314_migrate_type_to_tags extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $section = new PortfolioSection();
        $section->title = 'По типу';
        $section->slug = 'types';

        if (!$section->save()) {
            throw new Exception('Не удалось сохранить раздел');
        }

        $typeToTag = [];

        foreach (Portfolio::model()->getTypeList() as $key => $value) {
            $tag = new PortfolioTag();
            $tag->title = $value;
            $tag->name = $value;
            $tag->section_id = $section->id;

            if (!$tag->save()) {
                throw new Exception('Не удалось сохранить tag');
            }

            $typeToTag[$key] = $tag->id;
        }

        foreach (Portfolio::model()->findAll() as $portfolio) {
            if ($portfolio->type) {
                $model = new PortfolioToTag();
                $model->portfolio_id = $portfolio->id;
                $model->tag_id = $typeToTag[$portfolio->type];

                if (false === $model->save()) {
                    throw new Exception('Не удалось сохранить portfolio-to-tag');
                }
            }
        }
    }

    public function safeDown()
    {

    }
}
