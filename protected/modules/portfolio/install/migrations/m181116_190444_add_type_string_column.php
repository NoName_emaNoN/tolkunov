<?php

class m181116_190444_add_type_string_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'type_string', 'string DEFAULT NULL');

        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `type_string` = "HTML5" WHERE `type` = 1')->execute();
        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `type_string` = "GIF" WHERE `type` = 2')->execute();
        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `type_string` = "JPG" WHERE `type` = 3')->execute();
        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `type_string` = "Видео" WHERE `type` = 4')->execute();
        Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `type_string` = "Сторис" WHERE `type` = 5')->execute();
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'type_string');
    }
}
