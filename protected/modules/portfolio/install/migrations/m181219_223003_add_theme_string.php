<?php

class m181219_223003_add_theme_string extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'theme', 'string');

        $this->createIndex("ix_{{portfolio_portfolio}}_theme", '{{portfolio_portfolio}}', "theme", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'theme');
    }
}
