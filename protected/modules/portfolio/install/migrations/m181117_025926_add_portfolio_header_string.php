<?php

class m181117_025926_add_portfolio_header_string extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'header_string', 'string DEFAULT NULL');
        $this->addColumn('{{portfolio_portfolio}}', 'header_tag_id', 'integer DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'header_string');
        $this->dropColumn('{{portfolio_portfolio}}', 'header_tag_id');
    }
}
