<?php

class m180622_181014_create_portfolio_tag_table extends yupe\components\DbMigration {
	public function safeUp() {
		$this->createTable( '{{portfolio_tag}}', [
			'id'                     => 'pk',
			//для удобства добавлены некоторые базовые поля, которые могут пригодиться.
			'create_time'            => 'datetime NOT NULL',
			'update_time'            => 'datetime NOT NULL',
			'name'                   => 'string NOT NULL',
			'title'                  => 'string NOT NULL',
			'slug'                   => 'varchar(150) NOT NULL',
			'sort'                   => 'integer NOT NULL DEFAULT 1',
			'show_main_page'         => 'boolean DEFAULT 0',
			'image'                  => 'string',
			'image_hover'            => 'string',
			'color_background'       => 'varchar(25)',
			'color_background_hover' => 'varchar(25)',
			'color_text'             => 'varchar(25)',
			'color_text_hover'       => 'varchar(25)',

			//			'seo_title'       => 'string',
			//			'seo_keywords'    => 'string',
			//			'seo_description' => 'string',
		], $this->getOptions() );

		$this->createIndex( "ix_{{portfolio_tag}}_sort", '{{portfolio_tag}}', "sort", false );
		$this->createIndex( "ix_{{portfolio_tag}}_slug", '{{portfolio_tag}}', "slug", true );

		$this->createTable( '{{portfolio_portfolio_to_tag}}', [
			'id'           => 'pk',
			'portfolio_id' => 'integer NOT NULL',
			'tag_id'       => 'integer NOT NULL',
		], $this->getOptions() );

		$this->createIndex( "ix_{{portfolio_portfolio_to_tag}}_portfolio_id_tag_id", '{{portfolio_portfolio_to_tag}}', [ 'portfolio_id', 'tag_id' ], true );

		$this->addForeignKey(
			'fk_{{portfolio_portfolio_to_tag}}_portfolio_id',
			'{{portfolio_portfolio_to_tag}}',
			'portfolio_id',
			'{{portfolio_portfolio}}',
			'id',
			'CASCADE',
			'NO ACTION'
		);
		$this->addForeignKey(
			'fk_{{portfolio_portfolio_to_tag}}_tag_id',
			'{{portfolio_portfolio_to_tag}}',
			'tag_id',
			'{{portfolio_tag}}',
			'id',
			'CASCADE',
			'NO ACTION'
		);
	}

	public function safeDown() {
		$this->dropTableWithForeignKeys( '{{portfolio_tag}}' );
		$this->dropTableWithForeignKeys( '{{portfolio_portfolio_to_tag}}' );
	}
}
