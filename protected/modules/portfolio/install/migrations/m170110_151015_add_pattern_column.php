<?php

class m170110_151015_add_pattern_column extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'pattern', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'pattern');
    }
}
