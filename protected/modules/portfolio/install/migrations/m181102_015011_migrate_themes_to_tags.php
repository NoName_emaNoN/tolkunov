<?php

class m181102_015011_migrate_themes_to_tags extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $section = new PortfolioSection();
        $section->title = 'По темам';
        $section->slug = 'themes';

        if (!$section->save()) {
            throw new Exception('Не удалось сохранить раздел');
        }

        $typeToTag = [];

        foreach (PortfolioTheme::model()->findAll() as $value) {
            $tag = new PortfolioTag();
            $tag->title = $value->title;
            $tag->name = $value->title;
            $tag->section_id = $section->id;
            $tag->sort = $value->sort;

            if (!$tag->save()) {
                throw new Exception('Не удалось сохранить tag');
            }

            $typeToTag[$value->id] = $tag->id;
        }

        foreach (Portfolio::model()->findAll() as $portfolio) {
            if ($portfolio->theme_id) {
                $model = new PortfolioToTag();
                $model->portfolio_id = $portfolio->id;
                $model->tag_id = $typeToTag[$portfolio->theme_id];

                if (false === $model->save()) {
                    throw new Exception('Не удалось сохранить portfolio-to-tag');
                }
            }
        }
    }

    public function safeDown()
    {

    }
}
