<?php

class m181219_165804_add_banner_border extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'border_width', 'integer NOT NULL DEFAULT 0');
        $this->addColumn('{{portfolio_portfolio}}', 'border_color', 'varchar(25) DEFAULT NULL');
        $this->addColumn('{{portfolio_portfolio}}', 'border_opacity', 'integer NOT NULL DEFAULT 100');
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'border_width');
        $this->dropColumn('{{portfolio_portfolio}}', 'border_color');
        $this->dropColumn('{{portfolio_portfolio}}', 'border_opacity');
    }
}
