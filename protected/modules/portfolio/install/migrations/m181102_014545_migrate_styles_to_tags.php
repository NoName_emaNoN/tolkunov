<?php

class m181102_014545_migrate_styles_to_tags extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $section = new PortfolioSection();
        $section->title = 'По стилям';
        $section->slug = 'styles';

        if (!$section->save()) {
            throw new Exception('Не удалось сохранить раздел');
        }

        $typeToTag = [];

        foreach (PortfolioStyle::model()->findAll() as $value) {
            $tag = new PortfolioTag();
            $tag->title = $value->title;
            $tag->name = $value->title;
            $tag->section_id = $section->id;
            $tag->sort = $value->sort;

            if (!$tag->save()) {
                throw new Exception('Не удалось сохранить tag');
            }

            $typeToTag[$value->id] = $tag->id;
        }

        foreach (Portfolio::model()->findAll() as $portfolio) {
            if ($portfolio->style_id) {
                $model = new PortfolioToTag();
                $model->portfolio_id = $portfolio->id;
                $model->tag_id = $typeToTag[$portfolio->style_id];

                if (false === $model->save()) {
                    throw new Exception('Не удалось сохранить portfolio-to-tag');
                }
            }
        }
    }

    public function safeDown()
    {

    }
}
