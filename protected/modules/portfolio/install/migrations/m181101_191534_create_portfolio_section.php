<?php

class m181101_191534_create_portfolio_section extends yupe\components\DbMigration
{
    public function safeUp()
    {
        /* Portfolio Section */
        $this->createTable(
            '{{portfolio_section}}',
            [
                'id' => 'pk',
                //для удобства добавлены некоторые базовые поля, которые могут пригодиться.
                'create_time' => 'datetime NOT NULL',
                'update_time' => 'datetime NOT NULL',
                'title' => 'string NOT NULL',
                'slug' => 'varchar(150) NOT NULL',
                'sort' => 'integer NOT NULL DEFAULT 1',
            ],
            $this->getOptions()
        );

        // ix
        $this->createIndex("ux_{{portfolio_section}}_slug", '{{portfolio_section}}', "slug", true);
        $this->createIndex("ix_{{portfolio_section}}_sort", '{{portfolio_section}}', "sort", false);

        $this->addColumn('{{portfolio_tag}}', 'section_id', 'integer DEFAULT NULL');

        $this->createIndex("ix_{{portfolio_tag}}_section_id", '{{portfolio_tag}}', "section_id", false);
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_tag}}', 'section_id');

        $this->dropTable('{{portfolio_section}}');
    }
}
