<?php

class m181031_190048_create_preview_video extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'preview_video', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'preview_video');
    }
}
