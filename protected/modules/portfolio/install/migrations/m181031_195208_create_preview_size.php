<?php

class m181031_195208_create_preview_size extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_portfolio}}', 'preview_size', 'string NOT NULL DEFAULT "1x1"');
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_portfolio}}', 'preview_size');
    }
}
