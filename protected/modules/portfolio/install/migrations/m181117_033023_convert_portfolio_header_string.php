<?php

class m181117_033023_convert_portfolio_header_string extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $types = [
            1 => Yii::t('PortfolioModule.portfolio', 'HTML5'),
            2 => Yii::t('PortfolioModule.portfolio', 'GIF'),
            3 => Yii::t('PortfolioModule.portfolio', 'JPG'),
            4 => Yii::t('PortfolioModule.portfolio', 'Видео'),
            5 => Yii::t('PortfolioModule.portfolio', 'Сторис'),
        ];

        $command = Yii::app()->db->createCommand('UPDATE {{portfolio_portfolio}} SET `header_string` = :header_string, header_tag_id = :header_tag_id WHERE `type` = :type');

        foreach ($types as $type_id => $type_name) {
            $tag_id = Yii::app()->db->createCommand('SELECT `id` FROM {{portfolio_tag}} WHERE `name` = :name')->queryScalar([':name' => $type_name]);
            $header_string = '[' . $type_name . '-баннер]';

            $command->execute([
                ':header_string' => $header_string,
                ':header_tag_id' => $tag_id,
                ':type' => $type_id,
            ]);
        }
    }

    public function safeDown()
    {

    }
}
