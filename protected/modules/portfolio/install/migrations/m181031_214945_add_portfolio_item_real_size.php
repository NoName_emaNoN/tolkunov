<?php

class m181031_214945_add_portfolio_item_real_size extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{portfolio_item}}', 'real_width', 'integer NOT NULL');
        $this->addColumn('{{portfolio_item}}', 'real_height', 'integer NOT NULL');

        Yii::app()->db->createCommand('UPDATE {{portfolio_item}} SET `real_width` = `width`')->execute();
        Yii::app()->db->createCommand('UPDATE {{portfolio_item}} SET `real_height` = `height`')->execute();
    }

    public function safeDown()
    {
        $this->dropColumn('{{portfolio_item}}', 'real_width');
        $this->dropColumn('{{portfolio_item}}', 'real_height');
    }
}
