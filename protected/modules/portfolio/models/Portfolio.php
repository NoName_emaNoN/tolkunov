<?php

/**
 * This is the model class for table "{{portfolio_portfolio}}".
 *
 * The followings are the available columns in table '{{portfolio_portfolio}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $type
 * @property string $type_string
 * @property string $date
 * @property integer $client_id
 * @property string $title
 * @property string $header_string
 * @property integer $header_tag_id
 * @property string $image
 * @property string $pattern
 * @property string $preview_video
 * @property string $preview_size
 * @property integer $theme_id
 * @property integer $style_id
 * @property integer $is_loved
 * @property integer $status
 * @property string $slug
 * @property string $text
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property integer $border_width
 * @property string $border_color
 * @property integer $border_opacity
 * @property string $theme
 *
 * The followings are the available model relations:
 * @property PortfolioItem[] $items
 * @property PortfolioStyle $style
 * @property Client $client
 * @property PortfolioToTag[] $tags
 * @property PortfolioTag $headerTag
 *
 * @method Portfolio published()
 */

Yii::import('vendor.yiiext.taggable-behavior.ETaggableBehavior');

class Portfolio extends yupe\models\YModel
{
    /**
     *
     */
    const STATUS_DRAFT = 0;
    /**
     *
     */
    const STATUS_PUBLISHED = 1;
    /**
     *
     */
    const STATUS_MODERATION = 2;

    const TYPE_HTML5 = 1;
    const TYPE_GIF = 2;
    const TYPE_JPG = 3;
    const TYPE_VIDEO = 4;
    const TYPE_STORY = 5;

    public $status = self::STATUS_PUBLISHED;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{portfolio_portfolio}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['title, header_string, text, seo_title, seo_description, seo_keywords, border_color, theme', 'filter', 'filter' => 'trim'],
            ['title, header_string, seo_title, seo_description, seo_keywords, theme', 'filter', 'filter' => [new CHtmlPurifier(), 'purify']],
            ['type, type_string, date, client_id, title, slug', 'required'],
            ['type, client_id, theme_id, style_id, status, border_width', 'numerical', 'integerOnly' => true],
            ['header_tag_id', 'default', 'value' => null],
            ['header_tag_id, border_opacity', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            ['is_loved', 'boolean'],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            ['border_width', 'in', 'range' => range(0, 20)],
            ['border_opacity', 'in', 'range' => range(0, 100)],
            ['border_color', 'length', 'max' => 25],
            ['border_color', 'default', 'value' => null],
            ['type', 'in', 'range' => array_keys($this->getTypeList())],
            ['preview_size', 'in', 'range' => array_keys($this->getPreviewSizeList())],
            ['client_id', 'exist', 'className' => 'Client', 'attributeName' => 'id'],
            ['theme_id', 'exist', 'className' => 'PortfolioTheme', 'attributeName' => 'id'],
            ['style_id', 'exist', 'className' => 'PortfolioStyle', 'attributeName' => 'id'],
            ['title, header_string, type_string, theme, seo_title, seo_keywords, seo_description', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PortfolioModule.portfolio', 'Bad characters in {attribute} field'),
            ],
            ['text', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            [
                'id, create_time, update_time, type, type_string, theme, date, client_id, title, header_string, header_tag_id, image, pattern, preview_video, preview_size, theme_id, style_id, is_loved, status, slug, text, seo_title, seo_keywords, seo_description, border_width, border_color, border_opacity',
                'safe',
                'on' => 'search',
            ],
        ];
    }

    /**
     * @return array
     */
    public function scopes()
    {
        return [
            'published' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params' => [':status' => self::STATUS_PUBLISHED],
            ],
        ];
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function type($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.type = :type',
            'params' => [':type' => $id],
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('portfolio');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/portfolio/portfolio/view',
                'params' => [
                    'slug' => function (Portfolio $data) {
                        return $data->slug;
                    },
                ],
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
//                'defaultImage' => $module->defaultImage,
            ],
            'patternUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'pattern',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => 'delete-file-2',
//                'defaultImage' => $module->defaultImage,
            ],
            'previewVideoUpload' => [
                'class' => 'yupe\components\behaviors\FileUploadBehavior',
                'attributeName' => 'preview_video',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'deleteFileKey' => 'delete-preview-video',
            ],
            'tagsBehavior' => [
                'class' => 'vendor.yiiext.taggable-behavior.EARTaggableBehavior',
                'tagTable' => '{{portfolio_tag}}',
                'tagBindingTable' => '{{portfolio_portfolio_to_tag}}',
                'tagModel' => 'PortfolioTag',
                'modelTableFk' => 'portfolio_id',
                'tagBindingTableTagId' => 'tag_id',
                'cacheID' => 'cache',
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'items' => [self::HAS_MANY, 'PortfolioItem', 'portfolio_id'],
            'style' => [self::BELONGS_TO, 'PortfolioStyle', 'style_id'],
            'client' => [self::BELONGS_TO, 'Client', 'client_id'],
//            'theme' => [self::BELONGS_TO, 'PortfolioTheme', 'theme_id'],
            'headerTag' => [self::BELONGS_TO, 'PortfolioTag', 'header_tag_id'],
            'tags' => [self::HAS_MANY, 'PortfolioToTag', 'portfolio_id', 'together' => true,],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'type' => 'Тип',
            'type_string' => 'Тип (отображаемый в списке)',
            'date' => 'Дата',
            'client_id' => 'Клиент',
            'title' => 'Заголовок',
            'header_string' => 'Заголовок (для типа баннера)',
            'header_tag_id' => 'Тег для заголовка',
            'slug' => 'URL',
            'image' => 'Изображение',
            'pattern' => 'Паттерн',
            'preview_video' => 'Видео-обложка',
            'preview_size' => 'Размер превью',
            'theme_id' => 'Сфера деятельности',
            'style_id' => 'Стиль',
            'is_loved' => 'Любимый проект',
            'status' => 'Статус',
            'text' => 'Текст',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
            'border_width' => 'Ширина рамки',
            'border_color' => 'Цвет рамки',
            'border_opacity' => 'Прозрачность рамки',
            'theme' => 'Сфера деятельности',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.type_string', $this->type_string, true);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.client_id', $this->client_id);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.header_string', $this->header_string, true);
        $criteria->compare($this->tableAlias . '.header_tag_id', $this->header_tag_id);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.pattern', $this->pattern, true);
        $criteria->compare($this->tableAlias . '.preview_video', $this->preview_video, true);
        $criteria->compare($this->tableAlias . '.preview_size', $this->preview_size, true);
        $criteria->compare($this->tableAlias . '.theme_id', $this->theme_id);
        $criteria->compare($this->tableAlias . '.style_id', $this->style_id);
        $criteria->compare($this->tableAlias . '.is_loved', $this->is_loved);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);
        $criteria->compare($this->tableAlias . '.border_width', $this->border_width, false);
        $criteria->compare($this->tableAlias . '.border_color', $this->border_color, true);
        $criteria->compare($this->tableAlias . '.border_opacity', $this->border_opacity, false);
        $criteria->compare($this->tableAlias . '.theme', $this->theme, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date DESC'],
        ]);
    }

    /**
     * @param $id
     * @return CActiveDataProvider
     */
    public function searchNotFor($id)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.type', $this->type);
        $criteria->compare($this->tableAlias . '.type_string', $this->type_string, true);
        $criteria->compare($this->tableAlias . '.date', $this->date, true);
        $criteria->compare($this->tableAlias . '.client_id', $this->client_id);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.header_string', $this->header_string, true);
        $criteria->compare($this->tableAlias . '.header_tag_id', $this->header_tag_id);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.pattern', $this->pattern, true);
        $criteria->compare($this->tableAlias . '.preview_video', $this->preview_video, true);
        $criteria->compare($this->tableAlias . '.preview_size', $this->preview_size, true);
        $criteria->compare($this->tableAlias . '.theme_id', $this->theme_id);
        $criteria->compare($this->tableAlias . '.style_id', $this->style_id);
        $criteria->compare($this->tableAlias . '.is_loved', $this->is_loved);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.seo_title', $this->seo_title, true);
        $criteria->compare($this->tableAlias . '.seo_keywords', $this->seo_keywords, true);
        $criteria->compare($this->tableAlias . '.seo_description', $this->seo_description, true);
        $criteria->compare($this->tableAlias . '.border_width', $this->border_width, false);
        $criteria->compare($this->tableAlias . '.border_color', $this->border_color, true);
        $criteria->compare($this->tableAlias . '.border_opacity', $this->border_opacity, false);
        $criteria->compare($this->tableAlias . '.theme', $this->theme, true);

        $criteria->addNotInCondition($this->tableAlias . '.id', [$id]);

        $criteria->limit = 8 * 3;

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => ['defaultOrder' => $this->tableAlias . '.date DESC'],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Portfolio the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_DRAFT => Yii::t('PortfolioModule.portfolio', 'Draft'),
            self::STATUS_MODERATION => Yii::t('PortfolioModule.portfolio', 'On moderation'),
            self::STATUS_PUBLISHED => Yii::t('PortfolioModule.portfolio', 'Published'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PortfolioModule.portfolio', '*unknown*');
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_HTML5 => Yii::t('PortfolioModule.portfolio', 'HTML5'),
            self::TYPE_GIF => Yii::t('PortfolioModule.portfolio', 'GIF'),
            self::TYPE_JPG => Yii::t('PortfolioModule.portfolio', 'JPG'),
            self::TYPE_VIDEO => Yii::t('PortfolioModule.portfolio', 'Видео'),
            self::TYPE_STORY => Yii::t('PortfolioModule.portfolio', 'Сторис'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        $data = $this->getTypeList();

        return isset($data[$this->type]) ? $data[$this->type] : Yii::t('PortfolioModule.portfolio', '*unknown*');
    }

    public function getTypeStringList()
    {
        return [
            self::TYPE_HTML5 => Yii::t('PortfolioModule.portfolio', 'html5'),
            self::TYPE_GIF => Yii::t('PortfolioModule.portfolio', 'gif'),
            self::TYPE_JPG => Yii::t('PortfolioModule.portfolio', 'jpg'),
            self::TYPE_VIDEO => Yii::t('PortfolioModule.portfolio', 'video'),
            self::TYPE_STORY => Yii::t('PortfolioModule.portfolio', 'story'),
        ];
    }

    public function getTypeString()
    {
        $data = $this->getTypeStringList();

        return isset($data[$this->type]) ? $data[$this->type] : null;
    }

    public function getPreviewSizeList()
    {
        return [
            '1x1' => Yii::t('PortfolioModule.portfolio', '1x1 (204x204)'),
            '1x2' => Yii::t('PortfolioModule.portfolio', '1x2 (204x428)'),
            '1x3' => Yii::t('PortfolioModule.portfolio', '1x3 (204x652)'),
            '2x1' => Yii::t('PortfolioModule.portfolio', '2x1 (428x204)'),
            '2x2' => Yii::t('PortfolioModule.portfolio', '2x2 (428x428)'),
            '2x3' => Yii::t('PortfolioModule.portfolio', '2x3 (428x652)'),
        ];
    }

    public function getPreviewSize()
    {
        $data = $this->getPreviewSizeList();

        return isset($data[$this->preview_size]) ? $data[$this->preview_size] : null;
    }

    public function getPreviewSizeCss()
    {
        list($width, $height) = explode('x', $this->preview_size);

        return ' b-portfolio__item_width_' . $width . ' b-portfolio__item_height_' . $height;
    }

    public function getPreviewImageUrl()
    {
        list($width, $height) = explode('x', $this->preview_size);

        $width = $width * 204 + ($width - 1) * 20;
        $height = $height * 204 + ($height - 1) * 20;

        return $this->imageUpload->getImageUrl($width, $height);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $slugParts = [
                $this->getTypeString(),
                'banner-dlya',
                $this->client->slug,
                yupe\helpers\YText::translit($this->title),
            ];

            $this->slug = implode('-', $slugParts);
        }

        return parent::beforeValidate();
    }

    /**
     * @return array
     */
    public function getYesNoList()
    {
        return [
            Yii::t('PortfolioModule.portfolio', 'No'),
            Yii::t('PortfolioModule.portfolio', 'Yes'),
        ];
    }

    public function getLanguages()
    {
        $languages = [];

        foreach ($this->items as $item) {
            $languages[$item->language] = $item->getLanguage();
        }

        ksort($languages, SORT_NUMERIC);

        $languagesAssoc = [];

        foreach ($languages as $key => $value) {
            $languagesAssoc[PortfolioItem::model()->getLanguageStringList()[$key]] = $value;
        }

        return $languagesAssoc;
    }

    public function getUrlHashForSize($width, $height)
    {
        foreach ($this->items as $item) {
            if ($item->width == $width && $item->height == $height) {
                return $item->getId();
            }
        }

        return null;
    }

    public function getTitle()
    {
        return strip_tags($this->getHeaderString()) . ' для ' . $this->client->title . ' «' . $this->title . '»';
    }

    /**
     * @param $tag
     * @param array $with
     * @return mixed
     */
    public function getByTag($tag, array $with = [])
    {
        return Portfolio::model()->with($with)
            ->published()
            ->taggedWith($tag)->findAll();
    }

    /**
     * Возвращает похожие портфолио
     * @param int $limit
     *
     * @return Portfolio[]
     */
    public function getRelatedMaterials($limit = 3)
    {
        $tags = array_map(
            function ($tag) {
                return $tag->tag_id;
            },
            $this->tags
        );

        if (!empty($tags)) {
            $criteria = new CDbCriteria(
                [
                    'select' => 't.*, COUNT(t.id) as count',
                    'group' => 't.id',
                    'order' => 'count DESC',
                    'limit' => $limit,
                ]
            );

            $criteria->join = 'LEFT JOIN {{portfolio_portfolio_to_tag}} pt ON pt.portfolio_id = t.id AND pt.portfolio_id != ' . $this->id;

            $criteria->addInCondition('pt.tag_id', $tags);

            return Portfolio::model()->published()->findAll($criteria);
        }

        return [];
    }

    public function getUniqueTypeStringList()
    {
        return Yii::app()->db->createCommand('SELECT DISTINCT(`type_string`) FROM {{portfolio_portfolio}}')->queryColumn();
    }

    public function getUniqueThemeList()
    {
        return Yii::app()->db->createCommand('SELECT DISTINCT(`theme`) FROM {{portfolio_portfolio}} WHERE `theme` IS NOT NULL')->queryColumn();
    }

    public function getHeaderString()
    {
        if (!$this->header_string || !$this->header_tag_id) {
            return CHtml::link($this->getType() . '-баннер', ['/portfolio/portfolio/index', '#' => '!' . $this->getTypeString()], ['class' => 'b-page__link']);
        }

        if (!$this->headerTag) {
            return $this->header_string;
        }

        $s = $this->header_string;

        $s = str_replace('[', '<a class="b-page__link" href="' . $this->headerTag->url . '">', $s);
        $s = str_replace(']', '</a>', $s);

        return $s;
    }

    public function getBorderColorString()
    {
        if (!$this->border_color) {
            return 'transparent';
        }

        $hex = $this->border_color;

        (strlen($hex) === 4) ? list($r, $g, $b) = sscanf($hex, "#%1x%1x%1x") : list($r, $g, $b) = sscanf($hex, "#%2x%2x%2x");

        return 'rgba(' . implode(',', [$r, $g, $b]) . ',' . $this->border_opacity . '%)';
    }
}
