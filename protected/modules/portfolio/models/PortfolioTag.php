<?php

/**
 * This is the model class for table "{{portfolio_tag}}".
 *
 * The followings are the available columns in table '{{portfolio_tag}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property string $name
 * @property string $title
 * @property string $slug
 * @property integer $sort
 * @property integer $show_main_page
 * @property string $image
 * @property string $image_hover
 * @property string $color_background
 * @property string $color_background_hover
 * @property string $color_text
 * @property string $color_text_hover
 * @property integer $section_id
 *
 * The followings are the available model relations:
 * @property PortfolioToTag[] $portfolioPortfolioToTags
 * @property yupe\components\behaviors\ImageUploadBehavior $imageUpload
 * @property yupe\components\behaviors\ImageUploadBehavior $imageHoverUpload
 * @property PortfolioSection $section
 *
 * @method PortfolioTag sortBySection()
 */
class PortfolioTag extends yupe\models\YModel
{
    const MAIN_PAGE_VALUE = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{portfolio_tag}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['name, title, slug, section_id', 'required'],
            ['sort, section_id', 'numerical', 'integerOnly' => true],
            ['name, title, image, image_hover', 'length', 'max' => 255],
            ['slug', 'length', 'max' => 150],
            ['show_main_page', 'boolean'],
            ['section_id', 'exist', 'className' => 'PortfolioSection', 'attributeName' => 'id'],
            ['slug', 'unique'],
            [
                'slug',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PortfolioModule.portfolio', 'Bad characters in {attribute} field'),
            ],
            ['color_background, color_background_hover, color_text, color_text_hover', 'length', 'max' => 25],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, name, title, slug, sort, show_main_page, image, image_hover, color_background, color_background_hover, color_text, color_text_hover, section_id', 'safe', 'on' => 'search'],
        ];
    }

    public function scopes()
    {
        return [
            'sortBySection' => [
                'with' => ['section'],
                'order' => 'section.`sort` ASC, ' . $this->getTableAlias(false, false) . '.`sort` ASC',
            ],
        ];
    }

    /**
     * @param integer $id
     * @return $this
     */
    public function section($id)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->tableAlias . '.section_id = :section_id',
            'params' => [':section_id' => $id],
        ]);

        return $this;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'section' => [self::BELONGS_TO, 'PortfolioSection', 'section_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата изменения',
            'name' => 'Название',
            'title' => 'Заголовок для раздела',
            'slug' => 'URL',
            'sort' => 'Сортировка',
            'show_main_page' => 'Показать на главной',
            'image' => 'Картинка',
            'image_hover' => 'Картинка при наведении',
            'color_background' => 'Цвет фона',
            'color_background_hover' => 'Цвет фона при наведении',
            'color_text' => 'Цвет текста',
            'color_text_hover' => 'Цвет текста при наведении',
            'section_id' => 'Раздел',
        ];
    }

    public function forMain()
    {

        $this->getDbCriteria()->addCondition('show_main_page=' . self::MAIN_PAGE_VALUE);
        return $this;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.title', $this->title, true);
        $criteria->compare($this->tableAlias . '.slug', $this->slug, true);
        $criteria->compare($this->tableAlias . '.sort', $this->sort);
        $criteria->compare($this->tableAlias . '.show_main_page', $this->show_main_page);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.image_hover', $this->image_hover, true);
        $criteria->compare($this->tableAlias . '.color_background', $this->color_background, true);
        $criteria->compare($this->tableAlias . '.color_background_hover', $this->color_background_hover, true);
        $criteria->compare($this->tableAlias . '.color_text', $this->color_text, true);
        $criteria->compare($this->tableAlias . '.color_text_hover', $this->color_text_hover, true);
        $criteria->compare($this->tableAlias . '.section_id', $this->section_id, false);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
        ]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('portfolio');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'seo' => [
                'class' => 'vendor.chemezov.yii-seo.behaviors.SeoActiveRecordBehavior',
                'route' => '/portfolio/portfolio/index',
                'params' => [
                    'section' => function (PortfolioTag $data) {
                        return $data->section->slug;
                    },
                    'tag' => function (PortfolioTag $data) {
                        return $data->slug;
                    },
                ],
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => false,
            ],
            'imageHoverUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image_hover',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => false,
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'sort',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->slug) {
            $this->slug = yupe\helpers\YText::translit($this->name);
        }

        return parent::beforeValidate();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     *
     * @param string $className active record class name.
     *
     * @return PortfolioTag the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
