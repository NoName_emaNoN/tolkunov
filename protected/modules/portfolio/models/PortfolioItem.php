<?php

/**
 * This is the model class for table "{{portfolio_item}}".
 *
 * The followings are the available columns in table '{{portfolio_item}}':
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $portfolio_id
 * @property string $file
 * @property string $image
 * @property integer $width
 * @property integer $height
 * @property integer $real_width
 * @property integer $real_height
 * @property integer $language
 * @property integer $order
 *
 * The followings are the available model relations:
 * @property Portfolio $portfolio
 */
class PortfolioItem extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{portfolio_item}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['portfolio_id, width, height, real_width, real_height, language', 'required'],
            ['portfolio_id, width, height, real_width, real_height, language, order', 'numerical', 'integerOnly' => true],
            ['language', 'in', 'range' => array_keys($this->getLanguageList())],
//            ['file,  ', 'length', 'max' => 255],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            ['id, create_time, update_time, portfolio_id, file, image, width, height, real_width, real_height, language, order', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('portfolio');

        return [
            'CTimestampBehavior' => [
                'class' => 'zii.behaviors.CTimestampBehavior',
                'setUpdateOnCreate' => true,
            ],
            'fileUpload' => [
                'class' => 'yupe\components\behaviors\FileUploadBehavior',
                'attributeName' => 'file',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'deleteFileKey' => false,
            ],
            'imageUpload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath,
                'resizeOnUpload' => false,
                'deleteFileKey' => false,
//                'defaultImage' => $module->defaultImage,
            ],
            'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
                'attributeName' => 'order',
            ],
        ];
    }

    public function defaultScope()
    {
        return [
            'order' => $this->getTableAlias(false, false) . '.`order` ASC',
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'portfolio' => [self::BELONGS_TO, 'Portfolio', 'portfolio_id'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата обновления',
            'portfolio_id' => 'Портфолио',
            'file' => 'Файл',
            'image' => 'Изображение',
            'width' => 'Ширина',
            'height' => 'Высота',
            'real_width' => 'Ширина (реальная)',
            'real_height' => 'Высота (реальная)',
            'language' => 'Язык',
            'order' => 'Сортировка',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.create_time', $this->create_time, true);
        $criteria->compare($this->tableAlias . '.update_time', $this->update_time, true);
        $criteria->compare($this->tableAlias . '.portfolio_id', $this->portfolio_id);
        $criteria->compare($this->tableAlias . '.file', $this->file, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.width', $this->width);
        $criteria->compare($this->tableAlias . '.height', $this->height);
        $criteria->compare($this->tableAlias . '.real_width', $this->real_width);
        $criteria->compare($this->tableAlias . '.real_height', $this->real_height);
        $criteria->compare($this->tableAlias . '.language', $this->language);
        $criteria->compare($this->tableAlias . '.order', $this->order);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => false,
            'sort' => [
                'defaultOrder' => $this->tableAlias . '.order',
            ],
        ]);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PortfolioItem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array
     */
    public function getLanguageList()
    {
        return [
            1 => Yii::t('PortfolioModule.portfolio', 'Русский'),
            2 => Yii::t('PortfolioModule.portfolio', 'Английский'),
            3 => Yii::t('PortfolioModule.portfolio', 'Испанский'),
            4 => Yii::t('PortfolioModule.portfolio', 'Итальянский'),
            5 => Yii::t('PortfolioModule.portfolio', 'Китайский'),
            6 => Yii::t('PortfolioModule.portfolio', 'Японский'),
            7 => Yii::t('PortfolioModule.portfolio', 'Арабский'),
            8 => Yii::t('PortfolioModule.portfolio', 'Португальский'),
            9 => Yii::t('PortfolioModule.portfolio', 'Корейский'),
            10 => Yii::t('PortfolioModule.portfolio', 'Немецкий'),
            11 => Yii::t('PortfolioModule.portfolio', 'Французский'),
            12 => Yii::t('PortfolioModule.portfolio', 'Турецкий'),
            13 => Yii::t('PortfolioModule.portfolio', 'Вьетнамский'),
            14 => Yii::t('PortfolioModule.portfolio', 'Хинди'),
            15 => Yii::t('PortfolioModule.portfolio', 'Бенгальский'),
            16 => Yii::t('PortfolioModule.portfolio', 'Лахнда'),
            17 => Yii::t('PortfolioModule.portfolio', 'Яванский'),
            18 => Yii::t('PortfolioModule.portfolio', 'Бенгальский'),
            19 => Yii::t('PortfolioModule.portfolio', 'Урду'),
            20 => Yii::t('PortfolioModule.portfolio', 'Персидский'),
            21 => Yii::t('PortfolioModule.portfolio', 'Малайский'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getLanguage()
    {
        $data = $this->getLanguageList();

        return isset($data[$this->language]) ? $data[$this->language] : Yii::t('PortfolioModule.portfolio', '*unknown*');
    }

    /**
     * @return array
     */
    public function getLanguageStringList()
    {
        return [
            1 => 'ru',
            2 => 'en',
            3 => 'es',
            4 => 'it',
            5 => 'zh',
            6 => 'ja',
            7 => 'ar',
            8 => 'pt',
            9 => 'ko',
            10 => 'de',
            11 => 'fr',
            12 => 'tr',
            13 => 'vi',
            14 => 'hi',
            15 => 'bn',
            16 => 'lah',
            17 => 'jv',
            18 => 'bn',
            19 => 'ur',
            20 => 'fa',
            21 => 'ms',
        ];
    }

    /**
     * @return mixed|string
     */
    public function getLanguageString()
    {
        $data = $this->getLanguageStringList();

        return isset($data[$this->language]) ? $data[$this->language] : null;
    }

    public function getId()
    {
        return 'banner_language_' . $this->getLanguageString() . '_size_' . $this->width . '_' . $this->height . '_' . $this->id;
    }

    public function getIframeUrl()
    {
        $file = $this->fileUpload->getFilePath();
        $path = pathinfo($file, PATHINFO_DIRNAME) . DIRECTORY_SEPARATOR . pathinfo($file, PATHINFO_FILENAME);

        if (!file_exists($file)) {
            return null;
        }

        if (!file_exists($path)) {
            /* Директории назначения не существует, создадим её и распакуем содержимое архива */

            if (!mkdir($path)) {
                throw new Exception('Could not create directory "' . $path . '"');
            }

            $zip = new ZipArchive;
            if ($zip->open($file) === true) {
                $zip->extractTo($path);
                $zip->close();
            } else {
                throw new Exception('Could not extract archive "' . $file . '"');
            }
        }

        $htmlFileList = array_merge(glob($path . DIRECTORY_SEPARATOR . '*' . DIRECTORY_SEPARATOR . '*.html'), glob($path . DIRECTORY_SEPARATOR . '*.html'));
        $htmlFile = reset($htmlFileList);

        if (!$htmlFile) {
            throw new Exception('There is no HTML file in archive');
        }

//        var_dump($htmlFile, $path);

        $r = str_replace(DIRECTORY_SEPARATOR, '/', str_replace($path, '', $htmlFile));
//        var_dump($this->fileUpload->getFileUrl());

        $url = Yii::app()->uploadManager->getFileUrl(pathinfo($file, PATHINFO_FILENAME), $this->fileUpload->getUploadPath()) . $r;

//        var_dump($r, $url);

        return $url;
    }

    public function getIframe()
    {
        return CHtml::tag('span', ['class' => 'js-iframe', 'data-src' => $this->getIframeUrl(), 'data-width' => $this->real_width, 'data-height' => $this->real_height]);
//        return CHtml::tag('iframe', ['src' => $this->getIframeUrl(), 'width' => $this->width, 'height' => $this->height, 'frameborder' => 0, 'scrolling' => "no"], 'Ваш браузер не поддерживает плавающие фреймы!');
    }

    protected function beforeValidate()
    {
        if (!$this->real_width && !$this->real_height) {
            $this->real_width = $this->width;
            $this->real_height = $this->height;
        }

        if (!$this->width && !$this->height) {
            $this->width = $this->real_width;
            $this->height = $this->real_height;
        }

        return parent::beforeValidate();
    }
}
