<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Portfolio
 * @var $form yupe\widgets\ActiveForm
 * @var $this PortfolioBackendController
 **/
?>
<ul class="nav nav-tabs">
    <li class="active"><a href="#common" data-toggle="tab"><?= Yii::t("PortfolioModule.portfolio", "Общие"); ?></a></li>
    <li><a href="#cover" data-toggle="tab"><?= Yii::t("PortfolioModule.portfolio", "Обложка"); ?></a></li>
    <li><a href="#images" data-toggle="tab"><?= Yii::t("PortfolioModule.portfolio", "Ресайзы"); ?></a></li>
    <li><a href="#border" data-toggle="tab"><?= Yii::t("PortfolioModule.portfolio", "Рамка"); ?></a></li>
    <li><a href="#seo" data-toggle="tab"><?= Yii::t("PortfolioModule.portfolio", "SEO"); ?></a></li>
</ul>

<?php $form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'portfolio-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
); ?>

<div class="alert alert-info">
    <?= Yii::t('PortfolioModule.portfolio', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?= Yii::t('PortfolioModule.portfolio', 'обязательны.'); ?>
</div>

<?= $form->errorSummary($model); ?>

<div class="tab-content">
    <div class="tab-pane active" id="common">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->textFieldGroup($model, 'title', [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('title'),
                            'data-content' => $model->getAttributeDescription('title'),
                        ],
                    ],
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->typeAheadGroup($model, 'theme', [
                    'widgetOptions' => [
                        'options' => [
                            'minLength' => 0,
                        ],
                        'datasets' => [
                            'source' => $model->getUniqueThemeList(),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="clearfix">
            <?
            $tags = CHtml::listData($model->tags, 'id', 'tag_id');

            ?>
            <?= $form->select2Group($refTags, 'tag_id[]', [
                'label' => Yii::t('PortfolioModule.portfolio', 'Теги'),
                'widgetOptions' => [
                    'val' => array_values($tags),
                    'htmlOptions' => [
                        'multiple' => true,
                    ],
                    'data' => CHtml::listData(PortfolioTag::model()->sortBySection()->findAll(), 'id', 'name'),
                    'options' => [
                        'placeholder' => Yii::t('PortfolioModule.portfolio', 'Выберите теги'),
                        'width' => '100%',
                    ],
                ],
            ]); ?>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <?php echo $form->dropDownListGroup(
                    $model,
                    'type',
                    [
                        'widgetOptions' => [
                            'data' => $model->getTypeList(),
                            'htmlOptions' => [
                                'empty' => Yii::t('PortfolioModule.portfolio', '--выберите--'),
                            ],
                        ],
                    ]
                ); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->typeAheadGroup($model, 'type_string', [
                    'widgetOptions' => [
                        'options' => [
                            'minLength' => 0,
                        ],
                        'datasets' => [
                            'source' => $model->getUniqueTypeStringList(),
                        ],
                    ],
                ]); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->select2Group($model, 'client_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(Client::model()->findAll(), 'id', 'title'),
                        'options' => [
                            'placeholder' => Yii::t('PortfolioModule.portfolio', 'Выберите клиента'),
                            'width' => '100%',
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <?php echo $form->dropDownListGroup(
                    $model,
                    'status',
                    [
                        'widgetOptions' => [
                            'data' => $model->getStatusList(),
                            'htmlOptions' => [
                                'empty' => Yii::t('PortfolioModule.portfolio', '--выберите--'),
                            ],
                        ],
                    ]
                ); ?>
            </div>
            <div class="col-sm-6">
                <?php echo $form->datePickerGroup($model, 'date', [
                    'widgetOptions' => [
                        'options' => [
                            'format' => 'yyyy-mm-dd',
//                        'showToday'  => true,
                            'autoclose' => true,
//                        'minuteStep' => 1,
//                        'todayBtn'   => true,
                        ],
                        'htmlOptions' => [],
                    ],
                    'prepend' => '<i class="fa fa-calendar"></i>',
                ]); ?>
            </div>
        </div>
        <!--        <div class="row">-->
        <!--            <div class="col-sm-6">-->
        <!--                --><? //= $form->dropDownListGroup($model, 'theme_id', [
        //                    'widgetOptions' => [
        //                        'data' => CHtml::listData(PortfolioTheme::model()->findAll(), 'id', 'title'),
        //                        'htmlOptions' => [
        //                            'empty' => Yii::t('PortfolioModule.portfolio', '--выберите--'),
        //                        ],
        //                    ],
        //                ]); ?>
        <!--            </div>-->
        <!--            <div class="col-sm-6">-->
        <!--                --><? //= $form->dropDownListGroup($model, 'style_id', [
        //                    'widgetOptions' => [
        //                        'data' => CHtml::listData(PortfolioStyle::model()->findAll(), 'id', 'title'),
        //                        'htmlOptions' => [
        //                            'empty' => Yii::t('PortfolioModule.portfolio', '--выберите--'),
        //                        ],
        //                    ],
        //                ]); ?>
        <!--            </div>-->
        <!--        </div>-->
        <div class="row">
            <div class="col-sm-8">
                <?= $form->textFieldGroup($model, 'header_string', [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('header_string'),
                            'data-content' => $model->getAttributeDescription('header_string'),
                        ],
                    ],
                    'hint' => 'Пример: «[Обложка] для ВК».',
                ]); ?>
            </div>
            <div class="col-sm-4">
                <?= $form->select2Group($model, 'header_tag_id', [
                    'widgetOptions' => [
                        'data' => CHtml::listData(PortfolioTag::model()->sortBySection()->findAll(), 'id', 'name'),
                        'options' => [
                            'placeholder' => Yii::t('PortfolioModule.portfolio', 'Выберите тег'),
                            'width' => '100%',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <!--        <div class="row">-->
        <!--            <div class="col-sm-12">-->
        <!--                --><? //= $form->checkboxGroup($model, 'is_loved'); ?>
        <!--            </div>-->
        <!--        </div>-->
        <div class="row">
            <div class="col-sm-12 <?= $model->hasErrors('text') ? 'has-error' : ''; ?>">
                <?= $form->labelEx($model, 'text'); ?>
                <?php $this->widget(
                    $this->module->getVisualEditor(),
                    [
                        'model' => $model,
                        'attribute' => 'text',
                    ]
                ); ?>
                <?= $form->error($model, 'text'); ?>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="cover">
        <div class='row'>
            <div class="col-sm-6">
                <?php echo $form->dropDownListGroup(
                    $model,
                    'preview_size',
                    [
                        'widgetOptions' => [
                            'data' => $model->getPreviewSizeList(),
                        ],
                    ]
                ); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->image ? '' : ' hidden' ?>">
                    <?=
                    CHtml::image(
                        !$model->getIsNewRecord() && $model->image ? $model->getPreviewImageUrl() : '#',
                        $model->title,
                        [
                            'class' => 'preview-image img-thumbnail',
                            'style' => !$model->getIsNewRecord() && $model->image ? '' : 'display:none',
                        ]
                    ); ?>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->image && false): ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delete-file"> <?= Yii::t(
                                'YupeModule.yupe',
                                'Delete the file'
                            ) ?>
                        </label>
                    </div>
                <?php endif; ?>

                <?= $form->fileFieldGroup(
                    $model,
                    'image',
                    [
                        'widgetOptions' => [
                            'htmlOptions' => [
                                'onchange' => 'readURL(this);',
                            ],
                        ],
                    ]
                ); ?>
            </div>
        </div>
        <!--        <div class='row'>-->
        <!--            <div class="col-sm-7">-->
        <!--                <div class="preview-image-wrapper--><? //= !$model->getIsNewRecord() && $model->pattern ? '' : ' hidden' ?><!--">-->
        <!--                    --><? //=
        //                    CHtml::image(
        //                        !$model->getIsNewRecord() && $model->pattern ? $model->patternUpload->getImageUrl(200, 200, true) : '#',
        //                        $model->title,
        //                        [
        //                            'class' => 'img-thumbnail',
        //                            'style' => !$model->getIsNewRecord() && $model->pattern ? '' : 'display:none',
        //                        ]
        //                    ); ?>
        <!--                </div>-->
        <!---->
        <!--                --><?php //if (!$model->getIsNewRecord() && $model->pattern): ?>
        <!--                    <div class="checkbox">-->
        <!--                        <label>-->
        <!--                            <input type="checkbox" name="delete-file-2"> --><? //= Yii::t(
        //                                'YupeModule.yupe',
        //                                'Delete the file'
        //                            ) ?>
        <!--                        </label>-->
        <!--                    </div>-->
        <!--                --><?php //endif; ?>
        <!---->
        <!--                --><? //= $form->fileFieldGroup(
        //                    $model,
        //                    'pattern'
        //                ); ?>
        <!--            </div>-->
        <!--        </div>-->
        <div class='row'>
            <div class="col-sm-7">
                <div class="preview-image-wrapper<?= !$model->getIsNewRecord() && $model->preview_video ? '' : ' hidden' ?>">
                    <video autoplay loop muted src="<?= !$model->getIsNewRecord() && $model->preview_video ? $model->previewVideoUpload->getFileUrl() : '#' ?>"></video>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->preview_video): ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="delete-preview-video"> <?= Yii::t(
                                'YupeModule.yupe',
                                'Delete the file'
                            ) ?>
                        </label>
                    </div>
                <?php endif; ?>

                <?= $form->fileFieldGroup($model, 'preview_video'); ?>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="border">
        <div class='row'>
            <div class="col-sm-12">
                <?php echo $form->textFieldGroup($model, 'border_width', ['hint' => '0 для отсутствия рамки']); ?>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-12">
                <?= $form->colorPickerGroup($model, 'border_color', [
                    'widgetOptions' => [
                        'htmlOptions' => ['autocomplete' => "off"],
                    ],
                ]); ?>
            </div>
        </div>
        <div class='row'>
            <div class="col-sm-12">
                <?php echo $form->textFieldGroup($model, 'border_opacity', ['hint' => 'от 0 до 100, где 0 - рамка прозрачна, 100 - нет']); ?>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="images">
        <div class="row form-group">
            <div class="col-xs-2">
                <?= Yii::t("PortfolioModule.portfolio", "Ресайзы"); ?>
            </div>
            <div class="col-xs-2">
                <button id="button-add-image" type="button" class="btn btn-default"><i class="fa fa-fw fa-plus"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="product-images">
                    <div class="image-template hidden form-group">
                        <div class="row">
                            <div class="col-xs-6 col-sm-2">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Файл/Изображение"); ?></label>
                                <input type="file" class="image-file"/>
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Ширина (в тегах)"); ?></label>
                                <input type="text" class="image-width form-control"/>
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Высота (в тегах)"); ?></label>
                                <input type="text" class="image-height form-control"/>
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Ширина (реальная)"); ?></label>
                                <input type="text" class="image-real-width form-control"/>
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Высота (реальная)"); ?></label>
                                <input type="text" class="image-real-height form-control"/>
                            </div>
                            <div class="col-xs-6 col-sm-1">
                                <label for=""><?= Yii::t("PortfolioModule.portfolio", "Язык"); ?></label>
                                <?= CHtml::dropDownList('', 1, PortfolioItem::model()->getLanguageList(), [
                                    'empty' => Yii::t('PortfolioModule.portfolio', '--choose--'),
                                    'class' => 'form-control image-language',
                                ]) ?>
                            </div>
                            <div class="col-xs-2 col-sm-1" style="padding-top: 24px">
                                <button class="button-delete-image btn btn-default" type="button"><i class="fa fa-fw fa-trash-o"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (!$model->getIsNewRecord() && $model->items): ?>
                    <?php
                    $itemModel = new PortfolioItem('search');
                    $itemModel->unsetAttributes(); // clear any default values

                    if (Yii::app()->getRequest()->getParam('PortfolioItem') !== null) {
                        $itemModel->setAttributes(Yii::app()->getRequest()->getParam('PortfolioItem'));
                    }
                    $itemModel->portfolio_id = $model->id;

                    $this->widget(
                        'bootstrap.widgets.TbExtendedGridView',
                        [
                            'id' => 'portfolio-item-grid',
                            'type' => 'striped condensed',
                            'dataProvider' => $itemModel->search(),
                            'filter' => $itemModel,
                            'sortableRows' => true,
                            'sortableAjaxSave' => true,
                            'sortableAttribute' => 'order',
                            'sortableAction' => '/portfolio/portfolioBackend/sortableItem',
                            'columns' => [
                                [
                                    'type' => 'raw',
                                    'header' => '',
                                    'filter' => false,
                                    'value' => function (PortfolioItem $data) {
                                        if ($data->portfolio->type == Portfolio::TYPE_HTML5) {
//                                            return $data->getIframe();
                                        } else {
//                                            return CHtml::image($data->getImageUrl(100, 100), '#', ['class' => 'img-responsive']);
                                        }
                                    },
                                    'htmlOptions' => ['class' => 'col-xs-6 col-sm-2'],
                                ],
                                [
                                    'class' => 'bootstrap.widgets.TbEditableColumn',
                                    'name' => 'width',
                                    'editable' => [
                                        'url' => $this->createUrl('inlineItem'),
                                        'mode' => 'inline',
                                        'params' => [
                                            Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                                        ],
                                    ],
                                    'filter' => CHtml::activeTextField($itemModel, 'width', ['class' => 'form-control']),
                                    'htmlOptions' => ['class' => 'col-xs-5 col-sm-2'],
                                ],
                                [
                                    'class' => 'bootstrap.widgets.TbEditableColumn',
                                    'name' => 'height',
                                    'editable' => [
                                        'url' => $this->createUrl('inlineItem'),
                                        'mode' => 'inline',
                                        'params' => [
                                            Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                                        ],
                                    ],
                                    'filter' => CHtml::activeTextField($itemModel, 'height', ['class' => 'form-control']),
                                    'htmlOptions' => ['class' => 'col-xs-5 col-sm-2'],
                                ],
                                [
                                    'class' => 'bootstrap.widgets.TbEditableColumn',
                                    'name' => 'real_width',
                                    'editable' => [
                                        'url' => $this->createUrl('inlineItem'),
                                        'mode' => 'inline',
                                        'params' => [
                                            Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                                        ],
                                    ],
                                    'filter' => CHtml::activeTextField($itemModel, 'real_width', ['class' => 'form-control']),
                                    'htmlOptions' => ['class' => 'col-xs-5 col-sm-2'],
                                ],
                                [
                                    'class' => 'bootstrap.widgets.TbEditableColumn',
                                    'name' => 'real_height',
                                    'editable' => [
                                        'url' => $this->createUrl('inlineItem'),
                                        'mode' => 'inline',
                                        'params' => [
                                            Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                                        ],
                                    ],
                                    'filter' => CHtml::activeTextField($itemModel, 'real_height', ['class' => 'form-control']),
                                    'htmlOptions' => ['class' => 'col-xs-5 col-sm-2'],
                                ],
                                [
                                    'class' => 'yupe\widgets\EditableStatusColumn',
                                    'name' => 'language',
                                    'url' => $this->createUrl('inlineItem'),
                                    'source' => $itemModel->getLanguageList(),
                                    'editable' => [
                                        'emptytext' => '---',
                                    ],
                                    'htmlOptions' => ['class' => 'col-xs-6 col-sm-1'],
                                ],
                                [
                                    'class' => 'yupe\widgets\CustomButtonColumn',
                                    'template' => '{delete}',
                                    'deleteButtonUrl' => 'Yii::app()->controller->createUrl("deleteItem",array("id"=>$data->primaryKey))',
                                    'htmlOptions' => ['class' => 'col-xs-2 col-sm-1'],
                                ],
                            ],
                        ]
                    ); ?>
                <?php endif; ?>
                <br>
            </div>
        </div>
    </div>

    <div class="tab-pane" id="seo">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textFieldGroup($model, 'seo_title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textFieldGroup($model, 'seo_keywords'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->textAreaGroup($model, 'seo_description', ['widgetOptions' => ['htmlOptions' => ['rows' => 8]]]); ?>
            </div>
        </div>
    </div>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить портфолио и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить портфолио и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(function () {

        $('#button-add-image').on('click', function () {
            var newImage = $("#product-images .image-template").clone().removeClass('image-template').removeClass('hidden');
            var key = $.now();

            newImage.appendTo("#product-images");
            newImage.find(".image-file").attr('name', 'PortfolioItem[new_' + key + '][image]');
            newImage.find(".image-width").attr('name', 'PortfolioItem[new_' + key + '][width]');
            newImage.find(".image-height").attr('name', 'PortfolioItem[new_' + key + '][height]');
            newImage.find(".image-real-width").attr('name', 'PortfolioItem[new_' + key + '][real_width]');
            newImage.find(".image-real-height").attr('name', 'PortfolioItem[new_' + key + '][real_height]');
            newImage.find(".image-language").attr('name', 'PortfolioItem[new_' + key + '][language]');

            return false;
        });

        $('#product-images').on('click', '.button-delete-image', function () {
            $(this).closest('.row').remove();
        });

        $('.product-delete-image').on('click', function (event) {
            event.preventDefault();
            var blockForDelete = $(this).closest('tr');
            $.ajax({
                type: "POST",
                data: {
                    'id': $(this).data('id'),
                    '<?= Yii::app()->getRequest()->csrfTokenName;?>': '<?= Yii::app()->getRequest()->csrfToken;?>'
                },
                url: '<?= Yii::app()->createUrl('/portfolio/portfolioBackend/deleteItem');?>',
                success: function () {
                    blockForDelete.remove();
                }
            });
        });

        function activateFirstTabWithErrors() {
            var tab = $('.has-error').parents('.tab-pane').first();
            if (tab.length) {
                var id = tab.attr('id');
                $('a[href="#' + id + '"]').tab('show');
            }
        }

        activateFirstTabWithErrors();
    });
</script>
