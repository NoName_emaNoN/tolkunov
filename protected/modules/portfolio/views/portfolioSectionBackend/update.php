<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioSection
 *   @var $this PortfolioSectionBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Разделы') => ['/portfolio/portfolioSectionBackend/index'],
    $model->title => ['/portfolio/portfolioSectionBackend/view', 'id' => $model->id],
    Yii::t('PortfolioModule.portfolio', 'Редактирование'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Разделы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление разделами'), 'url' => ['/portfolio/portfolioSectionBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить раздел'), 'url' => ['/portfolio/portfolioSectionBackend/create']],
    ['label' => Yii::t('PortfolioModule.portfolio', 'Раздел') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('PortfolioModule.portfolio', 'Редактирование раздела'), 'url' => [
        '/portfolio/portfolioSectionBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('PortfolioModule.portfolio', 'Просмотреть раздел'), 'url' => [
        '/portfolio/portfolioSectionBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('PortfolioModule.portfolio', 'Удалить раздел'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/portfolio/portfolioSectionBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('PortfolioModule.portfolio', 'Вы уверены, что хотите удалить раздел?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Редактирование') . ' ' . Yii::t('PortfolioModule.portfolio', 'раздела'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>