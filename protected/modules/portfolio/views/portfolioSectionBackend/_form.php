<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model PortfolioSection
 * @var $form yupe\widgets\ActiveForm
 * @var $this PortfolioSectionBackendController
 **/
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm', [
        'id' => 'portfolio-section-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => ['class' => 'well'],
    ]
);
?>

    <div class="alert alert-info">
        <?= Yii::t('PortfolioModule.portfolio', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?= Yii::t('PortfolioModule.portfolio', 'обязательны.'); ?>
    </div>

<?= $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php echo $form->textFieldGroup($model, 'title'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo $form->slugFieldGroup(
                $model,
                'slug',
                [
                    'sourceAttribute' => 'title',
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'class' => 'popover-help',
                            'data-original-title' => $model->getAttributeLabel('slug'),
                            'data-content' => $model->getAttributeDescription('slug'),
                            'placeholder' => 'Для автоматической генерации оставьте поле пустым',
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить раздел и продолжить'),
    ]
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton', [
        'buttonType' => 'submit',
        'htmlOptions' => ['name' => 'submit-type', 'value' => 'index'],
        'label' => Yii::t('PortfolioModule.portfolio', 'Сохранить раздел и закрыть'),
    ]
); ?>

<?php $this->endWidget(); ?>