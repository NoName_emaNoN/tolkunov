<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioSection
 *   @var $this PortfolioSectionBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Разделы') => ['/portfolio/portfolioSectionBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Добавление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Разделы - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление разделами'), 'url' => ['/portfolio/portfolioSectionBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить раздел'), 'url' => ['/portfolio/portfolioSectionBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Разделы'); ?>
        <small><?=  Yii::t('PortfolioModule.portfolio', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>