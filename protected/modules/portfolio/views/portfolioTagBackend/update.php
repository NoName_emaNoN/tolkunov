<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioTag
 *   @var $this PortfolioTagBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Портфолио') => ['/portfolio/portfolioBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Теги') => ['/portfolio/portfolioTagBackend/index'],
    $model->name => '',
    Yii::t('PortfolioModule.portfolio', 'Редактирование'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Теги - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление Тегами'), 'url' => ['/portfolio/portfolioTagBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить Тег'), 'url' => ['/portfolio/portfolioTagBackend/create']],
    ['label' => Yii::t('PortfolioModule.portfolio', 'Тег') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('PortfolioModule.portfolio', 'Редактирование Тега'), 'url' => [
        '/portfolio/portfolioTagBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('PortfolioModule.portfolio', 'Удалить Тег'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/portfolio/portfolioTagBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('PortfolioModule.portfolio', 'Вы уверены, что хотите удалить Тег?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Редактирование') . ' ' . Yii::t('PortfolioModule.portfolio', 'Тега'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
