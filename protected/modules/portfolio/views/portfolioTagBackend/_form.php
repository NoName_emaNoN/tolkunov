<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model PortfolioTag
 * @var $form  yupe\widgets\ActiveForm
 * @var $this  PortfolioTagBackendController
 **/
$form = $this->beginWidget( 'yupe\widgets\ActiveForm', [
	'id'                     => 'portfolio-tag-form',
	'enableAjaxValidation'   => false,
	'enableClientValidation' => true,
	'htmlOptions'            => [ 'class' => 'well', 'enctype' => 'multipart/form-data' ],
] );
?>

<div class="alert alert-info">
	<?= Yii::t( 'PortfolioModule.portfolio', 'Поля, отмеченные' ); ?>
	<span class="required">*</span>
	<?= Yii::t( 'PortfolioModule.portfolio', 'обязательны.' ); ?>
</div>

<?= $form->errorSummary( $model ); ?>
<div class="row">
	<div class="col-sm-6">
		<?= $form->dropDownListGroup($model, 'section_id', [
			'widgetOptions' => [
				'data' => CHtml::listData(PortfolioSection::model()->findAll(), 'id', 'title'),
				'htmlOptions' => [
					'empty' => Yii::t('PortfolioModule.portfolio', '--выберите--'),
				],
			],
		]); ?>
		<?= $form->textFieldGroup( $model, 'name' ); ?>
		<?= ! $model->isNewRecord ? $form->textFieldGroup( $model, 'slug' ) : ''; ?>
		<?= $form->textFieldGroup( $model, 'title' ); ?>
		<?= $form->textFieldGroup( $model, 'sort' ); ?>
<!--		--><?//= $form->checkboxGroup( $model, 'show_main_page' ); ?>
	</div>
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-6" id="image_container">
				<?= CHtml::image( ! $model->getIsNewRecord() && $model->image ? $model->imageUpload->getImageUrl() : '#', $model->name, [
					'class' => 'preview-image',
					'style' => (! $model->getIsNewRecord() && $model->image ? 'display:block;' : 'display:none;').'max-width:100%;',
				] ); ?>
				<?= $form->fileFieldGroup( $model, 'image', [
					'widgetOptions' => [
						'htmlOptions' => [
							'onchange' => 'readURL(this,"#image_container");',
						],
					],
				] ); ?>
			</div>
			<div class="col-sm-6" id="image_hover_container">
				<?= CHtml::image( ! $model->getIsNewRecord() && $model->image_hover ? $model->imageHoverUpload->getImageUrl() : '#', $model->name, [
					'class' => 'preview-image',
					'style' => (! $model->getIsNewRecord() && $model->image_hover ? 'display:block;' : 'display:none;').'max-width:100%;',
				] ); ?>
				<?= $form->fileFieldGroup( $model, 'image_hover', [
					'widgetOptions' => [
						'htmlOptions' => [
							'onchange' => 'readURL(this,"#image_hover_container");',
						],
					],
				] ); ?>
			</div>
		</div>


		<?= $form->colorPickerGroup( $model, 'color_background', [
				'widgetOptions' => [
					'htmlOptions' => [ 'autocomplete' => "off" ]
				]
			] ); ?>
		<?= $form->colorPickerGroup( $model, 'color_background_hover', [
				'widgetOptions' => [
					'htmlOptions' => [ 'autocomplete' => "off" ]
				]
			] ); ?>
		<?= $form->colorPickerGroup( $model, 'color_text', [
				'widgetOptions' => [
					'htmlOptions' => [ 'autocomplete' => "off" ]
				]
			] ); ?>
		<?= $form->colorPickerGroup( $model, 'color_text_hover', [
				'widgetOptions' => [
					'htmlOptions' => [ 'autocomplete' => "off" ]
				]
			] ); ?>
	</div>
</div>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType' => 'submit',
	'context'    => 'primary',
	'label'      => Yii::t( 'PortfolioModule.portfolio', 'Сохранить Тег и продолжить' ),
] ); ?>

<?php $this->widget( 'bootstrap.widgets.TbButton', [
	'buttonType'  => 'submit',
	'htmlOptions' => [ 'name' => 'submit-type', 'value' => 'index' ],
	'label'       => Yii::t( 'PortfolioModule.portfolio', 'Сохранить Тег и закрыть' ),
] ); ?>

<?php $this->endWidget(); ?>
