<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioTag
 *   @var $this PortfolioTagBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Портфолио') => ['/portfolio/portfolioBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Теги') => ['/portfolio/portfolioTagBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Добавление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Теги - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление Тегами'), 'url' => ['/portfolio/portfolioTagBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить Тег'), 'url' => ['/portfolio/portfolioTagBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Теги'); ?>
        <small><?=  Yii::t('PortfolioModule.portfolio', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>
