<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioTag
 *   @var $this PortfolioTagBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Портфолио') => ['/portfolio/portfolioBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Теги') => ['/portfolio/portfolioTagBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Управление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Теги - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление Тегами'), 'url' => ['/portfolio/portfolioTagBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить Тег'), 'url' => ['/portfolio/portfolioTagBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Теги'); ?>
        <small><?=  Yii::t('PortfolioModule.portfolio', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('PortfolioModule.portfolio', 'Поиск Тегов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('portfolio-tag-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('PortfolioModule.portfolio', 'В данном разделе представлены средства управления Тегами'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'portfolio-tag-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '80px'],
            ],
//            'create_time',
//            'update_time',
            'name',
            'title',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'section_id',
                'url' => $this->createUrl('inline'),
                'source' => CHtml::listData(PortfolioSection::model()->findAll(), 'id', 'title'),
                'editable' => [
                    'emptytext' => '---',
                ],
            ],
            'slug',
            'sort',
//            'show_main_page',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]
); ?>
