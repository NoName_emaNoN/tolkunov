<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioTheme
 *   @var $this PortfolioThemeBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Сферы деятельности') => ['/portfolio/portfolioThemeBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Добавление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Сферы деятельности - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление сферами деятельности'), 'url' => ['/portfolio/portfolioThemeBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить сферу деятельности'), 'url' => ['/portfolio/portfolioThemeBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Сферы деятельности'); ?>
        <small><?=  Yii::t('PortfolioModule.portfolio', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>