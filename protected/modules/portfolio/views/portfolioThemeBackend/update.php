<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioTheme
 *   @var $this PortfolioThemeBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Сферы деятельности') => ['/portfolio/portfolioThemeBackend/index'],
    $model->title => ['/portfolio/portfolioThemeBackend/view', 'id' => $model->id],
    Yii::t('PortfolioModule.portfolio', 'Редактирование'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Сферы деятельности - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление сферами деятельности'), 'url' => ['/portfolio/portfolioThemeBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить сферу деятельности'), 'url' => ['/portfolio/portfolioThemeBackend/create']],
    ['label' => Yii::t('PortfolioModule.portfolio', 'Сфера деятельности') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('PortfolioModule.portfolio', 'Редактирование сферы деятельности'), 'url' => [
        '/portfolio/portfolioThemeBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('PortfolioModule.portfolio', 'Просмотреть сферу деятельности'), 'url' => [
        '/portfolio/portfolioThemeBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('PortfolioModule.portfolio', 'Удалить сферу деятельности'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/portfolio/portfolioThemeBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('PortfolioModule.portfolio', 'Вы уверены, что хотите удалить сферу деятельности?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Редактирование') . ' ' . Yii::t('PortfolioModule.portfolio', 'сферы деятельности'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>