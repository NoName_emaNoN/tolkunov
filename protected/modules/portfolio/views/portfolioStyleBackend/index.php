<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model PortfolioStyle
 * @var $this PortfolioStyleBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Стили') => ['/portfolio/portfolioStyleBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Управление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Стили - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление стилями'), 'url' => ['/portfolio/portfolioStyleBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить стиль'), 'url' => ['/portfolio/portfolioStyleBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?= Yii::t('PortfolioModule.portfolio', 'Стили'); ?>
        <small><?= Yii::t('PortfolioModule.portfolio', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?= Yii::t('PortfolioModule.portfolio', 'Поиск стилей'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('portfolio-style-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
    ?>
</div>

<br/>

<p> <?= Yii::t('PortfolioModule.portfolio', 'В данном разделе представлены средства управления стилями'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id' => 'portfolio-style-grid',
        'type' => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'sortableRows' => true,
        'sortableAjaxSave' => true,
        'sortableAttribute' => 'sort',
        'sortableAction' => '/portfolio/portfolioStyleBackend/sortable',
        'columns' => [
            [
                'name' => 'id',
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::link($data->id, ["update", "id" => $data->id]);
                },
                'htmlOptions' => ['width' => '100px'],
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'title_h1',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'title_h1', ['class' => 'form-control']),
            ],
            [
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'slug',
                'editable' => [
                    'url' => $this->createUrl('inline'),
                    'mode' => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken,
                    ],
                ],
                'filter' => CHtml::activeTextField($model, 'slug', ['class' => 'form-control']),
            ],
            'create_time',
            'update_time',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
