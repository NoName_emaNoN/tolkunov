<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioStyle
 *   @var $this PortfolioStyleBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Стили') => ['/portfolio/portfolioStyleBackend/index'],
    $model->title,
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Стили - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление стилями'), 'url' => ['/portfolio/portfolioStyleBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить стиль'), 'url' => ['/portfolio/portfolioStyleBackend/create']],
    ['label' => Yii::t('PortfolioModule.portfolio', 'Стиль') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('PortfolioModule.portfolio', 'Редактирование стиля'), 'url' => [
        '/portfolio/portfolioStyleBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('PortfolioModule.portfolio', 'Просмотреть стиль'), 'url' => [
        '/portfolio/portfolioStyleBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('PortfolioModule.portfolio', 'Удалить стиль'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/portfolio/portfolioStyleBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('PortfolioModule.portfolio', 'Вы уверены, что хотите удалить стиль?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Просмотр') . ' ' . Yii::t('PortfolioModule.portfolio', 'стиля'); ?>        <br/>
        <small>&laquo;<?=  $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'create_time',
        'update_time',
        'title',
        'title_h1',
        'slug',
        'sort',
        'seo_title',
        'seo_keywords',
        'seo_description',
    ],
]); ?>
