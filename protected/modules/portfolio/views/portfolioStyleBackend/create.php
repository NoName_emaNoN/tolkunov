<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model PortfolioStyle
 *   @var $this PortfolioStyleBackendController
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('PortfolioModule.portfolio', 'Стили') => ['/portfolio/portfolioStyleBackend/index'],
    Yii::t('PortfolioModule.portfolio', 'Добавление'),
];

$this->pageTitle = Yii::t('PortfolioModule.portfolio', 'Стили - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('PortfolioModule.portfolio', 'Управление стилями'), 'url' => ['/portfolio/portfolioStyleBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('PortfolioModule.portfolio', 'Добавить стиль'), 'url' => ['/portfolio/portfolioStyleBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('PortfolioModule.portfolio', 'Стили'); ?>
        <small><?=  Yii::t('PortfolioModule.portfolio', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>