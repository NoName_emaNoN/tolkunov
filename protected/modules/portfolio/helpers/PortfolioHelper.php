<?php

/**
 * Class PortfolioHelper
 */
class PortfolioHelper
{
    public static function getItemsWidth(CDataProvider $items)
    {
        $count = 0;

        foreach ($items->getData() as $item) {
            /* @var $item Portfolio */
            list($width, $height) = explode('x', $item->preview_size);

            $count += $width;
        }

        return $count;
    }
}
