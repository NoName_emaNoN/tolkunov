<?php

use yupe\components\Event;

//Yii::import('application.modules.page.models.Page');

/**
 * Class PortfolioSitemapGeneratorListener
 */
class PortfolioSitemapGeneratorListener
{
    /**
     * @param Event $event
     */
    public static function onGenerate(Event $event)
    {
        $generator = $event->getGenerator();

        $provider = new CActiveDataProvider(Portfolio::model()->published());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                $item->absoluteUrl,
//                Yii::app()->createAbsoluteUrl('/portfolio/portfolio/view', ['slug' => $item->slug]),
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }

        $provider = new CActiveDataProvider(PortfolioTheme::model());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                $item->absoluteUrl,
//                Yii::app()->createAbsoluteUrl('/portfolio/portfolio/view', ['slug' => $item->slug]),
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }

        $provider = new CActiveDataProvider(PortfolioStyle::model());

        foreach (new CDataProviderIterator($provider) as $item) {
            $generator->addItem(
                $item->absoluteUrl,
//                Yii::app()->createAbsoluteUrl('/portfolio/portfolio/view', ['slug' => $item->slug]),
                strtotime($item->update_time),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }

        $sizes = self::loadSizes();

        foreach ($sizes as $size) {
            list($sizeWidth, $sizeHeight) = explode('x', $size);

            $generator->addItem(
                Yii::app()->createAbsoluteUrl('/portfolio/portfolio/size', ['width' => $sizeWidth, 'height' => $sizeHeight]),
                time(),
                SitemapHelper::FREQUENCY_WEEKLY,
                0.5
            );
        }
    }

    protected static function loadSizes()
    {
        $command = Yii::app()->db->createCommand();

        $command
            ->select(['CONCAT_WS("x",items.width,items.height) as size_string'])
            ->from('{{portfolio_item}} as items')
            ->group('size_string')
            ->order('COUNT(*) DESC');

        return $command->queryColumn();
    }
}
