<?php

use yupe\components\WebModule;

/**
 * PortfolioModule основной класс модуля portfolio
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2016 amyLabs && Yupe! team
 * @package yupe.modules.portfolio
 * @since 0.1
 */
class PortfolioModule extends WebModule
{
    const VERSION = '1.0';

    /**
     * @var string
     */
    public $uploadPath = 'portfolio';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif,svg';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var int
     */
    public $maxSize = 5368709120;
    /**
     * @var int
     */
    public $maxFiles = 1;

    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    public function getDependencies()
    {
        return ['client'];
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array|false
     */
    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type' => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'PortfolioModule.portfolio',
                    'Directory "{dir}" is not writeable! {link}',
                    [
                        '{dir}' => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('PortfolioModule.portfolio', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'portfolio',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return isset($messages[WebModule::CHECK_ERROR]) ? $messages : true;
    }

    /**
     * @return bool
     */
    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('PortfolioModule.portfolio', 'Content');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return [
            'editor' => Yii::t('PortfolioModule.portfolio', 'Visual Editor'),
            'uploadPath' => Yii::t(
                'PortfolioModule.portfolio',
                'File uploads directory (relative to "{path}")',
                ['{path}' => Yii::getPathOfAlias('webroot') . '/' . Yii::app()->getModule("yupe")->uploadPath]
            ),
            'allowedExtensions' => Yii::t('PortfolioModule.portfolio', 'Accepted extensions (separated by comma)'),
            'minSize' => Yii::t('PortfolioModule.portfolio', 'Minimum size (in bytes)'),
            'maxSize' => Yii::t('PortfolioModule.portfolio', 'Maximum size (in bytes)'),
        ];
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return parent::getEditableParamsGroups();
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            ['label' => Yii::t('PortfolioModule.portfolio', 'Портфолио')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('PortfolioModule.portfolio', 'Список портфолио'),
                'url' => ['/portfolio/portfolioBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PortfolioModule.portfolio', 'Добавить портфолио'),
                'url' => ['/portfolio/portfolioBackend/create'],
            ],
            '',
            ['label' => Yii::t('PortfolioModule.portfolio', 'Теги')],
            [
                'icon' => 'fa fa-fw fa-tags',
                'label' => Yii::t('PortfolioModule.portfolio', 'Список тегов'),
                'url' => ['/portfolio/portfolioTagBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PortfolioModule.portfolio', 'Добавить тег'),
                'url' => ['/portfolio/portfolioTagBackend/create'],
            ],
            '',
            ['label' => Yii::t('PortfolioModule.portfolio', 'Разделы')],
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('PortfolioModule.portfolio', 'Список разделов'),
                'url' => ['/portfolio/portfolioSectionBackend/index'],
            ],
            [
                'icon' => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PortfolioModule.portfolio', 'Добавить раздел'),
                'url' => ['/portfolio/portfolioSectionBackend/create'],
            ],
//            '',
//            ['label' => Yii::t('PortfolioModule.portfolio', 'Сферы деятельности')],
//            [
//                'icon' => 'fa fa-fw fa-list-alt',
//                'label' => Yii::t('PortfolioModule.portfolio', 'Список сфер деятельности'),
//                'url' => ['/portfolio/portfolioThemeBackend/index'],
//            ],
//            [
//                'icon' => 'fa fa-fw fa-plus-square',
//                'label' => Yii::t('PortfolioModule.portfolio', 'Добавить сферу деятельности'),
//                'url' => ['/portfolio/portfolioThemeBackend/create'],
//            ],
//            '',
//            ['label' => Yii::t('PortfolioModule.portfolio', 'Стили')],
//            [
//                'icon' => 'fa fa-fw fa-list-alt',
//                'label' => Yii::t('PortfolioModule.portfolio', 'Список стилей'),
//                'url' => ['/portfolio/portfolioStyleBackend/index'],
//            ],
//            [
//                'icon' => 'fa fa-fw fa-plus-square',
//                'label' => Yii::t('PortfolioModule.portfolio', 'Добавить стиль'),
//                'url' => ['/portfolio/portfolioStyleBackend/create'],
//            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('PortfolioModule.portfolio', self::VERSION);
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('PortfolioModule.portfolio', 'Портфолио');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('PortfolioModule.portfolio', 'Описание модуля "Портфолио"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('PortfolioModule.portfolio', 'Mikhail Chemezov');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('PortfolioModule.portfolio', 'michlenanosoft@gmail.com');
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('PortfolioModule.portfolio', 'http://zexed.net');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/portfolio/portfolioBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-archive";
    }

    /**
     * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
     *
     * @return bool
     **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'portfolio.models.*',
                'portfolio.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Portfolio.PortfolioManager',
                'description' => Yii::t('PortfolioModule.portfolio', 'Управление портфолио'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Portfolio.PortfolioBackend.Create',
                        'description' => Yii::t('PortfolioModule.portfolio', 'Добавление портфолио'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Portfolio.PortfolioBackend.Delete',
                        'description' => Yii::t('PortfolioModule.portfolio', 'Удаление портфолио'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Portfolio.PortfolioBackend.Index',
                        'description' => Yii::t('PortfolioModule.portfolio', 'Список портфолио'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Portfolio.PortfolioBackend.Update',
                        'description' => Yii::t('PortfolioModule.portfolio', 'Редактирование портфолио'),
                    ],
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Portfolio.PortfolioBackend.View',
                        'description' => Yii::t('PortfolioModule.portfolio', 'Просмотр портфолио'),
                    ],
                ],
            ],
        ];
    }
}
