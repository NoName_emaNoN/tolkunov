<?php $this->beginContent('//layouts/common'); ?>

<!-- flashMessages -->
<?php //$this->widget('yupe\widgets\YFlashMessages'); ?>
<!--<!-- breadcrumbs -->
<?php //$this->widget(
//    'bootstrap.widgets.TbBreadcrumbs',
//    [
//        'links' => $this->breadcrumbs,
//    ]
//);?>


<body>
<!--[if lte IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser.
    Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</p>
<!--<![endif]-->
<div class="b-page">
    <?
    if (Yii::app()->controller->route == 'site/index') {
        $block = $this->beginWidget('application.modules.tilda.widgets.TildaContentBlockWidget', [
            'alias' => 'tilda.main.header',
            'htmlOptions' => ['class' => 'b-page__header'],
        ]); ?>
        <nav class="navbar navbar-default b-navbar">
            <div id="bs-example-navbar-collapse-1" class="b-navbar__outer">
                <div class="b-navbar__container">
                    <div class="b-navbar__left">
                        <?= CHtml::link('TOLKUNOV.COM', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'b-header__logo']); ?>
                        <?php if (Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
                        <?php endif; ?>
                    </div>
                    <div class="b-navbar__right">
                        <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-xs hidden-sm']); ?>
                        <span data-target-selector="#bs-example-navbar-collapse-1" class="b-navbar__burger"><span class="burger-icon"></span><span class="burger-icon"></span><span class="burger-icon"></span></span>
                    </div>
                </div>
                <div class="container b-header-nav">
                    <div class="row b-header-nav__row">
                        <div class="b-header-nav__column b-header-nav__column_1">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "header-menu-studio-title"]); ?></p>

                                <?php if (Yii::app()->hasModule('menu')): ?>
                                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'header-menu-studiya', 'layout' => 'header-menu-studiya']); ?>
                                <?php endif; ?>
                                <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-md hidden-lg']); ?>
                            </div>
                        </div>
                        <div class="b-header-nav__column b-header-nav__column_2">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "header-menu-contacts-title"]); ?></p>

                                <?php if (Yii::app()->hasModule('menu')): ?>
                                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'header-menu-contacts', 'layout' => 'header-menu-contacts']); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="b-header-nav__column b-header-nav__column_3">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header">Ссылки</p>

                                <div class="b-social-links b-social-links_header-nav">
                                    <div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_vk"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_dark b-social-links__link_fb"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_tw"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_dark b-social-links__link_instagram"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg"
                                                                         class="b-social-links__link b-social-links__link_dark b-social-links__link_youtube"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_dark b-social-links__link_spark"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row b-header-nav__row">
                        <div class="b-header-nav__copyright clearfix">© Студия Дмитрия Толкунова, 2010‐<?= date('Y') ?></div>
                    </div>
                </div>
            </div>

            <div class="b-navbar__heightFix"></div>
        </nav>
        <div class="b-container-wide">
            <div class="row">
                <div class="clearfix">
                    <div class="col-xs-12 col-lg-6 col-xl-7">
                        <h1 class="b-page__title b-page__title_index">Делаем баннеры <br> для медийной рекламы</h1>

                        <div class="b-summary">
                            <a href="<?= $this->createUrl('/portfolio/portfolio/index') ?>" class="b-summary__item b-summary__item_1">
                                <?php
                                $portfolioCount = Portfolio::model()->published()->count();
                                $portfolioCountString = str_repeat('0', strlen((string)$portfolioCount));
                                ?>
                                <div data-number="<?= $portfolioCount ?>" class="b-summary__number odometer"><?= $portfolioCount ?></div>
                                <div class="b-summary__text"><?= Yii::t('PortfolioModule.portfolio', 'баннер&nbsp;<br>в портфолио|баннера&nbsp;<br>в портфолио|баннеров&nbsp;<br>в портфолио', [$portfolioCount]) ?></div>
                            </a>
                            <a href="<?= $this->createUrl('/review/review/index') ?>" class="b-summary__item b-summary__item_2">
                                <?php
                                $reviewsCount = Review::model()->published()->count();
                                $reviewsCountString = str_repeat('0', strlen((string)$reviewsCount));
                                ?>
                                <div data-number="<?= $reviewsCount ?>" class="b-summary__number odometer"><?= $reviewsCount ?></div>
                                <div class="b-summary__text"><?= Yii::t('ReviewModule.review', 'отзыв&nbsp;<br>от клиентов|отзыва&nbsp;<br>от клиентов|отзывов&nbsp;<br>от клиентов', [$reviewsCount]) ?></div>
                            </a>
                        </div>
                        <div class="b-showreel b-showreel_mobile hidden-lg">
                            <?php
                            $this->renderPartial('_showreel')
                            ?>
                        </div>
                        <div class="b-portfolio-tag-outer b-portfolio-tag-outer_main hidden">
                            <a href="<?= Yii::app()->createUrl('/portfolio/portfolio/index') ?>#!html5" data-tag-background-hover="#252525" class="js-portfolio-tag b-portfolio-tag b-portfolio-tag_text b-portfolio-tag_theme-violet">HTML5</a>
                            <?
                            if (!empty($tags)) {
                                $index = 1;
                                foreach ($tags as $tag) {
                                    $tagContent = $tag->name;
                                    $tagOptions = [
                                        'class' => 'js-portfolio-tag b-portfolio-tag',
                                        'data-tag-background' => $tag->color_background,
                                        'data-tag-background-hover' => $tag->color_background_hover,
                                        'data-tag-color' => $tag->color_text,
                                        'data-tag-color-hover' => $tag->color_text_hover,
                                        'style' => (!empty($tag->color_background) ? 'background:' . $tag->color_background . ';' : '') . (!empty($tag->color_text) ? 'color:' . $tag->color_text . ';' : ''),
                                    ];
                                    if (!empty($tag->image)) {
                                        $tagImageOptions = ['class' => 'b-portfolio-tag__image'];
                                        if (!empty($tag->image_hover)) {
                                            $tagImageOptions['data-hover-src'] = $tag->imageHoverUpload->getImageUrl();
                                        }

                                        $tagContent = CHtml::image($tag->imageUpload->getImageUrl(), $tagContent, $tagImageOptions);

                                        $tagOptions['class'] .= ' b-portfolio-tag_image';
                                    } else {
                                        $tagOptions['class'] .= ' b-portfolio-tag_text';
                                    }

                                    echo CHtml::link($tagContent, ['/portfolio/portfolio/index', 'tag' => $tag->slug], $tagOptions);
                                    $index++;
                                    switch ($index) {
                                        case '3':
                                            ?>
                                            <a href="<?= Yii::app()->createUrl('/portfolio/portfolio/index') ?>#!jpg" data-tag-background-hover="#252525"
                                               class="js-portfolio-tag b-portfolio-tag b-portfolio-tag_text b-portfolio-tag_theme-green">JPG</a>
                                            <?
                                            break;
                                        case '6':
                                            ?>
                                            <a href="<?= Yii::app()->createUrl('/portfolio/portfolio/index') ?>#!gif" data-tag-background-hover="#252525"
                                               class="js-portfolio-tag b-portfolio-tag b-portfolio-tag_text b-portfolio-tag_theme-blue">GIF</a>
                                            <?
                                            break;
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-6 col-xl-5 visible-lg">
                        <div class="b-showreel b-showreel_desktop">
                            <?php
                            $this->renderPartial('_showreel')
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?
        $this->endWidget();
    } else {
        $block = $this->beginWidget('application.modules.tilda.widgets.TildaContentBlockWidget', [
            'alias' => 'tilda.layout.header',
            'htmlOptions' => ['class' => 'b-page__header'],
        ]);
        ?>
        <nav class="navbar navbar-default b-navbar">
            <div id="bs-example-navbar-collapse-1" class="b-navbar__outer">
                <div class="b-navbar__container">
                    <div class="b-navbar__left">
                        <?= CHtml::link('TOLKUNOV.COM', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'b-header__logo']); ?>
                        <?php if (Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'top-menu']); ?>
                        <?php endif; ?>
                    </div>
                    <div class="b-navbar__right">
                        <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-xs hidden-sm']); ?>
                        <span data-target-selector="#bs-example-navbar-collapse-1" class="b-navbar__burger"><span class="burger-icon"></span><span class="burger-icon"></span><span class="burger-icon"></span></span>
                    </div>
                </div>
                <div class="container b-header-nav">
                    <div class="row b-header-nav__row">
                        <div class="b-header-nav__column b-header-nav__column_1">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "header-menu-studio-title"]); ?></p>

                                <?php if (Yii::app()->hasModule('menu')): ?>
                                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'header-menu-studiya', 'layout' => 'header-menu-studiya']); ?>
                                <?php endif; ?>
                                <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-md hidden-lg']); ?>
                            </div>
                        </div>
                        <div class="b-header-nav__column b-header-nav__column_2">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "header-menu-contacts-title"]); ?></p>

                                <?php if (Yii::app()->hasModule('menu')): ?>
                                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'header-menu-contacts', 'layout' => 'header-menu-contacts']); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="b-header-nav__column b-header-nav__column_3">
                            <div class="b-header-menu">
                                <p class="b-header-menu__header">Ссылки</p>

                                <div class="b-social-links b-social-links_header-nav">
                                    <div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_vk"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_dark b-social-links__link_fb"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_tw"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_dark b-social-links__link_instagram"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg"
                                                                         class="b-social-links__link b-social-links__link_dark b-social-links__link_youtube"></a></div>
                                    <div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_dark b-social-links__link_spark"></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row b-header-nav__row">
                        <div class="b-header-nav__copyright clearfix">© Студия Дмитрия Толкунова, 2010‐<?= date('Y') ?></div>
                    </div>
                </div>
            </div>

            <div class="b-navbar__heightFix"></div>
        </nav>
        <?
        $this->endWidget();
    }
    ?>
    <?php echo $content; ?>
</div>
<?php
$block = $this->beginWidget('application.modules.tilda.widgets.TildaContentBlockWidget', [
    'alias' => 'tilda.layout.footer',
    'htmlOptions' => ['class' => 'b-footer b-footer_tilda'],
]);
?>
<div class="b-footer">
    <div class="container">
        <div class="row b-footer__row">
            <div class="b-footer__column b-footer__column_1">
                <div class="row">
                    <div class="col-xs-12 col-md-3 col-lg-12">
                        <?= CHtml::link('', Yii::app()->hasModule('homepage') ? ['/homepage/hp/index'] : ['/site/index'], ['class' => 'b-footer__logo']); ?>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-12">
                        <div class="b-social-links">
                            <div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_vk"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_fb"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_tw"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_instagram"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg" class="b-social-links__link b-social-links__link_youtube"></a></div>
                            <div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_spark"></a></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-5 col-lg-12">
                        <div class="b-footer__copyright clearfix">© Студия Толкунова Дмитрия,<br>2010‐<?= date('Y') ?></div>
                    </div>
                </div>
            </div>
            <div class="b-footer__column b-footer__column_2">
                <div class="b-footer-menu">
                    <p class="b-footer-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "menyu-v-futere-1-zagolovok"]); ?></p>

                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'footer-menu-1', 'layout' => 'footer_menu_1']); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="b-footer__column b-footer__column_3">
                <div class="b-footer-menu">
                    <p class="b-footer-menu__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "menyu-v-futere-2-zagolovok"]); ?></p>

                    <?php if (Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'footer-menu-2', 'layout' => 'footer_menu_2']); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->endWidget();
?>


<div class="b-call-widget b-call-widget__collapsed">
    <div class="b-call-widget__button"></div>
    <div class="b-call-widget__content">
        <iframe src="<?= Yii::app()->getBaseUrl(true); ?>/widget/widget.html" id="callbackFrame"></iframe>
    </div>
</div>

<script>
    /* When iframe is loaded */
    $(function () {
        $('#callbackFrame').ready(function () {
            console.log('iframe loaded successfully');

            setTimeout(function () {
                $('.b-call-widget__content').css('display', 'none');
            }, 1);
        });
    });
</script>

<script>
    var loaderHandle = function (itemsCssClass, buttonCssClass, afterAjaxUpdate, isMasonry, isPackery) {
        var $items = $(itemsCssClass);
        var $button = $(buttonCssClass);
        var loading = false;

        $button.on('click', function (e) {
            if (loading) {
                return false;
            }

            loading = true;
            var url = $(this).attr('href');
            $button.addClass('g-loader__button_state_loading');

            $.get(url, function (html) {
                $(document).trigger('loaderAjaxLoaded');

                var items = $($(html).find(itemsCssClass).html());
                // append items to grid

                if (isMasonry) {
                    var $grid = $items.masonry();
                    $grid.append(items).masonry('appended', items);
                } else {
                    if (isPackery) {
                        var $grid = $items.packery();
                        $grid.append(items).packery('appended', items);
                    } else {
                        $items.append(items);
                    }
                }

                afterAjaxUpdate && afterAjaxUpdate();

                /* Change button url */
                if ($(html).find('.g-loader__content').hasClass('hidden') || !$(html).find(buttonCssClass).length) {
                    console.log('there is no load button. hide it.');
                    $('.g-loader__content').addClass('hidden');
                } else {
                    var newUrl = $(html).find(buttonCssClass).attr('href');
                    console.log(newUrl);
                    $button.attr('href', newUrl);
                }

                loading = false;
                $button.removeClass('g-loader__button_state_loading');
            });

            e.preventDefault();

            return false;
        });
    }

    $(function () {
        if ($('.g-loader__button').length) {
            $(window).on('scroll', function () {
                var buttonTop = $('.g-loader__button').offset().top;
                var scrollTop = $(window).scrollTop();

                if (buttonTop - scrollTop < 2000) {
                    if (!$('.g-loader__content').hasClass('hidden')) {
                        $('.g-loader__button').trigger('click');
                    }
                }
            });
        }
    });
</script>

<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        ["code" => "STAT", "silent" => true]
    ); ?>
<?php endif; ?>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END); ?>
</body>
<?php $this->endContent(); ?>
