<?php $this->beginContent('//layouts/common'); ?>

<body class="t-body">

<div class="b-tilda-content-outer">
    <?php echo $content; ?>
</div>

<style type="text/css">
    .b-tilda-content-outer *,
    .b-tilda-content-outer :after,
    .b-tilda-content-outer :before {
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        box-sizing: content-box;
    }

    @media screen and (max-width: 960px) {
        .t-col {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
    }

    .t-submit {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
</style>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END); ?>
</body>
<?php $this->endContent(); ?>
