<?php $this->beginContent('//layouts/common'); ?>

<body class="t-body">
		<!--[if lte IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser.
			Please <a target="_blank" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
		</p>
		<!--<![endif]-->
		<?php
		$block = $this->beginWidget( 'application.modules.tilda.widgets.TildaContentBlockWidget', [
			'alias'       => 'tilda.layout.header',
			'htmlOptions' => [ 'class' => 'b-page__header' ]
		] );
		?>
		<nav class="navbar navbar-default b-navbar">
					<div id="bs-example-navbar-collapse-1" class="b-navbar__outer">
						<div class="b-navbar__container">
							<div class="b-navbar__left">
								<?= CHtml::link( 'TOLKUNOV.COM', Yii::app()->hasModule( 'homepage' ) ? [ '/homepage/hp/index' ] : [ '/site/index' ], [ 'class' => 'b-header__logo' ] ); ?>
								<?php if ( Yii::app()->hasModule( 'menu' ) ): ?>
									<?php $this->widget( 'application.modules.menu.widgets.MenuWidget', [ 'name' => 'top-menu' ] ); ?>
								<?php endif; ?>
							</div>
							<div class="b-navbar__right">
								<?= CHtml::link( 'Заказать баннер', [ '/order/order/create' ], [ 'class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-xs hidden-sm' ] ); ?>
								<span data-target-selector="#bs-example-navbar-collapse-1" class="b-navbar__burger"><span class="burger-icon"></span><span class="burger-icon"></span><span class="burger-icon"></span></span>
							</div>
						</div>
						<div class="container b-header-nav">
							<div class="row b-header-nav__row">
								<div class="b-header-nav__column b-header-nav__column_1">
									<div class="b-header-menu">
										<p class="b-header-menu__header"><?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", [ "code" => "header-menu-studio-title" ] ); ?></p>

										<?php if ( Yii::app()->hasModule( 'menu' ) ): ?>
											<?php $this->widget( 'application.modules.menu.widgets.MenuWidget', [ 'name' => 'header-menu-studiya', 'layout' => 'header-menu-studiya' ] ); ?>
										<?php endif; ?>
										<?= CHtml::link( 'Заказать баннер', [ '/order/order/create' ], [ 'class' => 'btn btn-success b-navbar__btn b-navbar__btn_order hidden-md hidden-lg' ] ); ?>
									</div>
								</div>
								<div class="b-header-nav__column b-header-nav__column_2">
									<div class="b-header-menu">
										<p class="b-header-menu__header"><?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", [ "code" => "header-menu-contacts-title" ] ); ?></p>

										<?php if ( Yii::app()->hasModule( 'menu' ) ): ?>
											<?php $this->widget( 'application.modules.menu.widgets.MenuWidget', [ 'name' => 'header-menu-contacts', 'layout' => 'header-menu-contacts' ] ); ?>
										<?php endif; ?>
									</div>
								</div>
								<div class="b-header-nav__column b-header-nav__column_3">
									<div class="b-header-menu">
										<p class="b-header-menu__header">Ссылки</p>
										<div class="b-social-links b-social-links_header-nav">
											<div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_vk"></a></div>
											<div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_dark b-social-links__link_fb"></a></div>
											<div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_dark b-social-links__link_tw"></a></div>
											<div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_dark b-social-links__link_instagram"></a></div>
											<div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg" class="b-social-links__link b-social-links__link_dark b-social-links__link_youtube"></a></div>
											<div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_dark b-social-links__link_spark"></a></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row b-header-nav__row">
								<div class="b-header-nav__copyright clearfix">© Студия Дмитрия Толкунова, 2010‐<?= date( 'Y' ) ?></div>
							</div>
						</div>
					</div>

					<div class="b-navbar__heightFix"></div>
				</nav>
		<?
		$this->endWidget();
		?>
		<div class="b-tilda-content-outer">
			<?php echo $content; ?>
		</div>

		<?php
		$block = $this->beginWidget( 'application.modules.tilda.widgets.TildaContentBlockWidget', [
			'alias'       => 'tilda.layout.footer',
			'htmlOptions' => [ 'class' => 'b-footer b-footer_tilda' ]
		] );
		?>
		<div class="b-footer">
			<div class="container">
				<div class="row b-footer__row">
					<div class="b-footer__column b-footer__column_1">
						<div class="row">
							<div class="col-xs-12 col-md-3 col-lg-12">
								<?= CHtml::link( '', Yii::app()->hasModule( 'homepage' ) ? [ '/homepage/hp/index' ] : [ '/site/index' ], [ 'class' => 'b-footer__logo' ] ); ?>
							</div>
							<div class="col-xs-12 col-md-4 col-lg-12">
								<div class="b-social-links">
									<div class="b-social-links__item"><a target="_blank" href="https://vk.com/tolkunov_com" class="b-social-links__link b-social-links__link_vk"></a></div>
									<div class="b-social-links__item"><a target="_blank" href="https://www.facebook.com/tolkunov/" class="b-social-links__link b-social-links__link_fb"></a></div>
									<div class="b-social-links__item"><a target="_blank" href="https://twitter.com/tolkunov_com" class="b-social-links__link b-social-links__link_tw"></a></div>
									<div class="b-social-links__item"><a target="_blank" href="https://www.instagram.com/tolkunov_com/" class="b-social-links__link b-social-links__link_instagram"></a></div>
									<div class="b-social-links__item"><a target="_blank" href="https://www.youtube.com/channel/UCwm8I4uQjaq5ycfSJZTuxrg" class="b-social-links__link b-social-links__link_youtube"></a></div>
									<div class="b-social-links__item"><a target="_blank" href="https://spark.ru/startup/tolkunov-com" class="b-social-links__link b-social-links__link_spark"></a></div>
								</div>
							</div>
							<div class="col-xs-12 col-md-5 col-lg-12">
								<div class="b-footer__copyright clearfix">© Студия Толкунова Дмитрия,<br>2010‐<?= date( 'Y' ) ?></div>
							</div>
						</div>
					</div>
					<div class="b-footer__column b-footer__column_2">
						<div class="b-footer-menu">
							<p class="b-footer-menu__header"><?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", [ "code" => "menyu-v-futere-1-zagolovok" ] ); ?></p>

							<?php if ( Yii::app()->hasModule( 'menu' ) ): ?>
								<?php $this->widget( 'application.modules.menu.widgets.MenuWidget', [ 'name' => 'footer-menu-1', 'layout' => 'footer_menu_1' ] ); ?>
							<?php endif; ?>
						</div>
					</div>
					<div class="b-footer__column b-footer__column_3">
						<div class="b-footer-menu">
							<p class="b-footer-menu__header"><?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", [ "code" => "menyu-v-futere-2-zagolovok" ] ); ?></p>

							<?php if ( Yii::app()->hasModule( 'menu' ) ): ?>
								<?php $this->widget( 'application.modules.menu.widgets.MenuWidget', [ 'name' => 'footer-menu-2', 'layout' => 'footer_menu_2' ] ); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?
		$this->endWidget();
		?>
		<div class="b-call-widget b-call-widget__collapsed">
			<div class="b-call-widget__button"></div>
			<div class="b-call-widget__content">
				<iframe src="<?= Yii::app()->getBaseUrl(true); ?>/widget/widget.html" id="callbackFrame"></iframe>
			</div>
		</div>

		<script>
			/* When iframe is loaded */
			$(function () {
				$('#callbackFrame').ready(function () {
					console.log('iframe loaded successfully');

					setTimeout(function () {
						$('.b-call-widget__content').css('display', 'none');
					}, 1);
				});
			});
		</script>


		<?php if ( Yii::app()->hasModule( 'contentblock' ) ): ?>
			<?php $this->widget( "application.modules.contentblock.widgets.ContentBlockWidget", [ "code" => "STAT", "silent" => true ] ); ?>
		<?php endif; ?>
		<style type="text/css">
			.b-tilda-content-outer *,
			.b-tilda-content-outer :after,
			.b-tilda-content-outer :before
			{
				-webkit-box-sizing: content-box;
				-moz-box-sizing: content-box;
				box-sizing: content-box;
			}
			@media screen and (max-width: 960px){
				.t-col
				{
					-webkit-box-sizing : border-box;
					-moz-box-sizing    : border-box;
					box-sizing         : border-box;
				}
			}
			.t-submit
			{
				-webkit-box-sizing : border-box;
				-moz-box-sizing    : border-box;
				box-sizing         : border-box;
			}
		</style>
		<?php \yupe\components\TemplateEvent::fire( DefautThemeEvents::BODY_END ); ?>
	</body>
<?php $this->endContent(); ?>
