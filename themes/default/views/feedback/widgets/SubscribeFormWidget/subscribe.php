<?php
/**
 * Created by PhpStorm.
 * User: Michael
 * Date: 29.06.2016
 * Time: 7:34
 *
 * @var $this ContactController
 * @var $model SubscribeForm
 * @var $form TbActiveForm
 */
?>

<?php if (Yii::app()->user->hasFlash(\yupe\widgets\YFlashMessages::SUCCESS_MESSAGE)): ?>
    <div class="b-subscribe-done">
        <p class="b-subscribe-done__title">Почти готово!</p>

        <div class="b-subscribe-done__text">Чтобы письма не пападали в спам - подтвердите подписку.<br>Для этого перейдите по ссылке в письме, которое мы отправили на ваш email.</div>
    </div>
<?php else: ?>
    <div class="b-subscribe b-subscribe_theme_alt" id="subscribe_block">
        <p class="b-subscribe__title">Получайте новые акции прямо на почту</p>

        <p class="b-subscribe__lead">Никакого спама или рекламы. Только акции.</p>

        <?php $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            [
                'id' => 'subscribe-form',
                'htmlOptions' => [
                    'class' => 'form-inline b-subscribe__form',
//                    'enctype' => 'multipart/form-data',
                ],
            ]
        ); ?>

        <?php echo $form->emailFieldGroup($model, 'email', ['labelOptions' => ['class' => 'sr-only'], 'widgetOptions' => ['htmlOptions' => ['placeholder' => 'Введите свой email', 'class' => 'b-subscribe__input']]]); ?>

        <?php echo CHtml::ajaxSubmitButton(
            'Подписаться',
            ['/feedback/contact/subscribe'],
            [
                'beforeSend' => 'js:function(){ $("#submit_button_' . $form->id . '").button("loading"); }',
                'replace' => '#subscribe_block',
            ],
            [
                'id' => 'submit_button_' . $form->id,
                'class' => 'btn btn-lg btn-success b-subscribe__button',
                'data-loading-text' => 'Подписаться',//                            'onclick' => "yaCounter38387270.reachGoal('prosmotr_doma'); return true;",
            ]
        ); ?>
        <?php $this->widget('\yupe\widgets\YFlashMessages', ['options' => ['htmlOptions' => ['style' => 'margin-bottom:15px;color:#fff;position:relative;top:10px;'], 'closeText' => false]]); ?>
        <?php $this->endWidget(); ?>
    </div>
<?php endif; ?>