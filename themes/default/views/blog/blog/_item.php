<?php
/**
 * @var $data Blog
 */
?>
<a href="<?= $data->url ?>" class="b-blog__item">
    <p class="b-blog__title"><?= CHtml::encode($data->name) ?></p>

    <div class="b-blog__content">
        <?= CHtml::image(
            $data->getImageUrl(288, 160),
            CHtml::encode($data->name),
            ['class' => 'img-responsive b-blog__image', 'data-rjs'=>'2']
        ); ?>
    </div>
    <div class="b-blog__text">
        <p>Начнем с того, что Flash технология переживает кризис уже не первый год. Наверное с 2000х годов время от времени поднимаются вопросы о том, какой Flash не актуальный.</p>
    </div>
    <div class="row text-muted">
        <div class="col-xs-6"><span>16 марта 2015</span></div>
        <div class="col-xs-6 text-right"><span>16 марта 2015</span></div>
    </div>
</a>

<div class="row">
    <div class="col-sm-2">
        <?= CHtml::image(
            $data->getImageUrl(64, 64),
            CHtml::encode($data->name),
            ['width' => 64, 'height' => 64, 'class' => 'thumbnail', 'data-rjs'=>'2']
        ); ?>
    </div>
    <div class="col-sm-6 blog-info">

        <h2><?= CHtml::link(
                CHtml::encode($data->name),
                ['/blog/blog/view/', 'slug' => CHtml::encode($data->slug)]
            ); ?></h2>
        <span><?= CHtml::image(
                $data->createUser->getAvatar(24),
                CHtml::encode($data->createUser->nick_name)
            ); ?> <?= CHtml::link(
                CHtml::encode($data->createUser->nick_name),
                ['/user/people/userInfo', 'username' => CHtml::encode($data->createUser->nick_name)]
            ); ?> </span>
        <span> <i class="glyphicon glyphicon-calendar"></i> <?= Yii::app()->getDateFormatter()->formatDateTime(
                $data->create_time,
                "long",
                false
            ); ?> </span>
        <span> <i class="glyphicon glyphicon-pencil"></i> <?= CHtml::link(
                CHtml::encode($data->postsCount),
                ['/blog/post/blog/', 'slug' => CHtml::encode($data->slug)]
            ); ?> </span>
        <span> <i class="glyphicon glyphicon-user"></i> <?= CHtml::link(
                CHtml::encode($data->membersCount),
                ['/blog/blog/members', 'slug' => CHtml::encode($data->slug)]
            ); ?> </span>
        <span> <?= strip_tags($data->description); ?> </span>
    </div>

    <div class="col-sm-4 text-right">
        <?php $this->widget(
            'application.modules.blog.widgets.JoinBlogWidget',
            ['user' => Yii::app()->getUser(), 'blog' => $data]
        ); ?>
        <?php
        if ($data->userIn(Yii::app()->getUser()->getId())) {
            echo CHtml::link(Yii::t('BlogModule.blog', 'Add a post'), ['/blog/publisher/write', 'blog-id' => $data->id], ['class' => 'btn btn-success btn-sm']);
        }
        ?>
    </div>
</div>
<hr/>
