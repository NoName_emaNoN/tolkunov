<?php
/**
 * @var $this BlogController
 * @var $blog Blog
 */
$this->title = [CHtml::encode($blog->name), 'Справочник клиента', Yii::app()->getModule('yupe')->siteName];
//$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
//$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="container">
    <h1 class="b-page__title">Справочник клиента</h1>

    <?php $this->widget('application.modules.blog.widgets.BlogsWidget'); ?>

    <p class="b-page__lead">Пишем о баннерах, рекламе, маркетинге, бизнесе, полезных сервисах, а&nbsp;также рассказываем немного о жизни студии.</p>
</div>
<?php $this->widget(
    'zii.widgets.CListView',
    [
        'id' => 'posts-list',
        'dataProvider' => new CArrayDataProvider($blog->posts, ['pagination' => false]),
        'itemView' => '../post/_item',
        'template' => '{items}',
        'cssFile' => false,
        'itemsCssClass' => 'b-blog__list',
        'htmlOptions' => ['class' => 'b-blog'],
    ]
); ?>
<script>
    $('.b-blog__item').each(function () {
        $(this).data('hold', Math.floor(Math.random() * 450) + 50);
    });
</script>
<div class="container">
    <div class="b-subscribe">
        <p class="b-subscribe__title">Получайте новые статьи прямо на почту</p>

        <p class="b-subscribe__lead">Никакого спама или рекламы. Только статьи.</p>

        <form action="blog_done.html" class="form-inline b-subscribe__form">
            <div class="form-group">
                <input name="email" type="email" placeholder="Введите свой email" class="form-control b-subscribe__input">
            </div>
            <button type="submit" class="btn btn-lg btn-success b-subscribe__button">Подписаться</button>
        </form>
    </div>
</div>

<?php $this->widget('blog.widgets.LastPostsOfBlogWidget', ['blogId' => $blog->id, 'limit' => 10]); ?>

<br/>

<?= CHtml::link(
    Yii::t('BlogModule.blog', 'All entries for blog "{blog}"', ['{blog}' => CHtml::encode($blog->name)]),
    ['/blog/post/blog/', 'slug' => $blog->slug],
    ['class' => 'btn btn-default']
); ?>

<br/><br/>

<?php $this->widget('application.modules.blog.widgets.ShareWidget'); ?>
