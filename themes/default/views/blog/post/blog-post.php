<?php
/**
 * @var $this PostController
 * @var $target Blog
 */
$this->title = [$target->name, 'Справочник клиента', Yii::app()->getModule('yupe')->siteName];
?>

<div class="container">
    <h1 class="b-page__title">Справочник клиента</h1>

    <?php $this->widget('application.modules.blog.widgets.BlogsWidget', ['current' => $target]); ?>

    <p class="b-page__lead"><?= strip_tags($target->description) ?></p>
</div>
<?php $this->widget(
    'zii.widgets.CListView',
    [
        'id' => 'posts-list',
        'dataProvider' => $posts->search(),
        'itemView' => '_item',
        'template' => '{items}',
        'cssFile' => false,
        'itemsCssClass' => 'b-blog__list',
        'htmlOptions' => ['class' => 'b-blog'],
    ]
); ?>
<script>
    $('.b-blog__item').each(function () {
        $(this).data('hold', Math.floor(Math.random() * 450) + 50);
    });
</script>
<script type="text/javascript">
    _hcwp = window._hcwp || [];
    _hcwp.push({widget: "Bloggerstream", widget_id: "85763", selector: 'span.b-blog__comments', label: 'Комментарии: {%COUNT%}'});
    (function () {
        if ("HC_LOAD_INIT" in window)return;
        HC_LOAD_INIT = true;
        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
        var hcc = document.createElement("script");
        hcc.type = "text/javascript";
        hcc.async = true;
        hcc.src = ("https:" == document.location.protocol ? "https" : "http") + "://w.hypercomments.com/widget/hc/85763/" + lang + "/widget.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hcc, s.nextSibling);
    })();
</script>
<div class="container">
    <?php $this->widget('application.modules.feedback.widgets.SubscribeFormWidget', ['view' => 'subscribe_blog']); ?>
</div>
