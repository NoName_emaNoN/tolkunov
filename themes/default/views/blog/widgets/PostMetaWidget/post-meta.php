<div class="b-tags b-blog-item__tags">
    <?php foreach ($post->getTags() as $tag): ?>
        <?= CHtml::link(CHtml::encode($tag), ['/blog/post/tag', 'tag' => CHtml::encode($tag)], ['class' => 'b-tags__item']); ?>
    <?php endforeach; ?>
</div>
