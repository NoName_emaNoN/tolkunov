<?php
/**
 * @var $this OrderController
 * @var $id integer
 */

$this->title = ['Оформление заказа на разработку баннеров', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="container">
    <div class="b-order-done">
        <p class="b-order-done__title">Готово, ваша заявка №<?= CHtml::encode($id) ?> отправлена!</p>

        <p class="b-order-done__text">Мы внимательно изучим её, а затем свяжемся с вами.</p>
    </div>
</div>
