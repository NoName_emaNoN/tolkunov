<?php
/**
 * @var $data Client
 */
?>
<a href="<?= $data->url ?>" class="b-clients__item">
    <span class="b-clients__title"><?= $data->title ?></span>

    <div class="b-clients__content">
        <?php if ($data->image): ?>
            <?= CHtml::image($data->imageUpload->getImageUrl(150, 50, false), CHtml::encode($data->title), ['class' => 'b-clients__image', 'data-rjs' => '2']); ?>
        <?php else: ?>
            <?= CHtml::image($data->imageUpload->getImageUrl(150, 50, false), CHtml::encode($data->title), ['class' => 'b-clients__image b-clients__image_empty', 'data-rjs' => '2']); ?>
        <?php endif; ?>
    </div>
</a>
