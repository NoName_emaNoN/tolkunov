<?php
/**
 * Отображение для client/index
 *
 * @var $this ClientController
 * @var $dataProvider CActiveDataProvider
 **/

$this->title = ['Клиенты', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('ClientModule.client', 'Клиенты')];
?>

<div class="container">
    <h1 class="b-page__title">Клиенты</h1>

    <p class="b-page__lead">Самые лучшие люди и компании. Будем рады вас видеть в этом списке.</p>
</div>
<div class="b-clients">
    <div class="js-clients-fix-height">
        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'template' => '{items}{pager}',
                'cssFile' => false,
                'itemsCssClass' => 'b-clients__list',
                'htmlOptions' => ['class' => 'container-fluid'],
                'enableHistory' => false,
                'beforeAjaxUpdate' => 'js:function(){$(".js-clients-fix-height").css("height", $(".js-clients-fix-height").height());}',
                'afterAjaxUpdate' => 'js:function(){$(".js-clients-fix-height").css("height", "auto");window.retinajs();}',
                'pagerCssClass' => 'g-loader g-loader_theme_dark g-loader_clients',
                'pager' => [
                    'class' => 'application.components.LinkPager',
                    'header' => false,
                ],
            ]
        ); ?>
    </div>
</div>

<script>
    $(function () {
        var itemsCssClass = '.b-clients__list';
        var buttonCssClass = '.g-loader__button';
        var afterAjaxUpdate = function () {
            loadClients(function () {
                window.retinajs();
            });
        };

        loaderHandle(itemsCssClass, buttonCssClass, afterAjaxUpdate, false);
    });
</script>
