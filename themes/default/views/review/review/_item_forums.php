<?php
/**
 * @var $data Review
 * @var $this CListView
 */
?>
<div class="b-reviews__header">
    <span class="b-reviews__type"><?=$data->getType()?></span>
<!--    --><?//= CHtml::link($data->getType(), (get_class($this) === 'application\controllers\SiteController') ? ['/review/review/index', '#' => '!' . $data->getTypeString()] : null, ['class' => 'b-reviews__type']); ?>

    <?php if ($data->forum): ?>
        <div class="b-reviews__icon">
            <div class="b-reviews__forum-icon" style="background-image:url('<?= $data->forum->getImageUrl(25, 25) ?>');" data-rjs="2"></div>
        </div>
    <?php endif; ?>
</div>
<div class="text-center"><?= CHtml::link($data->client->title, $data->client->url, ['class' => 'b-reviews__title']); ?></div>
<div class="b-reviews__content b-reviews__content_type_text">
    <?= $data->text ?>
</div>
<div class="b-reviews__footer">
    <span class="b-reviews__date"><?= Yii::app()->dateFormatter->format('dd.MM.yyyy', $data->date); ?></span>
    <?php if ($data->link): ?>
        <?= CHtml::link('Оригинал отзыва', $data->link, ['class' => 'b-reviews__link', 'target' => '_blank', 'rel' => 'nofollow']); ?>
    <?php endif; ?>
</div>
