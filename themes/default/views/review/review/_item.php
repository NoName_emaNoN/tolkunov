<?php
/**
 * @var $data Review
 * @var $this ReviewController
 */
?>
<div data-effect="relax" data-origin="top" data-expose="true" class="b-reviews__item b-reviews__item_type_<?= $data->getTypeString() ?>">
    <?php if ($this instanceof CController): ?>
        <?php $this->renderPartial('//review/review/_item_' . $data->getTypeString(), ['data' => $data]); ?>
    <?php else: ?>
        <?php $this->controller->renderPartial('//review/review/_item_' . $data->getTypeString(), ['data' => $data]); ?>
    <?php endif; ?>
</div>
