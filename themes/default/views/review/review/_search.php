<?php
/**
 * @var $model Review
 * @var $this ReviewController
 */
?>

<div class="b-reviews-filter b-reviews__filter">
    <a href="#" data-type-id="" data-url="<?= $this->createAbsoluteUrl('/review/review/index') ?>" class="b-reviews-filter__item b-reviews-filter__item_type_all<?= $model->type == null ? ' b-reviews-filter__item_active' : '' ?>">
        <span class="b-reviews-filter__title">Все</span>
        <span class="b-reviews-filter__count"><?= Review::model()->published()->count() ?></span>
    </a>
    <?php foreach ($model->getTypeList() as $key => $value): ?>
        <a href="#!<?= $model->getTypeStringList()[$key] ?>" data-type-id="<?= $key ?>" data-url="<?= $this->createAbsoluteUrl('/review/review/index', ['Review[type]=' => $key]) ?>"
           class="b-reviews-filter__item b-reviews-filter__item_type_<?= $model->getTypeStringList()[$key] ?><?= $model->type == $key ? ' b-reviews-filter__item_active' : '' ?>">
            <span class="b-reviews-filter__title"><?= $value ?></span>
            <span class="b-reviews-filter__count"><?= Review::model()->published()->type($key)->count() ?></span>
        </a>
    <?php endforeach; ?>
</div>
