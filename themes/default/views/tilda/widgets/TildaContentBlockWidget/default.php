<?
/**
 * @var $output string
 * @var $htmlOptions array
*/
if(!empty($htmlOptions))
{
	echo CHtml::tag('div',$htmlOptions,$output);
}
else
{
	echo $output;
}
