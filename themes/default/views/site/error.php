<?php $this->title = [Yii::t('default', 'Error') . ' ' . $error['code'], Yii::app()->getModule('yupe')->siteName]; ?>
<?php Yii::app()->clientScript->scriptMap['jquery.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js'; ?>
<?php
Yii::app()->clientScript->registerPackage('jquery');
?>

<!--<h2>--><? //= Yii::t('default', 'Error') . ' ' . $error['code']; ?><!--!</h2>-->

<?php
switch ($error['code']) {
    case '404':
        $msg = Yii::t('default', 'Страница не найдена');
        break;
    default:
        $msg = $error['message'];
        break;
}
?>

<?php
/** @var $block TildaContentBlockWidget */
$block = $this->beginWidget('application.modules.tilda.widgets.TildaContentBlockWidget', [
    'alias' => 'tilda.main.error',
]);
?>

<? if ($error['code'] != 404 || $block->isEmpty()) { ?>
    <div class="container">
        <div class="b-error">
            <p class="b-error__title"><?= $error['code'] ?></p>

            <p class="b-error__text"><?= $msg ?></p>
        </div>
    </div>
<? } ?>
<? $this->endWidget() ?>
