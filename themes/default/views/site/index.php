<?php
/**
 * @var $this \application\controllers\SiteController
 * @var $tags \PortfolioTag[]
 */

$this->title = Yii::app()->getModule( 'yupe' )->siteName;
Yii::app()->clientScript->scriptMap['jquery.js'] = $this->mainAssets . '/assets/vendor/jquery/dist/jquery.min.js';
//Yii::app()->getClientScript()->registerScriptFile( $this->mainAssets . '/assets/js/emerge.js' );
?>
<?php if ( Yii::app()->hasModule( 'portfolio' ) ): ?>
	<?php $this->widget( 'application.modules.portfolio.widgets.LastPortfolioWidget', [ 'limit' => 56 ] ); ?>
<?php endif; ?>

<?
/** @var $block TildaContentBlockWidget */
$block = $this->beginWidget( 'application.modules.tilda.widgets.TildaContentBlockWidget', [
	'alias' => 'tilda.mainPage.firstBlock'
] );
?>

<?if($block->isEmpty()){?>
	<div class="b-treatment g-mt20">
		<div class="b-treatment__gradient"></div>
		<div class="container b-treatment__container">
			<div class="b-treatment__image"></div>
			<div class="b-treatment__content">
				<div class="b-treatment__column b-treatment__column_1">
					<p class="b-treatment__title">Эффектная графика<br>с анализом рекламной<br>площадки</p>

					<div class="b-treatment__text">
						<p>Чтобы выделить предложение, я и моя команда анализируем площадку и информационное окружение баннера. Разрабатываем стилистику и анимацию для преодоления «баннерной слепоты» и привлечения внимания посетителей.</p>
					</div>
				</div>
				<div class="b-treatment__column b-treatment__column_2"><b class="b-treatment__name">Толкунов Дмитрий</b>

					<p class="b-treatment__role">директор студии</p>

					<div class="b-treatment-links b-treatment__links">
						<a target="_blank" href="https://vk.com/tolkunovdb" class="b-treatment-links__item b-treatment-links__item_vk"></a>
						<a target="_blank" href="https://www.facebook.com/tolkunovdb" class="b-treatment-links__item b-treatment-links__item_fb"></a>
						<a target="_blank" href="https://twitter.com/tolkunovdb" class="b-treatment-links__item b-treatment-links__item_tw"></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="b-splash-prices">
			<h2 class="b-splash-prices__header">Интернет-баннеры всех видов и размеров</h2>

			<div class="b-splash-prices__list">
				<div class="b-splash-prices__item b-splash-prices__item_1">
					<?= CHtml::link( 'HTML5-баннеры', [ '/portfolio/portfolio/index', '#' => '!html5' ], [ 'class' => 'b-splash-prices__title' ] ); ?>

					<div class="b-splash-prices__text">Для любых сайтов, поддерживающих HTML5. В отличие от flash корректно отображается на мобильных устройствах.</div>

					<?php $sizes = $this->loadSizes( Portfolio::TYPE_HTML5 ); ?>

					<?php $this->widget( 'application.components.PortfolioMenu', [
						'encodeLabel'    => false,
						'itemCssClass'   => 'b-splash-sizes__item',
						'activeCssClass' => 'b-splash-sizes__item_more',
						'htmlOptions'    => [ 'class' => 'b-splash-prices__sizes b-splash-sizes' ],
						'items'          => CMap::mergeArray( array_map( function ( $size ) {
							list( $sizeWidth, $sizeHeight ) = explode( 'x', $size );

							return [
								'linkOptions' => [ 'class' => 'b-splash-sizes__item', ],
								'label'       => $sizeWidth . '<span class="b-splash-sizes__divider">x</span>' . $sizeHeight,
								'url'         => [ '/portfolio/portfolio/size', 'width' => $sizeWidth, 'height' => $sizeHeight, '#' => '!html5' ],
							];
						}, $sizes ), [
							[
								'linkOptions' => [ 'class' => 'b-splash-sizes__item b-splash-sizes__item_more', ],
								'label'       => '...',
								'url'         => [ '/portfolio/portfolio/sizes', '#' => '!html5' ],
								'active'      => true,
							],
						] ),
					] ); ?>

					<?= CHtml::link( 'От ' . Tariff::getMinPriceForType( Tariff::TYPE_HTML5 ) . ' руб.', [ '/offer/offer/price', '#' => 'html5' ], [ 'class' => 'b-splash-prices__price' ] ); ?>
				</div>
				<div class="b-splash-prices__item b-splash-prices__item_2">
					<?= CHtml::link( 'GIF-баннеры', [ '/portfolio/portfolio/index', '#' => '!gif' ], [ 'class' => 'b-splash-prices__title' ] ); ?>

					<div class="b-splash-prices__text">Для сайтов, блогов и форумов, где нельзя разместить флеш или html5.</div>

					<?php $sizes = $this->loadSizes( Portfolio::TYPE_GIF ); ?>

					<?php $this->widget( 'application.components.PortfolioMenu', [
						'encodeLabel'    => false,
						'itemCssClass'   => 'b-splash-sizes__item',
						'activeCssClass' => 'b-splash-sizes__item_more',
						'htmlOptions'    => [ 'class' => 'b-splash-prices__sizes b-splash-sizes' ],
						'items'          => CMap::mergeArray( array_map( function ( $size ) {
							list( $sizeWidth, $sizeHeight ) = explode( 'x', $size );

							return [
								'linkOptions' => [ 'class' => 'b-splash-sizes__item', ],
								'label'       => $sizeWidth . '<span class="b-splash-sizes__divider">x</span>' . $sizeHeight,
								'url'         => [ '/portfolio/portfolio/size', 'width' => $sizeWidth, 'height' => $sizeHeight, '#' => '!gif' ],
							];
						}, $sizes ), [
							[
								'linkOptions' => [ 'class' => 'b-splash-sizes__item b-splash-sizes__item_more', ],
								'label'       => '...',
								'url'         => [ '/portfolio/portfolio/sizes', '#' => '!gif' ],
								'active'      => true,
							],
						] ),
					] ); ?>

					<?= CHtml::link( 'От ' . Tariff::getMinPriceForType( Tariff::TYPE_GIF ) . ' руб.', [ '/offer/offer/price', '#' => 'gif' ], [ 'class' => 'b-splash-prices__price' ] ); ?>
				</div>
				<div class="b-splash-prices__item b-splash-prices__item_3">
					<?= CHtml::link( 'JPG-баннеры', [ '/portfolio/portfolio/index', '#' => '!jpg' ], [ 'class' => 'b-splash-prices__title' ] ); ?>

					<div class="b-splash-prices__text">Для рекламы в социальных сетях (Вконтакте, Facebook, Одноклассники) и сайтов компаний (например, для слайдера).</div>

					<?php $sizes = $this->loadSizes( Portfolio::TYPE_JPG ); ?>

					<?php $this->widget( 'application.components.PortfolioMenu', [
						'encodeLabel'    => false,
						'itemCssClass'   => 'b-splash-sizes__item',
						'activeCssClass' => 'b-splash-sizes__item_more',
						'htmlOptions'    => [ 'class' => 'b-splash-prices__sizes b-splash-sizes' ],
						'items'          => CMap::mergeArray( array_map( function ( $size ) {
							list( $sizeWidth, $sizeHeight ) = explode( 'x', $size );

							return [
								'linkOptions' => [ 'class' => 'b-splash-sizes__item', ],
								'label'       => $sizeWidth . '<span class="b-splash-sizes__divider">x</span>' . $sizeHeight,
								'url'         => [ '/portfolio/portfolio/size', 'width' => $sizeWidth, 'height' => $sizeHeight, '#' => '!jpg' ],
							];
						}, $sizes ), [
							[
								'linkOptions' => [ 'class' => 'b-splash-sizes__item b-splash-sizes__item_more', ],
								'label'       => '...',
								'url'         => [ '/portfolio/portfolio/sizes', '#' => '!jpg' ],
								'active'      => true,
							],
						] ),
					] ); ?>

					<?= CHtml::link( 'От ' . Tariff::getMinPriceForType( Tariff::TYPE_JPG ) . ' руб.', [ '/offer/offer/price', '#' => 'jpeg' ], [ 'class' => 'b-splash-prices__price' ] ); ?>
				</div>
			</div>
			<div class="text-center">
				<?= CHtml::link( 'Посмотреть все тарифы', [ '/offer/offer/price' ], [ 'class' => 'btn btn-success btn-lg b-splash-prices__button' ] ); ?>
			</div>
		</div>
		<div class="b-why-us">
			<h2 class="b-why-us__header">Комфортное сотрудничество</h2>

			<div class="b-why-us__list">
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/01-icon-deshevle.svg" width="154" height="155"></div>
					<p class="b-why-us__title">Дешевле агентства, лучше фрилансера</p>

					<div class="b-why-us__text">
						<p>Отвечаем репутацией за качество готового продукта, поэтому не&nbsp;передаём заказы фрилансерам.</p>
					</div>
				</div>
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/02-icon-opyt.svg" width="158" height="160"></div>
					<p class="b-why-us__title">Опыт в почти 100 тематиках</p>

					<div class="b-why-us__text">
						<p>Работали с клиентами из России, Украины, США, Канады, Польши. Делали баннеры для заводов, салонов красоты, салонов, брокеров, ресторанов, магазинов и многих других.</p>
					</div>
				</div>
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/03-icon-resultat.svg" width="159" height="154"></div>
					<p class="b-why-us__title">Доведение проекта до&nbsp;результата</p>

					<div class="b-why-us__text">
						<p>Клиент оплачивает решение задачи, а не просто красивую картинку. Если в готовом баннере что-то не устроит — бесплатно внесём обоснованные правки.</p>
					</div>
				</div>
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/04-icon-ponocsh.svg" width="156" height="157"></div>
					<p class="b-why-us__title">Помощь в размещении баннера на сайте</p>

					<div class="b-why-us__text">
						<p>Бесплатно консультируем по смежным вопросам: как разместить флеш-баннер, как вставить гиф-заглушку, где рекламироваться.</p>
					</div>
				</div>
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/05-icon-oplata.svg" width="138" height="139"></div>
					<p class="b-why-us__title">Разные способы оплаты</p>

					<div class="b-why-us__text">
						<p>Принимаем оплату на карту Сбербанка, на счёт Альфа-банка, QIWI, WebMoney, Яндекс.Деньги, расчетный счет ИП.</p>
					</div>
				</div>
				<div class="b-why-us__item">
					<div class="b-why-us__image"><img src="<?= $this->mainAssets ?>/assets/img/why-us/06-icon-srochno.svg" width="158" height="146"></div>
					<p class="b-why-us__title">Работа со срочными заказами</p>

					<div class="b-why-us__text">
						<p>Помогаем выходить из тупиковых ситуаций, когда баннер нужен «еще вчера», а исполнитель перестал выходить на связь.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?}?>
<?$this->endWidget()?>

<?php if ( Yii::app()->hasModule( 'review' ) ): ?>
	<?php $this->widget( 'application.modules.review.widgets.LastReviewWidget', [ 'limit' => 20 ] ); ?>
<?php endif; ?>


<?
$block = $this->beginWidget( 'application.modules.tilda.widgets.TildaContentBlockWidget', [
	'alias' => 'tilda.mainPage.secondBlock'
] );?>

<? if ( $block->isEmpty() ) { ?>
	<?php if ( Yii::app()->hasModule( 'blog' ) ): ?>
		<?php $this->widget( 'application.modules.blog.widgets.LastPostsWidget', [ 'limit' => 20 ] ); ?>
	<?php endif; ?>
<? } ?>
<?
$this->endWidget();
?>

<script>
	$('.b-portfolio__item, .b-blog__item, .b-reviews__item').each(function(){
		$(this).data('hold', Math.floor(Math.random() * 250) + 50);
		$(this).addClass('emerge');
	});
</script>
