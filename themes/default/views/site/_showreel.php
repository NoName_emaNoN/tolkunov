<?php

Yii::import('application.modules.contentblock.models.ContentBlock');
/**
 * @var $type string
*/
$code = 'showreel-link';
$cacheName = "ContentBlock_$code";
$block = Yii::app()->getCache()->get( $cacheName );

if ( false === $block || null === $block) {

	$block = ContentBlock::model()->findByAttributes( [ 'code' => $code ] );


	Yii::app()->getCache()->set( $cacheName, $block );
}

if ( $block && $block->status == ContentBlock::STATUS_ACTIVE) {
	echo $block->getContent();
}
