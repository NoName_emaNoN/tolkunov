<?php
/**
 * @var $params array
 * @var $layoutParams array
 */

$params['items'] = array_map(
    function ($item) {
        $item['linkOptions'] = array_merge(!empty($item['linkOptions']) ? $item['linkOptions'] : [], ['class' => 'b-navbar__navLink']);

        return $item;
    },
    $params['items']
);

$this->widget('zii.widgets.CMenu', [
    'items' => $params['items'],
    'itemCssClass' => 'b-navbar__navItem',
    'activeCssClass' => 'b-navbar__navItem_active',
    'htmlOptions' => ['class' => 'b-navbar__nav visible-md visible-lg'],
]);
