<?php
/**
 * @var $params array
 * @var $layoutParams array
 */

$params['items'] = array_map(
    function ($item) {
        $item['linkOptions'] = array_merge(!empty($item['linkOptions']) ? $item['linkOptions'] : [], ['class' => 'b-header-menu__link']);
        $item['template'] = null;

        return $item;
    },
    $params['items']
);

$this->widget('application.components.MyMenu', [
    'items' => $params['items'],
    'activateItems' => false,
    'itemCssClass' => 'col-xs-6 col-md-12',
    'itemTemplate' => '<div class="b-header-menu__item">{menu}</div>',
    'htmlOptions' => ['class' => 'row'],
]);
