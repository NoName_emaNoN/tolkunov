<?php
/**
 * @var $model        Portfolio
 * @var $baseCriteria CDbCriteria
 */
?>

<div class="b-portfolio-filter / b-portfolio__filter">
	<span class="b-portfolio-filter__head-label"><span>ФИЛЬТР ПО ТИПУ БАННЕРОВ</span></span>
	<div class="b-portfolio-filter__container">
		<div class="b-portfolio-filter__inner">
		<a href="#" data-type-id="" class="b-portfolio-filter__item b-portfolio-filter__item_type_all<?= $model->type == NULL ? ' b-portfolio-filter__item_active' : '' ?>">
			<span class="b-portfolio-filter__title">Все</span>
			<span class="b-portfolio-filter__count"><?= Portfolio::model()->published()->count( $baseCriteria ) ?></span>
		</a>
		<?php foreach ( $model->getTypeList() as $key => $value ): ?>
			<a href="#!<?= $model->getTypeStringList()[ $key ] ?>" data-type-id="<?= $key ?>"
					class="b-portfolio-filter__item b-portfolio-filter__item_type_<?= $model->getTypeStringList()[ $key ] ?><?= $model->type == $key ? ' b-portfolio-filter__item_active' : '' ?>">
				<span class="b-portfolio-filter__title"><?= $value ?></span>
				<span class="b-portfolio-filter__count"><?= Portfolio::model()->published()->type( $key )->count( $baseCriteria ) ?></span>
			</a>
		<?php endforeach; ?>
	</div>
	</div>
</div>
