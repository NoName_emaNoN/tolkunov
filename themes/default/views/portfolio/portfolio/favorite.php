<?php
/**
 * Отображение для portfolio/favorite
 *
 * @var $this PortfolioController
 * @var $dataProvider CActiveDataProvider
 * @var $baseCriteria CDbCriteria
 * @var $model Portfolio
 **/

$this->title = ['Баннеры, которыми мы особенно гордимся', 'Портфолио', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('PortfolioModule.portfolio', 'Портфолио')];

Yii::app()->clientScript->registerScript('search', "
    $(function(){
    if (location.hash && $('a[href=\"' + location.hash + '\"]').length) {
        var link = $('a[href=\"' + location.hash + '\"]');
	    $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
		link.addClass('b-portfolio-filter__item_active');

        var typeId = $(link).data('type-id');
        var href = $(link).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.replaceState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('favorite') . "' + href;
    } else {
        history.replaceState(portfolioBaseState, document.title);
    }
    });

    $(document).on('click', '.b-portfolio-filter__item', function () {
	    $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
        $(this).addClass('b-portfolio-filter__item_active');
        var typeId = $(this).data('type-id');
        var href = $(this).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.pushState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('favorite') . "' + href;
        return false;
    });

    // Listen for history state changes
    window.addEventListener('popstate', function (e) {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('update list');
            $.fn.yiiListView.update('portfolio-list', {
                data: state.modal
            });
        }
    });
");
?>

<?php $this->renderPartial('_search', ['model' => $model, 'baseCriteria' => $baseCriteria]); ?>
<?php $this->renderPartial('_menu',['title'=>'Баннеры, которыми мы особенно гордимся']); ?>

<div class="b-portfolio">
    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'id' => 'portfolio-list',
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'cssFile' => false,
            'ajaxType' => 'GET',
            'enableHistory' => false,
            'afterAjaxUpdate' => 'js:function(){window.retinajs();}',
            'itemsCssClass' => 'b-portfolio__list',
            'htmlOptions' => ['class' => 'g-mb50'],
            'pagerCssClass' => 'g-loader',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>
</div>

<script>
    portfolioBaseUrl = '<?=$this->createUrl('favorite')?>';
    portfolioBaseState = {action: 'popup', modal: 'Portfolio[type]='};

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (!state) {
            location.reload();
        }
    });
</script>
<div class="g-loader hidden">
    <div class="g-loader__content"><a href="#" class="g-loader__button"><i class="g-loader__button-icon"></i><span class="g-loader__button-text">Загрузить ещё</span></a></div>
</div>
