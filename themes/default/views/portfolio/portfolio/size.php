<?php
/**
 * Отображение для portfolio/size
 *
 * @var $this PortfolioController
 * @var $dataProvider CActiveDataProvider
 * @var $baseCriteria CDbCriteria
 * @var $model Portfolio
 * @var $width integer
 * @var $height integer
 * @var $sizes array
 **/

$this->title = ['Баннеры размером ' . $width . 'x' . $height . ' пикселей', 'Портфолио', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('PortfolioModule.portfolio', 'Портфолио')];

Yii::app()->clientScript->registerScript('search', "
    $(function(){
    if (location.hash && $('a[href=\"' + location.hash + '\"]').length) {
        var link = $('a[href=\"' + location.hash + '\"]');
	    $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
		link.addClass('b-portfolio-filter__item_active');

        var typeId = $(link).data('type-id');
        var href = $(link).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.replaceState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('/portfolio/portfolio/size', ['width' => $width, 'height' => $height]) . "' + href;
    } else {
        history.replaceState(portfolioBaseState, document.title);
    }
    });

    $(document).on('click', '.b-portfolio-filter__item', function () {
	    $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
        $(this).addClass('b-portfolio-filter__item_active');
        var typeId = $(this).data('type-id');
        var href = $(this).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        console.log('update list');
        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.pushState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('/portfolio/portfolio/size', ['width' => $width, 'height' => $height]) . "' + href;
        return false;
    });

    // Listen for history state changes
    window.addEventListener('popstate', function (e) {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('update list');
            $.fn.yiiListView.update('portfolio-list', {
                data: state.modal
            });
        }
    });
");
?>

<?php //$this->renderPartial('_search', ['model' => $model, 'baseCriteria' => $baseCriteria]); ?>
<?php $this->renderPartial('_menu', ['title' => 'Баннеры размером ' . $width . 'x' . $height . ' пикселей', 'selectedSection' => PortfolioSection::model()->findByAttributes(['slug' => 'sizes'])]); ?>

<div class="b-size-filter">
    <?php $this->widget('application.components.PortfolioMenu', [
        'encodeLabel' => false,
        'itemCssClass' => 'b-size-filter__item',
        'activeCssClass' => 'b-size-filter__item_active',
        'htmlOptions' => ['class' => 'b-size-filter__list'],
        'items' => array_map(function ($size) use ($width, $height) {
            list($sizeWidth, $sizeHeight) = explode('x', $size);

            return [
                'label' => $sizeWidth . '<span class="b-size-filter__divider">x</span>' . $sizeHeight,
                'url' => ['size', 'width' => $sizeWidth, 'height' => $sizeHeight],
                'active' => $sizeWidth == $width && $sizeHeight == $height,
                'linkOptions' => [
                    'class' => 'b-size-filter__item',
                ],
            ];
        }, $sizes),
    ]); ?>
    <a href="#" class="b-size-filter__toggle hidden"><span class="b-size-filter__toggle-text">Показать все размеры</span><span class="b-size-filter__toggle-text hidden">Свернуть</span></a>
</div>

<div class="b-portfolio">

    <h1 class="b-portfolio__header">Баннеры размером <?= $width ?>x<?= $height ?> пикселей</h1>

    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'id' => 'portfolio-list',
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'cssFile' => false,
            'ajaxType' => 'GET',
            'enableHistory' => false,
            'afterAjaxUpdate' => 'js:function(){window.retinajs();}',
            'itemsCssClass' => 'b-portfolio__list',
            'viewData' => [
                'width' => $width,
                'height' => $height,
            ],
            'htmlOptions' => ['class' => 'g-mb150'],
            'pagerCssClass' => 'g-loader g-loader_portfolio',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>
</div>

<script>
    $(function () {
        var itemsCssClass = '.b-portfolio__list';
        var buttonCssClass = '.g-loader__button';
        var afterAjaxUpdate = function () {
            loadPortfolio(function () {
                console.log('Portfolio updated');
                window.retinajs();
            });
        };

        loaderHandle(itemsCssClass, buttonCssClass, afterAjaxUpdate, false, true);
    });
</script>

<script>
    portfolioBaseUrl = '<?=$this->createUrl('/portfolio/portfolio/size', ['width' => $width, 'height' => $height]);?>';
    portfolioBaseState = {action: 'popup', modal: 'Portfolio[type]='};

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (!state) {
            location.reload();
        }
    });
</script>
