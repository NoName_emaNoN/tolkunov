<?php
/**
 * @var $data Portfolio
 * @var $width integer|null
 * @var $height integer|null
 */
?>
<a href="<?= $data->url ?><?= isset($width, $height) ? '#' . $data->getUrlHashForSize($width, $height) : '' ?>" rel="portfolio" data-effect="relax" data-origin="bottom" data-expose="true"
   class="b-portfolio__item<?= $data->getPreviewSizeCss() ?>">
    <?= CHtml::image($data->getPreviewImageUrl(), CHtml::encode($data->title), ['class' => 'b-portfolio__image', 'data-rjs' => '2']); ?>
    <?php if ($data->preview_video): ?>
        <video autoplay loop muted src="<?= $data->previewVideoUpload->getFileUrl() ?>" class="b-portfolio__video"></video>
    <?php endif; ?>

    <div class="b-portfolio__content">
        <p class="b-portfolio__title"><?= $data->client->title ?></p>

        <?php if ($data->theme): ?>
            <p class="b-portfolio__category"><?= $data->theme ?></p>
        <?php endif; ?>
    </div>
    <span class="b-portfolio__type"><?= $data->type_string ?></span>

    <!--    --><?php //if ($data->is_loved): ?>
    <!--        <span class="b-portfolio__like"></span>-->
    <!--    --><?php //endif; ?>

    <p class="b-portfolio__date"><?= Yii::app()->dateFormatter->format('d.MM.yyyy', $data->date); ?></p>
</a>
