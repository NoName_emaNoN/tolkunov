<?php
/**
 * @var $this        PortfolioController
 * @var $tags        PortfolioTag[]
 * @var $selectedTag PortfolioTag
 * @var $title       string
 * @var $selectedSection     PortfolioSection
 *
 * @var $sections PortfolioSection[]
 */

$tagsCriteria = new CDbCriteria();
$tagsCriteria->order = 'sort ASC';
$tags = !empty($selectedSection) ? PortfolioTag::model()->section($selectedSection->id)->findAll($tagsCriteria) : [];

$sections = PortfolioSection::model()->search()->getData();

$items = [
    [
        'label'       => 'Все',
        'url'         => [ '/portfolio/portfolio/index' ],
        'active'      => $this->action->id == 'index' && empty( $selectedTag ),
        'linkOptions' => [ 'class' => 'b-filter__item' ],
    ]
];

foreach ($sections as $section) {
	$items[] = [
		'label'       => $section->title,
		'url'         => $section->url,
        'active'      => isset($selectedSection) ? $selectedSection->id == $section->id : false,
//		'active'      => $this->action->id == 'favorite',
		'linkOptions' => [ 'class' => 'b-filter__item b-filter__item_with-arrow' ],
	];
}

?>

<?php $this->widget( 'application.components.PortfolioMenu', [
	'itemCssClass'                => 'b-filter__item',
	'activeCssClass'              => 'b-filter__item_active',
	'htmlOptions'                 => [ 'class' => 'b-filter' ],
	'linkLabelWrapper'            => 'span',
	'linkLabelWrapperHtmlOptions' => [ 'class' => 'b-filter__title' ],
    'items' => $items,
//	'items'                       => [
//		[
//			'label'       => 'Все',
//			'url'         => [ '/portfolio/portfolio/index' ],
//			'active'      => $this->action->id == 'index' && empty( $selectedTag ),
//			'linkOptions' => [ 'class' => 'b-filter__item' ],
//		],
//		[
//			'label'       => 'Любимые',
//			'url'         => [ '/portfolio/portfolio/favorite' ],
//			'active'      => $this->action->id == 'favorite',
//			'linkOptions' => [ 'class' => 'b-filter__item' ],
//		],
//		[
//			'label'       => 'По темам',
//			'url'         => [ '/portfolio/portfolio/themes' ],
//			'active'      => $this->action->id == 'theme',
//			'linkOptions' => [ 'class' => 'b-filter__item b-filter__item_with-arrow' ],
//		],
//		[
//			'label'       => 'По размерам',
//			'url'         => [ '/portfolio/portfolio/sizes' ],
//			'active'      => $this->action->id == 'size',
//			'linkOptions' => [ 'class' => 'b-filter__item b-filter__item_with-arrow' ],
//		],
//		[
//			'label'       => 'По стилям',
//			'url'         => [ '/portfolio/portfolio/styles' ],
//			'active'      => $this->action->id == 'style',
//			'linkOptions' => [ 'class' => 'b-filter__item b-filter__item_with-arrow' ],
//		],
//		[
//			'label'       => 'По тегам',
//			'url'         => 'javascript:;',
//			'active'      => $this->action->id == 'index' && ! empty( $selectedTag ),
//			'linkOptions' => [ 'class' => 'b-filter__item b-filter__item_with-arrow', 'id' => 'jsTagMenuToggle' ],
//		],
//	],
] );

Yii::app()->clientScript->registerScript( 'jsTagMenuToggle', /** @lang JavaScript */
	"
$(function(){
	$('#jsTagMenuToggle').click(function(){
		var isActive = $(this).hasClass('b-filter__item_active');
		
		if(isActive)
		{
			return false
		}
		
//		var toggledBlocks = $('.b-style-filter, .b-size-filter, .b-theme-filter');
		
//		toggledBlocks.fadeOut();
//		$('#jsMenuTags').fadeIn();
		$('.b-filter__item').removeClass('b-filter__item_active');
		$(this).addClass('b-filter__item_active');
		window.location = $('.js-portfolio-tag').first().attr('href');
	});
})
" );
?>
<div id="jsMenuTags" style="text-align: center;<?=empty($selectedTag)?'display: none;':''?>" class="b-portfolio-tag-outer">
	<?
	if ( ! empty( $tags ) ) {
		foreach ( $tags as $tag ) {
			$isSelected = !empty($selectedTag) && ( $tag->id === $selectedTag->id );
			$tagContent = $tag->name;

            $tag->color_background = $tag->color_background ?: '#ffffff';
            $tag->color_background_hover = $tag->color_background_hover ?: '#4c5370';
            $tag->color_text = $tag->color_text ?: '#4c5370';
            $tag->color_text_hover = $tag->color_text_hover ?: '#ffffff';

			$tagOptions = [
				'class'                     => 'js-portfolio-tag b-portfolio-tag',
				'data-tag-background'       => $tag->color_background,
				'data-tag-background-hover' => $tag->color_background_hover,
				'data-tag-color'            => $tag->color_text,
				'data-tag-color-hover'      => $tag->color_text_hover,
				'style'                     => [
					'background'=>$tag->color_background,
					'color'=>$tag->color_text,
				]
			];

			if ( $isSelected ) {
				$tagOptions['class']                     .= ' b-portfolio-tag_selected';
				$tagOptions['data-tag-background']       = $tag->color_background_hover ? $tag->color_background_hover : $tag->color_background;
				$tagOptions['data-tag-color']            = $tag->color_text_hover ? $tag->color_text_hover : $tag->color_text;

				$tagOptions['data-tag-background-hover'] = '';
				$tagOptions['data-tag-color-hover']      = '';

				if ( ! empty( $tag->color_background_hover ) ) {
					$tagOptions['style']['background'] = $tag->color_background_hover;
				}
				if(!empty( $tag->color_text_hover))
				{
					$tagOptions['style']['color'] = $tag->color_text_hover;
				}
			}

			if ( ! empty( $tag->image ) ) {
				$tagImageOptions = [ 'class' => 'b-portfolio-tag__image' ];
				$src = $tag->imageUpload->getImageUrl();
				if ( ! empty( $tag->image_hover ) ) {
					$tagImageOptions['data-hover-src'] = $tag->imageHoverUpload->getImageUrl();
				}
				if ( $isSelected ) {
					$src = $tag->imageHoverUpload->getImageUrl();
					unset($tagImageOptions['data-hover-src']);
				}

				$tagContent = CHtml::image( $src, $tagContent, $tagImageOptions );

				$tagOptions['class'] .= ' b-portfolio-tag_image';
			} else {
				$tagOptions['class'] .= ' b-portfolio-tag_text';
			}
			$tagOptions['style'] = array_reduce(array_keys( $tagOptions['style']),function($carry,$key) use ($tagOptions) {
				$carry .= "$key:{$tagOptions['style'][$key]};";
				return $carry;
			});
			echo CHtml::link( $tagContent, $tag->url, $tagOptions );
		}
	}
	?>
</div>
