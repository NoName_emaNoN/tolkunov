<?php
/**
 * @var $this PortfolioController
 * @var $model Portfolio
 */

$this->title = $model->seo_title ?: [$model->getTitle(), 'Портфолио', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;

Yii::app()->clientScript->registerPackage('jquery');
?>
<div <?= Yii::app()->request->isAjaxRequest ? 'style="width:1080px;padding:80px 40px 0;background:#fff;border-radius:15px;"' : 'class="container"' ?>>
    <div class="b-portfolio-item__date"><?= Yii::app()->dateFormatter->format('d MMMM yyyy', $model->date); ?></div>

    <h1 class="b-page__title b-page__title_small"><?= $model->getHeaderString(); ?> для <?= CHtml::link($model->client->title,
            $model->client->url, ['class' => 'b-page__link']); ?><br>«<?= $model->title ?>»</h1>

    <?php $languages = $model->getLanguages(); ?>
    <?php if (count($languages) > 1): ?>
        <div class="b-language-tabs">
            <ul role="tablist" class="b-language-tabs__list">
                <?php foreach ($model->getLanguages() as $key => $language): ?>
                    <li role="presentation" class="b-language-tabs__item<?= $language == reset($languages) ? ' active' : '' ?>">
                        <a href="#banner_language_<?= $key ?>" aria-controls="banner_language_<?= $key ?>" role="tab" data-toggle="tab" class="b-language-tabs__link">
                            <span class="b-language-tabs__title"><?= $language ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="b-portfolio-item">
        <div class="tab-content">
            <?php foreach ($model->getLanguages() as $key => $language): ?>
                <div id="banner_language_<?= $key ?>" role="tabpanel" class="tab-pane<?= $language == reset($languages) ? ' active' : '' ?>">
                    <ul class="b-size-tabs list-unstyled">
                        <?php foreach (array_values(array_filter($model->items, function (PortfolioItem $item) use ($language) {
                            return $item->getLanguage() == $language;
                        })) as $itemKey => $item): ?>
                            <li role="presentation" class="b-size-tabs__item<?= $itemKey === 0 ? ' active' : '' ?>">
                                <?= CHtml::link($item->width . '<span class="b-size-tabs__divider">x</span>' . $item->height, '#' . $item->getId(),
                                    ['class' => 'b-size-tabs__link', 'data-toggle' => 'tab', 'role' => 'tab', 'aria-controls' => $item->getId()]); ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="tab-content">
                        <?php foreach (array_values(array_filter($model->items, function (PortfolioItem $item) use ($language) {
                            return $item->getLanguage() == $language;
                        })) as $itemKey => $item): ?>
                            <div id="<?= $item->getId() ?>" role="tabpanel" class="tab-pane<?= $itemKey === 0 ? ' active' : '' ?>">
                                <div class="b-portfolio-item__view">
                                    <div style="width:<?= $item->real_width ?>px;height:<?= $item->real_height ?>px;border:<?= $model->border_width ?>px solid <?= $model->getBorderColorString() ?>;margin-top:-<?= $model->border_width ?>px;"
                                         class="b-portfolio-item__border"></div>
                                    <!--                                <div class="b-portfolio-item__view"--><? //= $model->pattern ? ' style="background-image:url(\'' . $model->patternUpload->getImageUrl(1080) . '\');"' : ''; ?><!-- data-rjs="2">-->
                                    <?php if (in_array($model->type, [Portfolio::TYPE_VIDEO, Portfolio::TYPE_STORY])): ?>
                                        <video autoplay loop muted width="<?= $item->real_width ?>" height="<?= $item->real_height ?>" controls controlsList="nodownload" src="<?= $item->fileUpload->getFileUrl() ?>"
                                               class="b-portfolio-item__video"></video>
                                    <?php else: ?>
                                        <div class="b-portfolio-item__scroll scrollbar-light">
                                            <?php if ($model->type == Portfolio::TYPE_HTML5): ?>
                                                <div class="b-iframe-scroll" style="width:<?= $item->real_width ?>px;height: <?= $item->real_height ?>px">
                                                    <?= $item->getIframe() ?>
                                                    <div class="b-iframe-scroll__fix"></div>
                                                </div>
                                            <?php else: ?>
                                                <?= CHtml::tag('span', [
                                                    'class' => 'js-image',
                                                    'data-params' => json_encode([
                                                        'src' => $item->imageUpload->getImageUrl(),
                                                        'alt' => CHtml::encode($model->title),
                                                        'style' => "width:{$item->real_width}px;max-height: {$item->real_height}px",
                                                    ]),
                                                ]); ?>
                                                <!--                                        --><? //= CHtml::image($item->imageUpload->getImageUrl(), CHtml::encode($model->title), ['class' => 'b-portfolio-item__image img-responsive']); ?>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="b-portfolio-tag-outer b-portfolio-tag-outer_detailview">
            <?
            if (!empty($model->tags)) {
                foreach ($model->tags as $item) {
                    $tag = $item->tag;
                    $tagContent = $tag->name;

                    $tag->color_background = $tag->color_background ?: '#ffffff';
                    $tag->color_background_hover = $tag->color_background_hover ?: '#4c5370';
                    $tag->color_text = $tag->color_text ?: '#4c5370';
                    $tag->color_text_hover = $tag->color_text_hover ?: '#ffffff';

                    $tagOptions = [
                        'class' => 'js-portfolio-tag b-portfolio-tag',
                        'data-tag-background' => $tag->color_background,
                        'data-tag-background-hover' => $tag->color_background_hover,
                        'data-tag-color' => $tag->color_text,
                        'data-tag-color-hover' => $tag->color_text_hover,
                        'style' => (!empty($tag->color_background) ? 'background:' . $tag->color_background . ';' : '') . (!empty($tag->color_text) ? 'color:' . $tag->color_text . ';' : ''),
                    ];
                    if (!empty($tag->image)) {
                        $tagImageOptions = ['class' => 'b-portfolio-tag__image'];
                        if (!empty($tag->image_hover)) {
                            $tagImageOptions['data-hover-src'] = $tag->imageHoverUpload->getImageUrl();
                        }

                        $tagContent = CHtml::image($tag->imageUpload->getImageUrl(), $tagContent, $tagImageOptions);

                        $tagOptions['class'] .= ' b-portfolio-tag_image';
                    } else {
                        $tagOptions['class'] .= ' b-portfolio-tag_text';
                    }
                    echo CHtml::link($tagContent, $tag->url, $tagOptions);
                }
            }
            ?>
        </div>
        <div class="b-portfolio-item__text b-text">
            <?= $model->text; ?>
        </div>
        <!--        <div class="text-center b-portfolio-item__btn-container">-->
        <!--            --><? //= CHtml::link('Заказать похожий баннер', ['/order/order/portfolio', 'id' => $model->id], ['class' => 'btn btn-success b-portfolio-item__button']); ?>
        <!--        </div>-->
    </div>
    <?= !Yii::app()->request->isAjaxRequest ? '</div>' : ''; ?>

    <?php $dataProvider = new CArrayDataProvider($model->getRelatedMaterials(8 * 3), ['pagination' => false]); ?>
    <!--    --><?php //$dataProvider = Portfolio::model()->published()->theme($model->theme_id)->searchNotFor($model->id); ?>

    <?php if ($dataProvider->getTotalItemCount()): ?>
        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'id' => 'portfolio-list-' . $model->id,
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'summaryTagName' => 'h5',
                'summaryCssClass' => 'b-portfolio__header b-portfolio__header_small',
                'summaryText' => 'Похожие работы',
                'template' => '{summary}{items}',
                'cssFile' => false,
                'enableHistory' => false,
                'itemsCssClass' => 'b-portfolio__list b-portfolio__list_faded',
                'htmlOptions' => ['class' => 'b-portfolio ' . (Yii::app()->request->isAjaxRequest ? 'g-pb80' : 'g-pb150')],
            ]
        ); ?>
    <?php endif; ?>
    <?php if (Yii::app()->request->isAjaxRequest): ?>
</div>
<?php endif; ?>

<script>
    function showIframe() {
        $('.b-portfolio-item__view iframe').remove();
        $('.b-portfolio-item__view img').remove();

        $('.tab-content .tab-pane.active .tab-pane.active .js-iframe').each(function () {
            var $e = $(this);

            var $iframe = $('<iframe>')
                .attr('src', $e.data('src'))
                .attr('width', $e.data('width'))
                .attr('height', $e.data('height'))
                .attr('frameborder', '0')
                .attr('scrolling', 'no');

            $e.after($iframe);
        });

        $('.tab-content .tab-pane.active .tab-pane.active .js-image').each(function () {
            var $e = $(this);

            var $image = $('<img>')
                .attr($e.data('params'))
                .addClass('b-portfolio-item__image');
//                .addClass('img-responsive');

            $e.after($image);
        });
    }

    $('.b-language-tabs__link, .b-size-tabs__link').on('click', function () {
        var href = $(this).attr('href');

        history.replaceState(history.state, document.title, href);
        setTimeout(showIframe, 1);
    });

    setTimeout(function () {
        if (location.hash && location.hash.charAt(1) !== '!') {
            var hash = location.hash;
            var id = $('a[href=\"' + hash + '\"]').closest('.tab-pane').attr('id');

            if (id) {
                $('a[href=\"#' + id + '\"]').trigger('click');
            }

            $('a[href=\"' + hash + '\"]').trigger('click');
        }
    }, 1);

    $(function () {
        loadPortfolio();
        showIframe();
    });

    <?php if (!Yii::app()->request->isAjaxRequest): ?>
    setTimeout(showIframe, 1);
    <?php endif; ?>

    <?php if (Yii::app()->request->isAjaxRequest): ?>

    $('.fancybox-inner .b-portfolio__item').on('click', function (e) {
        e.preventDefault();

        var href = $(this).attr('href');

        $.fancybox.close(true);

        setTimeout(function () {
            $('<a>').addClass('b-portfolio__item').addClass('hidden').attr('href', href).appendTo($('body')).trigger('click');
        }, 300);

        return false;
    });

    setTimeout(window.retinajs, 1);

    $(".b-portfolio-item__scroll").scrollbar();

    if (!window.previousTitle) {
        window.previousTitle = document.title;
    }

    document.title = <?=CJavaScript::encode($model->seo_title ?: implode([$model->getTitle(), 'Портфолио', Yii::app()->getModule('yupe')->siteName], ' | '))?>;
    <?php endif; ?>

    <?php if (!Yii::app()->request->isAjaxRequest): ?>
    var state = {action: 'portfolioItem', path: "<?=$model->url?>"};
    // Change URL in browser
    history.replaceState(null, document.title, location.href);

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('state', state);

            location.reload();
        }
        if (!state) {
            location.reload();
        }
    });
    <?php endif;?>
</script>
