<?php
/**
 * Отображение для portfolio/index
 *
 * @var $this PortfolioController
 * @var $dataProvider CActiveDataProvider
 * @var $baseCriteria CDbCriteria
 * @var $model Portfolio
 * @var $selectedTag PortfolioTag|null
 * @var $selectedSection PortfolioSection|null
 **/

$this->title = ['Портфолио', Yii::app()->getModule('yupe')->siteName];

if (!empty($selectedTag)) {
    $this->title = [$selectedTag->title, 'Портфолио', Yii::app()->getModule('yupe')->siteName];
}
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('PortfolioModule.portfolio', 'Портфолио')];

Yii::app()->clientScript->registerScript('search', "
    $(function(){
    if (location.hash && $('a[href=\"' + location.hash + '\"]').length) {
        var link = $('a[href=\"' + location.hash + '\"]');
	    $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
		link.addClass('b-portfolio-filter__item_active');
		
        var typeId = $(link).data('type-id');
        var href = $(link).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.replaceState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('index') . "' + href;
    } else {
        history.replaceState(portfolioBaseState, document.title);
    }
    });

    $(document).on('click', '.b-portfolio-filter__item', function () {
        $('.b-portfolio-filter__item_active').removeClass('b-portfolio-filter__item_active');
        $(this).addClass('b-portfolio-filter__item_active');
        var typeId = $(this).data('type-id');
        var href = $(this).attr('href');
        var data = 'Portfolio[type]=' + typeId;

        state = {action: 'popup', modal: data};

        console.log('update list');
        $.fn.yiiListView.update('portfolio-list', {
            data: data
        });

        // Change URL in browser
        history.pushState(state, document.title, href);
        portfolioBaseState = state;
        portfolioBaseUrl = '" . $this->createUrl('index') . "' + href;
        return false;
    });

    // Listen for history state changes
    window.addEventListener('popstate', function (e) {
        var state = history.state;
        // back button pressed. close popup
        if (state && state.action == 'popup') {
            // Forward button pressed, reopen popup
            console.log('update list');
            $.fn.yiiListView.update('portfolio-list', {
                data: state.modal
            });
        }
    });
");
?>

<?
if (empty($selectedTag) || empty($selectedTag->title)) {
    $title = 'В портфолио ' . Yii::t('PortfolioModule.portfolio', '{n} работа|{n} работы|{n} работ', [Portfolio::model()->published()->count()]);
} else {
    $title = $selectedTag->title;
}
?>
<?php //$this->renderPartial('_search', ['model' => $model, 'baseCriteria' => $baseCriteria]); ?>
<?php $this->renderPartial('_menu', ['selectedTag' => $selectedTag, 'selectedSection' => $selectedSection, 'title' => $title]); ?>

<div class="b-portfolio">

    <h1 class="b-portfolio__header"><?= $title ?></h1>

    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'id' => 'portfolio-list',
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'cssFile' => false,
            'ajaxType' => 'GET',
            'enableHistory' => false,
            'beforeAjaxUpdate' => 'js:function(){$(".b-portfolio").css("height", $(".b-portfolio").height());}',
            'afterAjaxUpdate' => 'js:function(){loadPortfolio(function(){$(".b-portfolio").css("height", "auto");});window.retinajs();}',
            'itemsCssClass' => 'b-portfolio__list',
            'htmlOptions' => ['class' => 'g-mb150'],
            'pagerCssClass' => 'g-loader g-loader_portfolio',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>
</div>

<script>
    $(function () {
        var itemsCssClass = '.b-portfolio__list';
        var buttonCssClass = '.g-loader__button';
        var afterAjaxUpdate = function () {
            loadPortfolio(function () {
                console.log('Portfolio updated');
                window.retinajs();
            });
        };

        loaderHandle(itemsCssClass, buttonCssClass, afterAjaxUpdate, false, true);
    });
</script>

<script>
    portfolioBaseUrl = '<?=$this->createUrl('index')?>';
    portfolioBaseState = {action: 'popup', modal: 'Portfolio[type]='};

    // Listen for history state changes
    window.addEventListener('popstate', function () {
        var state = history.state;
        // back button pressed. close popup
        if (!state) {
            location.reload();
        }
    });
</script>
