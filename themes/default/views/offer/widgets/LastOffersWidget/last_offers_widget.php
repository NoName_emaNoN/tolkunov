<?php
/**
 * @var $this LastOffersWidget
 * @var $models Offer[]
 */
?>

<h2 id="offers" class="b-page__title">Акции</h2>
<p class="b-page__lead">Бонусы от партнеров для наших клиентов</p>
<div class="b-offers">
    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'dataProvider' => new CArrayDataProvider($models, ['pagination' => false]),
            'itemView' => '../../offer/_item',
            'template' => '{items}',
            'cssFile' => false,
            'itemsCssClass' => 'b-offers__list'
        ]
    ); ?>
    <div class="g-loader">
        <div class="g-loader__content">
            <?= CHtml::link(
                Yii::t('OfferModule.offer', 'Посмотреть {n} акцию|Посмотреть все {n} акции|Посмотреть все {n} акций', [Offer::model()->published()->count()]),
                ['/offer/offer/index'],
                ['class' => 'btn btn-primary btn-lg g-loader__link']
            ); ?>
        </div>
    </div>
</div>
