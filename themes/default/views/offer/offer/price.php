<?php
/**
 * Отображение для offer/price
 *
 * @var $this OfferController
 **/

$this->title = ['Стоимость', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('OfferModule.offer', 'Стоимость')];
?>

<div class="container">
    <h1 class="b-page__title">Стоимость разработки баннеров</h1>

    <p class="b-page__lead">Несколько тарифов для всех типов баннеров: html5, flash, gif и jpg.<br>Есть также постоянные <a href="#discounts" class="b-page__anchor">скидки</a> и выгодные <a href="#offers" class="b-page__anchor">акции</a>.
    </p>
</div>
<div class="b-prices">
    <div class="container">
        <div id="html5"></div>
        <p class="b-prices__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-nazvanie-html5-bannerov"]); ?></p>

        <p class="b-prices__lead"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-opisanie-html5-bannerov"]); ?></p>

        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'dataProvider' => Tariff::model()->published()->type(Tariff::TYPE_HTML5)->search(),
                'itemView' => '_price',
                'template' => '{items}',
                'cssFile' => false,
                'itemsCssClass' => 'b-prices__list b-prices__list_count_4',
            ]
        ); ?>

        <div id="gif"></div>
        <p class="b-prices__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-nazvanie-gif-bannerov"]); ?></p>

        <p class="b-prices__lead"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-opisanie-gif-bannerov"]); ?></p>

        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'dataProvider' => Tariff::model()->published()->type(Tariff::TYPE_GIF)->search(),
                'itemView' => '_price',
                'template' => '{items}',
                'cssFile' => false,
                'itemsCssClass' => 'b-prices__list b-prices__list_count_3',
            ]
        ); ?>

        <div id="jpeg"></div>
        <p class="b-prices__header"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-nazvanie-jpg-bannerov"]); ?></p>

        <p class="b-prices__lead"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "tekst-na-stranice-cen-opisanie-jpg-bannerov"]); ?></p>

        <?php $this->widget(
            'zii.widgets.CListView',
            [
                'dataProvider' => Tariff::model()->published()->type(Tariff::TYPE_JPG)->search(),
                'itemView' => '_price',
                'template' => '{items}',
                'cssFile' => false,
                'itemsCssClass' => 'b-prices__list b-prices__list_count_3',
            ]
        ); ?>
    </div>
</div>
<div class="container">
    <div id="discounts" class="b-discounts">
        <h2 class="b-discounts__header">Скидки</h2>

        <div class="b-discounts__list">
            <div class="b-discounts__item">
                <p class="b-discounts__title"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-1-zagolovok"]); ?></p>

                <div class="b-discounts__text">
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-1-tekst"]); ?>
                </div>
            </div>
            <div class="b-discounts__item">
                <p class="b-discounts__title"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-2-zagolovok"]); ?></p>

                <div class="b-discounts__text">
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-2-tekst"]); ?>
                </div>
            </div>
        </div>
        <div class="b-discounts__list">
            <div class="b-discounts__item">
                <p class="b-discounts__title"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-3-zagolovok"]); ?></p>

                <div class="b-discounts__text">
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-3-tekst"]); ?>
                </div>
            </div>
            <div class="b-discounts__item">
                <p class="b-discounts__title"><?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-4-zagolovok"]); ?></p>

                <div class="b-discounts__text">
                    <?php $this->widget("application.modules.contentblock.widgets.ContentBlockWidget", ["code" => "skidka-4-tekst"]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->widget('application.modules.offer.widgets.LastOffersWidget'); ?>

<div class="container">
    <?php $this->widget('application.modules.feedback.widgets.SubscribeFormWidget'); ?>
</div>
<div class="hidden">
    <div id="prices_example_1">
        <div class="b-price-modal b-price-modal_color_green">
            <p class="b-price-modal__title">Примеры html5 и флеш-баннеров<br>по тарифу <span class="b-price-modal__name">«Эконом»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— Простая анимация;</p>

                    <p class="b-price-modal__feature">— Нельзя вносить правки;</p>

                    <p class="b-price-modal__feature">— От 5 дней на разработку;</p>

                    <p class="b-price-modal__feature">— 1490
                        <small>₽</small>
                        за ресайз
                    </p>
                    <p class="b-price-modal__feature">— 490
                        <small>₽</small>
                        за гиф-заглушку
                    </p>
                </div>
                <div class="b-price-modal__price">2990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_2">
        <div class="b-price-modal b-price-modal_color_red">
            <p class="b-price-modal__title">Примеры html5 и флеш-баннеров<br>по тарифу <span class="b-price-modal__name">«Стандарт»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— Простая анимация;</p>

                    <p class="b-price-modal__feature">— Можно вносить правки;</p>

                    <p class="b-price-modal__feature">— От 2 дней на разработку;</p>

                    <p class="b-price-modal__feature">— 1490
                        <small>₽</small>
                        за ресайз
                    </p>
                    <p class="b-price-modal__feature">— Гиф-заглушка в <b>подарок</b></p>
                </div>
                <div class="b-price-modal__price">4990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_3">
        <div class="b-price-modal b-price-modal_color_blue">
            <p class="b-price-modal__title">Примеры html5 и флеш-баннеров<br>по тарифу <span class="b-price-modal__name">«Премиум»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— Сложная анимация с <b>«вау-эффектом»</b>;</p>

                    <p class="b-price-modal__feature">— Можно вносить правки;</p>

                    <p class="b-price-modal__feature">— От 2 дней на разработку;</p>

                    <p class="b-price-modal__feature">— Можно добавить интерактив;</p>

                    <p class="b-price-modal__feature">— 1990
                        <small>₽</small>
                        за ресайз
                    </p>
                    <p class="b-price-modal__feature">— Гиф-заглушка в <b>подарок</b></p>
                </div>
                <div class="b-price-modal__price">9990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_4">
        <div class="b-price-modal b-price-modal_color_green">
            <p class="b-price-modal__title">Примеры гиф-баннеров по тарифу <span class="b-price-modal__name">«Мини»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Простые</b> кнопки, очень маленькие баннеры, а также несложная анимация;</p>

                    <p class="b-price-modal__feature">— От 1 дня на разработку;</p>

                    <p class="b-price-modal__feature">— 490
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_5">
        <div class="b-price-modal b-price-modal_color_red">
            <p class="b-price-modal__title">Примеры гиф-баннеров<br>по тарифу <span class="b-price-modal__name">«Бизнес»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Крутые</b> гиф-баннеры, которые подойдут для рекламы любого проекта,<br>а также для размещения на любой площадке;</p>

                    <p class="b-price-modal__feature">— От 2 дней на разработку;</p>

                    <p class="b-price-modal__feature">— 990
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">2490<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_6">
        <div class="b-price-modal b-price-modal_color_blue">
            <p class="b-price-modal__title">Примеры гиф-баннеров<br>по тарифу <span class="b-price-modal__name">«Анимация+»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Крутые</b> гиф-баннеры, плюс плавная анимация каждого кадра.</p>

                    <p class="b-price-modal__feature">— Не для всех площадок, вес баннера от 500 кб.;</p>

                    <p class="b-price-modal__feature">— От 3 дней на разработку;</p>

                    <p class="b-price-modal__feature">— 1490
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">4990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_7">
        <div class="b-price-modal b-price-modal_color_green">
            <p class="b-price-modal__title">Примеры статичных баннеров<br>по тарифу <span class="b-price-modal__name">«Смолл»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Простые</b> кнопки, очень маленькие баннеры;</p><br>

                    <p class="b-price-modal__feature">— От 1 дня на разработку;</p>

                    <p class="b-price-modal__feature">— 290
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">490<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_8">
        <div class="b-price-modal b-price-modal_color_red">
            <p class="b-price-modal__title">Примеры статичных баннеров<br>по тарифу <span class="b-price-modal__name">«Мидл»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Крутые</b> статичные баннеры всех стандартных размеров,<br>которые подойдут для рекламы любого проекта, а также для размещения на любой площадке;</p>

                    <p class="b-price-modal__feature">— От 1 дня на разработку;</p>

                    <p class="b-price-modal__feature">— 690
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">1990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
    <div id="prices_example_9">
        <div class="b-price-modal b-price-modal_color_blue">
            <p class="b-price-modal__title">Примеры статичных баннеров<br>по тарифу <span class="b-price-modal__name">«Биг»</span></p>

            <div class="b-price-modal__list">
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
                <div class="b-price-modal__item"><img src="http://placehold.it/728x90" class="b-price-modal__image img-responsive"></div>
            </div>
            <div class="b-price-modal__footer">
                <div class="b-price-modal__features">
                    <p class="b-price-modal__feature">— <b>Большие</b> статичные баннеры для рекламы в социальных сетях, слайдеров на сайте,<br>а также для особых форматов на рекламных площадках;</p>

                    <p class="b-price-modal__feature">— От 2 дней на разработку;</p>

                    <p class="b-price-modal__feature">— 990
                        <small>₽</small>
                        за ресайз
                    </p>
                </div>
                <div class="b-price-modal__price">2990<span class="b-price-modal__rub">₽</span></div>
                <a href="#" class="btn btn-lg b-price-modal__button">Заказать баннер</a>
            </div>
        </div>
    </div>
</div>

<style>
    .b-prices__link {
        display: none;
    }
</style>