<?php
/**
 * Отображение для offer/index
 *
 * @var $this OfferController
 * @var $dataProvider CActiveDataProvider
 * @var $current Tariff|null
 **/

if (isset($current)) {
    $this->title = ['Тариф «' . $current->title . '»', 'Акции', Yii::app()->getModule('yupe')->siteName];
} else {
    $this->title = ['Акции', Yii::app()->getModule('yupe')->siteName];
}
$this->metaDescription = Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = Yii::app()->getModule('yupe')->siteKeyWords;

$this->breadcrumbs = [Yii::t('OfferModule.offer', 'Акции')];
?>

<div class="container">
    <div class="text-center">
        <a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="b-page__back js-fancybox-video hidden">КАК ЭТО РАБОТАЕТ?</a>
        <a href="#contact_form" class="b-page__back js-fancybox-inline">СТАТЬ ПАРТНЕРОМ</a>
    </div>
    <?php if (isset($current)): ?>
        <h1 class="b-page__title">Акции для заказов <?= $current->getType() ?> баннеров<br>по тарифу «<?= $current->title ?>»</h1>

        <?php if ($dataProvider->getTotalItemCount()): ?>
            <p class="b-page__lead"><?= Yii::t('OfferModule.offer', '{n} рубль|{n} рубля|{n} рублей', [OfferItem::model()->getMaxBonusForTarif($current->id)]); ?> бонусов от партнеров для наших клиентов</p>
        <?php else: ?>
            <p class="b-page__lead">Бонусы от партнеров для наших клиентов</p>
        <?php endif; ?>
    <?php else: ?>
        <h1 class="b-page__title">Все акции</h1>
        <p class="b-page__lead">Бонусы от партнеров для наших клиентов</p>
    <?php endif; ?>
</div>
<div class="b-offers">
    <?php if (isset($current)): ?>
        <?php $this->renderPartial('_filter', ['current' => $current]); ?>
    <?php endif;?>

    <?php ob_start(); ?>
    <div class="b-offers-empty b-offers__empty">
        <p class="b-offers-empty__title">Акций для этого тарифа пока что нет</p>

        <div class="b-offers-empty__text">
            <p>Но мы работаем над этим. Отбираем для наших клиентов самые интересные и выгодные бонусы.</p>

            <p>Если хотите стать нашим партнером, и разместить свой бонус — <a class="js-fancybox-inline" href="#contact_form">пишите</a>.</p>
        </div>
    </div>
    <?php $emptyText = ob_get_clean(); ?>

    <?php $this->widget(
        'zii.widgets.CListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_item',
            'template' => '{items}{pager}',
            'cssFile' => false,
            'itemsCssClass' => $dataProvider->getTotalItemCount() ? 'b-offers__list' : 'container',
            'viewData' => [
                'tariff' => isset($current) ? $current : null ,
            ],
            'emptyText' => $emptyText,
//            'htmlOptions' => ['class' => 'container'],
            'enableHistory' => false,
            'afterAjaxUpdate' => 'js:function(){loadOffers();window.retinajs();}',
            'pagerCssClass' => 'g-loader',
            'pager' => [
                'class' => 'application.components.LinkPager',
                'header' => false,
            ],
        ]
    ); ?>

    <script>
        $('.b-offers__item').each(function () {
            $(this).data('hold', Math.floor(Math.random() * 450) + 50);
        });
    </script>
</div>
<?php $this->widget('application.modules.feedback.widgets.SubscribeFormWidget'); ?>

<div class="hidden">
    <div id="contact_form" class="b-modal-form">
        <p class="b-modal-form__header">Стать партнёром</p>

        <?php $this->renderPartial('_form', ['model' => new OfferFeedback()]); ?>
    </div>
</div>
