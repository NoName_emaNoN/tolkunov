<?php
/**
 * @var $data Offer
 * @var $tariff Tariff|null
 */
?>
<a href="<?= $data->url ?>" data-effect="relax" data-origin="top" data-expose="true" class="b-offers__item emerge">
    <p class="b-offers__title"><?= $data->title ?></p>

    <div class="b-offers__content">
        <?php if ($data->image_splash): ?>
            <?= CHtml::image($data->imageSplashUpload->getImageUrl(288, 0), CHtml::encode($data->title), ['class' => 'img-responsive b-offers__image', 'data-rjs' => '2']); ?>
        <?php elseif ($data->image): ?>
            <?= CHtml::image($data->getImageUrl(288, 0), CHtml::encode($data->title), ['class' => 'img-responsive b-offers__image', 'data-rjs' => '2']); ?>
        <?php endif; ?>

        <?php if ($data->date): ?>
            <div class="b-offers__label b-offers__label_date">Действует до <b><?= Yii::app()->dateFormatter->format('d.MM.yyyy', $data->date); ?></b></div>
        <?php endif; ?>
        <?php if ($data->coupons): ?>
            <div class="b-offers__label b-offers__label_coupons"><b><?= $data->coupons ?></b> <?= Yii::t('OfferModule.offer', 'ПРОМОКОД|ПРОМОКОДА|ПРОМОКОДОВ', [$data->coupons]); ?></div>
        <?php endif; ?>
        
    </div>

    <div class="b-offers__text">
        <?= $data->description ?>
    </div>
</a>
