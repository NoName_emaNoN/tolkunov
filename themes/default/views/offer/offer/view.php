<?php
/**
 * @var $this OfferController
 * @var $model Offer
 */

$this->title = $model->seo_title ?: [$model->title, 'Акции', Yii::app()->getModule('yupe')->siteName];
$this->metaDescription = $model->seo_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->metaKeywords = $model->seo_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="container">
    <div class="text-center">
        <?= CHtml::link('Посмотреть все акции', ['/offer/offer/index'], ['class' => 'b-page__back']); ?>
    </div>

    <h1 class="b-page__title"><?= $model->title ?></h1>

    <div class="b-offer">
        <div class="b-offer__cover">
            <?php if ($model->image): ?>
                <?= CHtml::image($model->imageUpload->getImageUrl(1000, 300), CHtml::encode($model->title), ['class' => 'img-responsive b-offer__image hidden-xs hidden-sm', 'data-rjs' => '2']); ?>
                <?= CHtml::image($model->imageUpload->getImageUrl(500, 300), CHtml::encode($model->title), ['class' => 'img-responsive b-offer__image visible-xs', 'data-rjs' => '2']); ?>
                <?= CHtml::image($model->imageUpload->getImageUrl(800, 300), CHtml::encode($model->title), ['class' => 'img-responsive b-offer__image visible-sm', 'data-rjs' => '2']); ?>
            <?php endif; ?>

            <div class="b-offer__properties b-offer-properties">
                <div class="b-offer-properties__item">
                    <p class="b-offer-properties__title">Бонусов до</p>

                    <p class="b-offer-properties__value"><?= $model->getMaxBonus() ?><span class="b-offer__rub">₽</span></p>
                </div>
                <?php if ($model->date): ?>
                    <div class="b-offer-properties__item">
                        <p class="b-offer-properties__title">Действует до</p>

                        <p class="b-offer-properties__value"><?= Yii::app()->dateFormatter->format('d.MM.yyyy', $model->date); ?></p>
                    </div>
                <?php endif; ?>
                <?php if ($model->coupons): ?>
                    <div class="b-offer-properties__item">
                        <p class="b-offer-properties__title">Промокодов</p>

                        <p class="b-offer-properties__value"><?= $model->coupons ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="b-offer__text b-text">
            <?= $model->text ?>
        </div>
        <?php if ($model->items): ?>
            <div class="b-offer-variants">
                <p class="b-offer-variants__header">Бонусы в рублях при заказе баннера для тарифов</p>

                <div class="b-offer-variants__list">
                    <?php foreach ($model->items as $item): ?>
                        <div class="b-offer-variants__item b-offer-variants__item_color_<?= $item->tariff->getColorString() ?>">
                            <p class="b-offer-variants__title"><?= $item->tariff->title ?></p>

                            <p class="b-offer-variants__text"><?= $item->description ?></p>

                            <p class="b-offer-variants__price"><?= $item->bonus ?>
                                <small class="b-offer-variants__rub">₽</small>
                            </p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="b-offer__bottom">
            <?= CHtml::link('Заказать баннер', ['/order/order/create'], ['class' => 'btn btn-success btn-lg']); ?>
            <div class="b-offer__bottom-text">&nbsp;и получить бонус или&nbsp;</div>
            <?= CHtml::link(Yii::t('OfferModule.offer', 'Посмотреть {n} акцию|Посмотреть все {n} акции|Посмотреть все {n} акций', [Offer::model()->published()->count()]), ['/offer/offer/index'], ['class' => 'btn btn-primary btn-lg']); ?>
        </div>
    </div>
    <?php $this->widget('application.modules.feedback.widgets.SubscribeFormWidget'); ?>
</div>
